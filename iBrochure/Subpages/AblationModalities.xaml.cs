﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace iBrochure.Subpages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AblationModalities : Page
    {

        private enum DrawerAnimationStates
        {
            Closed,
            Closing,
            Open,
            Opening
        }

        private DrawerAnimationStates _currentDrawerState = DrawerAnimationStates.Closed;

        public AblationModalities()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            gridAblation.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridVsCryo.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridVsRadio.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridMicrowaveReferences.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridCryoReferences.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridRadioReferences.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            RefreshVisibility();

            TranslatePage();
        }

        private void TranslatePage()
        {
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text1, "AblationModalitiesTab1Text1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text1b, "AblationModalitiesTab1Text1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text2, "AblationModalitiesTab1Text2");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text3, "AblationModalitiesTab1Text3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text4, "AblationModalitiesTab1Text4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text5, "AblationModalitiesTab1Text5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text6, "AblationModalitiesTab1Text6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text7, "AblationModalitiesTab1Text7");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text8, "AblationModalitiesTab1Text8");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text9, "AblationModalitiesTab1Text9");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text10, "AblationModalitiesTab1Text10");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text11, "AblationModalitiesTab1Text11");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text12, "AblationModalitiesTab1Text12");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text13, "AblationModalitiesTab1Text13");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text14, "AblationModalitiesTab1Text14");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text15, "AblationModalitiesTab1Text15");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text16, "AblationModalitiesTab1Text16");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text17, "AblationModalitiesTab1Text17");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text17b, "AblationModalitiesTab1Text17");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text17c, "AblationModalitiesTab1Text17");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text17d, "AblationModalitiesTab1Text17");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1Text17e, "AblationModalitiesTab1Text17");


            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text1, "AblationModalitiesTab2Text1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text1b, "AblationModalitiesTab2Text1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text2, "AblationModalitiesTab2Text2");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text3, "AblationModalitiesTab2Text3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text4, "AblationModalitiesTab2Text4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text5, "AblationModalitiesTab2Text5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text6, "AblationModalitiesTab2Text6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text7, "AblationModalitiesTab2Text7");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text8, "AblationModalitiesTab2Text8");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text9, "AblationModalitiesTab2Text9");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text10, "AblationModalitiesTab2Text10");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text11, "AblationModalitiesTab2Text11");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text12, "AblationModalitiesTab2Text12");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text13, "AblationModalitiesTab2Text13");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text14, "AblationModalitiesTab2Text14");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text15, "AblationModalitiesTab2Text15");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text16, "AblationModalitiesTab2Text16");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text17, "AblationModalitiesTab2Text17");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text18, "AblationModalitiesTab2Text18");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text19, "AblationModalitiesTab2Text19");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text20, "AblationModalitiesTab2Text20");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text21, "AblationModalitiesTab2Text21");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text22, "AblationModalitiesTab2Text22");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text23, "AblationModalitiesTab2Text23");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text24, "AblationModalitiesTab2Text24");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text25, "AblationModalitiesTab2Text25");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text26, "AblationModalitiesTab2Text26");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text27, "AblationModalitiesTab2Text27");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text28, "AblationModalitiesTab2Text28");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text29, "AblationModalitiesTab2Text29");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text30, "AblationModalitiesTab2Text30");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text31, "AblationModalitiesTab2Text31");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text32, "AblationModalitiesTab2Text32");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text33, "AblationModalitiesTab2Text33");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text34, "AblationModalitiesTab2Text34");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text35, "AblationModalitiesTab2Text35");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text36, "AblationModalitiesTab2Text36");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text37, "AblationModalitiesTab2Text37");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text38, "AblationModalitiesTab2Text38");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text39, "AblationModalitiesTab2Text39");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text40, "AblationModalitiesTab2Text40");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text41, "AblationModalitiesTab2Text41");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text42, "AblationModalitiesTab2Text42");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text43, "AblationModalitiesTab2Text43");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2Text44, "AblationModalitiesTab2Text44");


            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text1, "AblationModalitiesTab3Text1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text1b, "AblationModalitiesTab3Text1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text2, "AblationModalitiesTab3Text2");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text3, "AblationModalitiesTab3Text3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text4, "AblationModalitiesTab3Text4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text5, "AblationModalitiesTab3Text5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text6, "AblationModalitiesTab3Text6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text7, "AblationModalitiesTab3Text7");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text8, "AblationModalitiesTab3Text8");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text9, "AblationModalitiesTab3Text9");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text10, "AblationModalitiesTab3Text10");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text11, "AblationModalitiesTab3Text11");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text12, "AblationModalitiesTab3Text12");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text13, "AblationModalitiesTab3Text13");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text14, "AblationModalitiesTab3Text14");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text15, "AblationModalitiesTab3Text15");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text16, "AblationModalitiesTab3Text16");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text17, "AblationModalitiesTab3Text17");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text18, "AblationModalitiesTab3Text18");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text19, "AblationModalitiesTab3Text19");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text20, "AblationModalitiesTab3Text20");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text21, "AblationModalitiesTab3Text21");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text22, "AblationModalitiesTab3Text22");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text23, "AblationModalitiesTab3Text23");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text24, "AblationModalitiesTab3Text24");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text25, "AblationModalitiesTab3Text25");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text26, "AblationModalitiesTab3Text26");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text27, "AblationModalitiesTab3Text27");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text28, "AblationModalitiesTab3Text28");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text29, "AblationModalitiesTab3Text29");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text30, "AblationModalitiesTab3Text30");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text31, "AblationModalitiesTab3Text31");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text32, "AblationModalitiesTab3Text32");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text33, "AblationModalitiesTab3Text33");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text34, "AblationModalitiesTab3Text34");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text35, "AblationModalitiesTab3Text35");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text36, "AblationModalitiesTab3Text36");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text37, "AblationModalitiesTab3Text37");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text38, "AblationModalitiesTab3Text38");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text39, "AblationModalitiesTab3Text39");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text40, "AblationModalitiesTab3Text40");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3Text41, "AblationModalitiesTab3Text41");


            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText1, "AblationModalitiesCompareText1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText2, "AblationModalitiesCompareText2");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText3, "AblationModalitiesCompareText3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText3b, "AblationModalitiesCompareText3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText4, "AblationModalitiesCompareText4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText4b, "AblationModalitiesCompareText4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText5, "AblationModalitiesCompareText5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText5b, "AblationModalitiesCompareText5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText6, "AblationModalitiesCompareText6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText6b, "AblationModalitiesCompareText6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText7, "AblationModalitiesCompareText7");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText7b, "AblationModalitiesCompareText7");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText8, "AblationModalitiesCompareText8");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesCompareText8b, "AblationModalitiesCompareText8");


            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1SourcesText1, "AblationModalitiesTab1SourcesText1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1SourcesText2, "AblationModalitiesTab1SourcesText2");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1SourcesText3, "AblationModalitiesTab1SourcesText3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1SourcesText4, "AblationModalitiesTab1SourcesText4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1SourcesText5, "AblationModalitiesTab1SourcesText5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1SourcesText6, "AblationModalitiesTab1SourcesText6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab1SourcesText7, "AblationModalitiesTab1SourcesText7");


            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText1, "AblationModalitiesTab2SourcesText1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText2, "AblationModalitiesTab2SourcesText2");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText3, "AblationModalitiesTab2SourcesText3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText4, "AblationModalitiesTab2SourcesText4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText5, "AblationModalitiesTab2SourcesText5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText6, "AblationModalitiesTab2SourcesText6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText7, "AblationModalitiesTab2SourcesText7");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText8, "AblationModalitiesTab2SourcesText8");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText9, "AblationModalitiesTab2SourcesText9");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText10, "AblationModalitiesTab2SourcesText10");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText11, "AblationModalitiesTab2SourcesText11");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText12, "AblationModalitiesTab2SourcesText12");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText13, "AblationModalitiesTab2SourcesText13");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText14, "AblationModalitiesTab2SourcesText14");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText15, "AblationModalitiesTab2SourcesText15");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText16, "AblationModalitiesTab2SourcesText16");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText17, "AblationModalitiesTab2SourcesText17");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab2SourcesText18, "AblationModalitiesTab2SourcesText18");


            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText1, "AblationModalitiesTab3SourcesText1");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText2, "AblationModalitiesTab3SourcesText2");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText3, "AblationModalitiesTab3SourcesText3");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText4, "AblationModalitiesTab3SourcesText4");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText5, "AblationModalitiesTab3SourcesText5");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText6, "AblationModalitiesTab3SourcesText6");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText7, "AblationModalitiesTab3SourcesText7");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText8, "AblationModalitiesTab3SourcesText8");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText9, "AblationModalitiesTab3SourcesText9");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText10, "AblationModalitiesTab3SourcesText10");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText11, "AblationModalitiesTab3SourcesText11");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText12, "AblationModalitiesTab3SourcesText12");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText13, "AblationModalitiesTab3SourcesText13");
            Translator.Current.styleTextBlockWithKey(AblationModalitiesTab3SourcesText14, "AblationModalitiesTab3SourcesText14");

        }

        private Point initialpoint;
        private bool swipeHandled;
        private void gridMain_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            initialpoint = e.Position;
            swipeHandled = false;
        }

        private void gridMain_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            int threshhold = 100;
            if (swipeHandled || initialpoint.X > 447)
            {
                e.Complete();
                return;
            }
            if (e.IsInertial)
            {
                Point currentpoint = e.Position;
                double diff = currentpoint.X - initialpoint.X;
                if (diff >= threshhold)
                {
                    swipeHandled = true;
                    animateMenuShow();
                }
            }
        }

        private void controlLeftNav_HomeClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void controlLeftNav_TimeAndPowerClicked(object sender, RoutedEventArgs e)
        {
            //nothing
        }

        private void controlLeftNav_SystemOverviewClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToSystemOverview();
        }

        private void controlLeftNav_HideMenuRequested(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void controlLeftNav_CompetitiveComparisonClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToComparison();
        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            animateMenuShow();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void animateMenuHide()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            menuHide.Begin();
        }

        private void animateMenuShow()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Visible;
            menuShow.Begin();
        }

        private void controlLeftNav_AblationModalitiesClicked(object sender, RoutedEventArgs e)
        {
            //nothing
        }

        private async void btnAblation_Click(object sender, RoutedEventArgs e)
        {
            setActiveTab(btnAblationON);
            gridAblation.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridVsCryo.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridVsRadio.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            if (_drawerCancelAnimation != null)
            {
                _drawerCancelAnimation.Cancel();
            }
            _drawerCancelAnimation = new CancellationTokenSource();
            await closeSelectionDrawer(_drawerCancelAnimation.Token);
        }

        private void btnVsCryo_Click(object sender, RoutedEventArgs e)
        {
            setActiveTab(btnVsCryoON);
            gridAblation.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridVsCryo.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridVsRadio.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void btnVsRadio_Click(object sender, RoutedEventArgs e)
        {
            setActiveTab(btnVsRadioON);
            gridAblation.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridVsCryo.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridVsRadio.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void btnAblationON_Click(object sender, RoutedEventArgs e)
        {
            //nothing for now
        }

        private void btnVsCryoON_Click(object sender, RoutedEventArgs e)
        {
            //nothing for now
        }

        private void btnRadioON_Click(object sender, RoutedEventArgs e)
        {
            //nothing for now
        }

        private void setActiveTab(Button active)
        {
            var collapsed = Visibility.Collapsed;
            var visible = Visibility.Visible;
            btnAblationON.Visibility = active == btnAblationON ? visible : collapsed;
            btnVsCryoON.Visibility = active == btnVsCryoON ? visible : collapsed;
            btnVsRadioON.Visibility = active == btnVsRadioON ? visible : collapsed;
        }

        private async void cloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            if (grid.Children.Count > 0)
            {
                //get reverse to cloak in reverse order
                IList<UIElement> reverseList = new List<UIElement>();
                for (int i = grid.Children.Count - 1; i >= 0; i--) //UIElement elem in grid.Children
                {
                    reverseList.Add(grid.Children.ElementAt(i));
                }

                //make copy of children and move to tag
                IList<UIElement> transferList = new List<UIElement>();
                foreach (UIElement elem in grid.Children)
                {
                    transferList.Add(elem);
                }
                grid.Tag = transferList;

                if (staggerDelayMilliseconds > 0)
                {
                    //remove children in reverse order
                    foreach (UIElement elem in reverseList)
                    {
                        //await Task.Delay(rnd.Next(50, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                        grid.Children.Remove(elem);
                    }
                }
                else
                {
                    grid.Children.Clear();
                }

            }
        }

        private async void decloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            grid.Visibility = Windows.UI.Xaml.Visibility.Visible; //ensure visible
            if (grid.Tag != null && grid.Children.Count == 0)
            {
                IList<UIElement> list = grid.Tag as List<UIElement>;
                foreach (UIElement elem in list)
                {
                    if (staggerDelayMilliseconds > 0)
                    {
                        //await Task.Delay(rnd.Next(50, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                    }
                    grid.Children.Add(elem);
                }
            }
        }

        private CancellationTokenSource _drawerCancelAnimation;
        private async Task closeSelectionDrawer(CancellationToken cancelToken)
        {
            _currentDrawerState = DrawerAnimationStates.Closing;
            //btnCloseSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            while (gridSelectionDrawerTransform.X < 338 && !cancelToken.IsCancellationRequested)
            {
                gridSelectionDrawerTransform.X = Math.Min(gridSelectionDrawerTransform.X + 20, 338);
                await Task.Delay(2);
            }
            _currentDrawerState = DrawerAnimationStates.Closed;
        }

        private async Task openSelectionDrawer(CancellationToken cancelToken)
        {
            _currentDrawerState = DrawerAnimationStates.Opening;
            //btnCloseSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            while (gridSelectionDrawerTransform.X > 0 && !cancelToken.IsCancellationRequested)
            {
                gridSelectionDrawerTransform.X  = Math.Max(gridSelectionDrawerTransform.X - 20, 0);
                await Task.Delay(2);
            }
            _currentDrawerState = DrawerAnimationStates.Open;
        }

        //private async void btnCloseSelectionDrawer_Click(object sender, RoutedEventArgs e)
        //{
        //    if (_drawerCancelAnimation != null)
        //    {
        //        _drawerCancelAnimation.Cancel();
        //    }
        //    _drawerCancelAnimation = new CancellationTokenSource();
        //    await closeSelectionDrawer(_drawerCancelAnimation.Token);
        //}

        private async void buttonShowSelectionDrawer_Click(object sender, RoutedEventArgs e)
        {
            if (_drawerCancelAnimation != null)
            {
                _drawerCancelAnimation.Cancel();
            }
            _drawerCancelAnimation = new CancellationTokenSource();

            if (_currentDrawerState == DrawerAnimationStates.Open || _currentDrawerState == DrawerAnimationStates.Opening)
            {
                await closeSelectionDrawer(_drawerCancelAnimation.Token);
            }
            else
            {
                await openSelectionDrawer(_drawerCancelAnimation.Token);
            }
        }

        private void toggleVisibility(UIElement element)
        {
            if (element.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                element.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            else
            {
                element.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        private void RefreshVisibility()
        {
            gridCryoVideo.Visibility = imageDrawerAblationVideoON.Visibility;
            gridCryoMechanism.Visibility = imageDrawerMechanismOfActionON.Visibility;
            gridCryoProbes.Visibility = imageDrawerProbesON.Visibility;
            gridCryoAblationSize.Visibility = imageDrawerAblationSizeON.Visibility;
            gridCryoAblationTime.Visibility = imageDrawerAblationTimeON.Visibility;
            gridCryoClinicalConsiderations.Visibility = imageDrawerClinicalConsiderationsON.Visibility;
            gridRadioVideo.Visibility = imageDrawerAblationVideoON.Visibility;
            gridRadioMechanism.Visibility = imageDrawerMechanismOfActionON.Visibility;
            gridRadioProbes.Visibility = imageDrawerProbesON.Visibility;
            gridRadioAblationSize.Visibility = imageDrawerAblationSizeON.Visibility;
            gridRadioAblationTime.Visibility = imageDrawerAblationTimeON.Visibility;
            gridRadioClinicalConsiderations.Visibility = imageDrawerClinicalConsiderationsON.Visibility;
        }

        private void buttonDrawerToggle_Click(object sender, RoutedEventArgs e)
        {
            if (sender == buttonDrawerAblationVideo)
            {
                toggleVisibility(imageDrawerAblationVideoON);
            }
            else if (sender == buttonDrawerMechanismOfAction)
            {
                toggleVisibility(imageDrawerMechanismOfActionON);
            }
            else if (sender == buttonDrawerProbes)
            {
                toggleVisibility(imageDrawerProbesON);
            }
            else if (sender == buttonDrawerAblationSize)
            {
                toggleVisibility(imageDrawerAblationSizeON);
            }
            else if (sender == buttonDrawerAblationTime)
            {
                toggleVisibility(imageDrawerAblationTimeON);
            }
            else if (sender == buttonDrawerClinicalConsiderations)
            {
                toggleVisibility(imageDrawerClinicalConsiderationsON);
            }
            RefreshVisibility();
        }

        private void buttonMicrowaveReferences_Click(object sender, RoutedEventArgs e)
        {
            gridMicrowaveReferences.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void buttonCloseMicrowaveReferences_Click(object sender, RoutedEventArgs e)
        {
            gridMicrowaveReferences.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void buttonCryoReferences_Click(object sender, RoutedEventArgs e)
        {
            gridSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridCryoReferences.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridWhiteFade1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void buttonCloseCryoReferences_Click(object sender, RoutedEventArgs e)
        {
            gridSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridCryoReferences.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridWhiteFade1.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void buttonRadioReferences_Click(object sender, RoutedEventArgs e)
        {
            gridSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridRadioReferences.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridWhiteFade2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void buttonCloseRadioReferences_Click(object sender, RoutedEventArgs e)
        {
            gridSelectionDrawer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridRadioReferences.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridWhiteFade2.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void btnCloseImageViewer_Click(object sender, RoutedEventArgs e)
        {
            gridImageViewer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void showImage(File file)
        {
            gridImage.Width = file.width;
            gridImage.Height = file.height;
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.UriSource = file.fileUri;
            imageView.Source = bitmapImage;
            gridImageViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private async void showAsset_A_Tapped(object sender, TappedRoutedEventArgs e)
        {
            File file = new File();
            file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[none]");
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Images/AblationModalities/Pop Over Assets/Increasing Impedence.png");
            file.width = 1157;
            file.height = 885;
            showImage(file);
        }
        private async void showAsset_B_Tapped(object sender, TappedRoutedEventArgs e)
        {
            File file = new File();
            file.displayName = "";
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Images/AblationModalities/Pop Over Assets/Heat Sink.png");
            file.width = 1157;
            file.height = 781;
            showImage(file);
        }
        private async void showAsset_C_Tapped(object sender, TappedRoutedEventArgs e)
        {
            File file = new File();
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Images/AblationModalities/Pop Over Assets/Fast Heating.png");
            file.width = 1157;
            file.height = 885;
            showImage(file);
        }
        private async void showAsset_D_Tapped(object sender, TappedRoutedEventArgs e)
        {
            File file = new File();
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Images/AblationModalities/Pop Over Assets/Lung MWA.png");
            file.width = 1157;
            file.height = 731;
            showImage(file);
        }
        private async void showAsset_E_Tapped(object sender, TappedRoutedEventArgs e)
        {
            File file = new File();
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Images/AblationModalities/Pop Over Assets/Burn Pattern Pop Up.png");
            file.width = 1157;
            file.height = 731;
            showImage(file);
        }

        private async void showAsset_TissueShrinkageMWA_Tapped(object sender, TappedRoutedEventArgs e)
        {
            controlDocumentViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            StorageFile uri = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/AblationModalities/Tissue Shrinkage MWA.mp4");
            await controlDocumentViewer.initializeFile(uri);
        }

        private List<float> getNormalizedMarkerList(int videoSecondsLength, params float[] secondsMarker)
        {
            List<float> result = new List<float>();
            foreach (float marker in secondsMarker)
            {
                result.Add(marker / videoSecondsLength);
            }
            return result;
        }

        private async void showMicroVideo_Click(object sender, RoutedEventArgs e)
        {
            int videoSecondsLength = 107;
            controlDocumentViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            StorageFile uri = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/AblationModalities/Neuwave_MicrowaveAblation_iBrochure.mp4");
            List<float> markers = getNormalizedMarkerList(videoSecondsLength, 6f, 43f, 69f);
            List<string> captions = new List<String> { "Mechanism of action", "Performance in vascular tissue", "Performance in aerated tissue" };

            await controlDocumentViewer.initializeVideoFileWithMarkers(uri, markers, captions);
        }
        private async void showCryoVideo_Click(object sender, RoutedEventArgs e)
        {
            int videoSecondsLength = 83;
            controlDocumentViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            StorageFile uri = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/AblationModalities/Cryo_Ablation_iBrochure.mp4");
            List<float> markers = getNormalizedMarkerList(videoSecondsLength, 6f, 64f);
            List<string> captions = new List<String> { "Mechanism of action", "Visible ice ball vs lethal isotherm" };
            await controlDocumentViewer.initializeVideoFileWithMarkers(uri, markers, captions);
        }
        private async void showRadioVideo_Click(object sender, RoutedEventArgs e)
        {
            int videoSecondsLength = 100;
            controlDocumentViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            StorageFile uri = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/AblationModalities/Radiofrequency_Ablation_iBrochure.mp4");
            List<float> markers = getNormalizedMarkerList(videoSecondsLength, 6f, 51f, 73f);
            List<string> captions = new List<String> { "Mechanism of action", "Performance in vascular tissue", "Performance in aerated tissue" };
            await controlDocumentViewer.initializeVideoFileWithMarkers(uri, markers,captions);
        }


        private async void showMicrowaveRadioVideo_Click(object sender, RoutedEventArgs e)
        {
            int videoSecondsLength = 97;
            controlDocumentViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            StorageFile uri = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/AblationModalities/Neuwave_Microwave_and_RF_side-by-side.mp4");
            List<float> markers = getNormalizedMarkerList(videoSecondsLength, 2f, 50f, 70f);
            List<string> captions = new List<String> { "Side by Side", "Liver & Kidney Tissue", "Aerated Lung Tissue" };
            await controlDocumentViewer.initializeVideoFileWithMarkers(uri, markers, captions);
            //await controlDocumentViewer.initializeFile(uri);

        }


    }
}
