﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace iBrochure.Subpages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Comparison : Page
    {
        public Comparison()
        {
            this.InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //cloakGridChildren(gridLeftMenu, 0, 0);
            //gridLeftMenu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText1, "CompetitiveComparisonText1");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText2, "CompetitiveComparisonText2");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText3, "CompetitiveComparisonText3");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText4, "CompetitiveComparisonText4");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText5, "CompetitiveComparisonText5");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText6, "CompetitiveComparisonText6");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText7, "CompetitiveComparisonText7");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText8, "CompetitiveComparisonText8");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText9, "CompetitiveComparisonText9");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText10, "CompetitiveComparisonText10");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText11, "CompetitiveComparisonText11");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText12, "CompetitiveComparisonText12");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText13, "CompetitiveComparisonText13");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText14, "CompetitiveComparisonText14");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText15, "CompetitiveComparisonText15");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText16, "CompetitiveComparisonText16");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText17, "CompetitiveComparisonText17");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText18, "CompetitiveComparisonText18");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText19, "CompetitiveComparisonText19");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText20, "CompetitiveComparisonText20");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText21, "CompetitiveComparisonText21");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText22, "CompetitiveComparisonText22");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText23, "CompetitiveComparisonText23");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText24, "CompetitiveComparisonText24");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText25, "CompetitiveComparisonText25");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText26, "CompetitiveComparisonText26");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText27, "CompetitiveComparisonText27");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText28, "CompetitiveComparisonText28");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText29, "CompetitiveComparisonText29");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText30, "CompetitiveComparisonText30");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText31, "CompetitiveComparisonText31");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText32, "CompetitiveComparisonText32");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText33, "CompetitiveComparisonText33");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText34, "CompetitiveComparisonText34");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText35, "CompetitiveComparisonText35");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText36, "CompetitiveComparisonText36");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText37, "CompetitiveComparisonText37");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText38, "CompetitiveComparisonText38");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText39, "CompetitiveComparisonText39");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText40, "CompetitiveComparisonText40");
            Translator.Current.styleTextBlockWithKey(CompetitiveComparisonText41, "CompetitiveComparisonText41");
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            animateMenuShow();
        }



        private async void cloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            if (grid.Children.Count > 0)
            {
                //get reverse to cloak in reverse order
                IList<UIElement> reverseList = new List<UIElement>();
                for (int i = grid.Children.Count - 1; i >= 0; i--) //UIElement elem in grid.Children
                {
                    reverseList.Add(grid.Children.ElementAt(i));
                }

                //make copy of children and move to tag
                IList<UIElement> transferList = new List<UIElement>();
                foreach (UIElement elem in grid.Children)
                {
                    transferList.Add(elem);
                }
                grid.Tag = transferList;

                if (staggerDelayMilliseconds > 0)
                {
                    //remove children in reverse order
                    foreach (UIElement elem in reverseList)
                    {
                        //await Task.Delay(rnd.Next(50, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                        grid.Children.Remove(elem);
                    }
                }
                else
                {
                    grid.Children.Clear();
                }

            }
        }

        private async void decloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            grid.Visibility = Windows.UI.Xaml.Visibility.Visible; //ensure visible
            if (grid.Tag != null && grid.Children.Count == 0)
            {
                IList<UIElement> list = grid.Tag as List<UIElement>;
                foreach (UIElement elem in list)
                {
                    if (staggerDelayMilliseconds > 0)
                    {
                        //await Task.Delay(rnd.Next(50, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                    }
                    grid.Children.Add(elem);
                }
                decloakGridChildren(gridBarsButtons, 0, 0);
            }
        }

        private void controlLeftNav_HideMenuRequested(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void controlLeftNav_HomeClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void controlLeftNav_TimeAndPowerClicked(object sender, RoutedEventArgs e)
        {

        }

        private void controlLeftNav_SystemOverviewClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToSystemOverview();
        }

        private void controlLeftNav_CompetitiveComparisonClicked(object sender, RoutedEventArgs e)
        {

        }

        private void btnCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void animateMenuHide()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            menuHide.Begin();
        }

        private void animateMenuShow()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Visible;
            menuShow.Begin();
        }

        private Point initialpoint;
        private bool swipeHandled;
        private void gridMain_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            initialpoint = e.Position;
            swipeHandled = false;
        }

        private void gridMain_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            int threshhold = 100;
            if (swipeHandled || initialpoint.X > 447)
            {
                e.Complete();
                return;
            }
            if (e.IsInertial)
            {
                Point currentpoint = e.Position;
                double diff = currentpoint.X - initialpoint.X;
                if (diff >= threshhold)
                {
                    swipeHandled = true;
                    animateMenuShow();
                }
            }
        }

        private void controlLeftNav_AblationModalitiesClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToAblationModalities();
        }

    }
}
