﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.Data.Pdf;
using Windows.ApplicationModel;
using Windows.Storage;
using Windows.Storage.Pickers;
using PdfViewModel;
//using Infuse.EtherWin;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Popups;
//using Infuse.EtherWin.Sessions;
using System.Net;
using Windows.Storage.Streams;
using System.Net.Http;
using Windows.UI.Xaml.Shapes;
using Windows.UI;
using System.Diagnostics;
using iBrochure.Classes;


// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlDocumentViewer : UserControl
    {

        private enum ViewerType
        {
            ViewerTypeNone,
            ViewerTypePDF,
            ViewerTypeImage,
            ViewerTypeVideo,
            ViewerTypeWeb
        }

        private Uri fileUri;

        private bool endFileViewDone = false;

        private DispatcherTimer timer;
        private bool tickUpdateSlider;

        private PdfDocViewModel pdfDataSourceZoomedInView;
        private PdfDocViewModel pdfDataSourceZoomedOutView;
        private PdfDocument pdfDocument;
        private StorageFile loadedFile;
        private GridView zoomedOutView;
        private ListView zoomedInView;
        private List<float> _videoMarkers = new List<float>();
        private List<string> _videoMarkerCaptions = new List<string>();

        float _videoMarkerDisplaySeconds = 5f;
        public float VideoMarkerDisplaySeconds
        {
            get { return _videoMarkerDisplaySeconds; }
            set { _videoMarkerDisplaySeconds = value; }
        }


        public Uri fileURI
        {
            get { return fileUri; }
        }

        public void ShowGridACTabs(bool show)
        {
            if (show)
            {
                gridACTabs.Visibility = Visibility.Visible;
            }
            else
            {
                gridACTabs.Visibility = Visibility.Collapsed;
            }
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (videoViewer.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                RefreshVideoMarkers();
            }
        }

        public SubpageControlDocumentViewer()
        {
            this.InitializeComponent();

        }

        void timer_Tick(object sender, object e)
        {
            tickUpdateSlider = true;
            //timelineSlider.Value = videoPlayer.Position.TotalMilliseconds;
            timelineSlider.Value = videoPlayer.Position.TotalSeconds;
            RefreshVideoMarkerCaptions();
            tickUpdateSlider = false;
        }

        public async Task initializeVideoFileWithMarkers(Uri file, List<float> videoNormalizedTimeMarkers, List<string> videoMarkerCaptions)
        {
            this.loadedFile = null;
            _videoMarkers = videoNormalizedTimeMarkers;
            _videoMarkerCaptions = videoMarkerCaptions;
            await initializeFile(file);
        }
        public async Task initializeVideoFileWithMarkers(StorageFile file, List<float> videoNormalizedTimeMarkers, List<string> videoMarkerCaptions)
        {
            _videoMarkers = videoNormalizedTimeMarkers;
            _videoMarkerCaptions = videoMarkerCaptions;
            this.loadedFile = file;
            this.fileUri = new Uri(file.Path);
            await this.LoadFile();
        }

        public async Task initializeFile(Uri file)
        {
            try
            {
                _videoMarkers = null;
                _videoMarkerCaptions = null;
                this.fileUri = file;
                await this.LoadFile();
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to Initialize File.", ex);
            }
        }
        public async Task initializeFile(StorageFile storageFile)
        {
            try
            {
                _videoMarkers = null;
                _videoMarkerCaptions = null;
                this.fileUri = new Uri(storageFile.Path);
                this.loadedFile = storageFile;
                await this.LoadFile();
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to Initialize File.", ex);
            }
        }

        private void showViewer(ViewerType type)
        {
            pdfViewer.Visibility = type == ViewerType.ViewerTypePDF ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
            imageViewer.Visibility = type == ViewerType.ViewerTypeImage ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
            videoViewer.Visibility = type == ViewerType.ViewerTypeVideo ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
            webBrowser.Visibility = type == ViewerType.ViewerTypeWeb ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async Task LoadFile()
        {
            // Getting installed location of this app
            StorageFolder installedLocation = Package.Current.InstalledLocation;

            bool cannotOpen = false;

            try
            {
                if (this.loadedFile == null)
                {
                    this.loadedFile = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(this.fileUri);
                }
                //this.loadedFile = await file.GetLocalFileAsync();
            }
            catch
            {
            }

            try
            {
                if (this.loadedFile != null)
                {
                    string value = System.IO.Path.GetExtension(fileUri.AbsolutePath).ToUpper();
                    switch (value)
                    {
                        //PDFs
                        case ".PDF":
                            await this.LoadPDF();
                            break;
                        //IMAGES
                        case ".JPG":
                        case ".JPEG":
                        case ".PNG":
                        case ".BMP":
                        case ".GIF":
                        case ".TIFF":
                        case ".TIF":
                            await this.LoadImage();
                            break;
                        //VIDEOS
                        case ".3G2":
                        case ".3GP2":
                        case ".3GP":
                        case ".3GPP":
                        case ".M4A":
                        case ".M4V":
                        case ".MP4V":
                        case ".MP4":
                        case ".MOV":
                        case ".M2TS":
                        case ".ASF":
                        case ".WM":
                        case ".WMV":
                        case ".WMA":
                        case ".AAC":
                        case ".ADT":
                        case ".ADTS":
                        case ".MP3":
                        case ".WAV":
                        case ".AVI":
                        case ".AC3":
                        case ".EC3":
                            await this.LoadVideo();
                            break;
                        default:
                            //if (file.IsLocal)
                            //{
                            await LaunchExternal("Would you like to open this file?");
                            await this.Close();
                            //}
                            //else
                            //{
                            //    await LoadWeb();
                            //}
                            break;
                    }
                }
                else
                {
                    btnExit.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    showViewer(ViewerType.ViewerTypeNone);
                    MessageDialog dialog = new MessageDialog("File does not exist. App is not configured correctly.", "Unable to Locate File");
                    await dialog.ShowAsync();
                    await this.Close();
                }
            }
            catch (Exception ex)
            {
                cannotOpen = true;
            }
            if (cannotOpen)
            {
                await LaunchExternal("Unable to display this file.  Would you like to try and launch it externally?");
                await this.Close();
            }
        }

        private async Task LaunchExternal(string prompt)
        {
            try
            {
                btnExit.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                showViewer(ViewerType.ViewerTypeNone);
                MessageDialog dialog = new MessageDialog(prompt, "External File");
                dialog.Commands.Add(new UICommand { Label = "Yes", Id = 0 });
                dialog.Commands.Add(new UICommand { Label = "No", Id = 1 });
                IUICommand response = await dialog.ShowAsync();
                if ((int)response.Id == 0)
                {
                    //file.HasBeenOpened = true;
                    //await Utilities.startFileView(file);
                    //await Utilities.endFileView(file);
                    endFileViewDone = true;
                    //LAUNCH EXTERNALLY
                    //StorageFile localFile = await file.GetLocalFileAsync();
                    StorageFile localFile = this.loadedFile;
                    if (localFile != null)
                    {
                        // Launch the retrieved file
                        bool success = await Windows.System.Launcher.LaunchFileAsync(localFile);

                        if (success)
                        {
                            // File launched
                        }
                        else
                        {
                            // File launch failed
                        }
                    }
                    else
                    {
                        // Could not find file
                    }
                }

            }
            catch (Exception ex)
            {
                //do nothing, carry on!
                throw ex;
            }
        }

        private async Task LoadPDF()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypePDF);
            StorageFile localFile = this.loadedFile;
            //if (file.IsLocal)
            //{
            //    localFile = await file.GetLocalFileAsync();
            //}
            //else
            //{
            //    localFile = await downloadToTempFile(new Uri(file.Path));
            //}
            this.pdfDocument = await loadPDFFromFile(localFile);
            if (this.pdfDocument != null)
            {
                InitializeZoomedInView();
                InitializeZoomedOutView();
            }

        }

        private async Task<PdfDocument> loadPDFFromFile(StorageFile localFile)
        {
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            txtProcessingDescription.Text = "Processing";
            PdfDocument result = await PdfDocument.LoadFromFileAsync(localFile);
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            return result;
        }

        private async Task<StorageFile> downloadToTempFile(Uri uriPath)
        {
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            txtProcessingDescription.Text = "Downloading";
            string tempFilename = Guid.NewGuid().ToString();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, uriPath);
            HttpResponseMessage response = await (new HttpClient()).SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
            StorageFile result = await (ApplicationData.Current.TemporaryFolder).CreateFileAsync(tempFilename, CreationCollisionOption.ReplaceExisting);
            using (var fs = await result.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite))
            {
                var outStream = fs.GetOutputStreamAt(0);
                var dataWriter = new Windows.Storage.Streams.DataWriter(outStream);
                dataWriter.WriteBytes(await response.Content.ReadAsByteArrayAsync());
                await dataWriter.StoreAsync();
                dataWriter.DetachStream();
                await outStream.FlushAsync();
                outStream.Dispose();
                fs.Dispose();
            }
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            return result;
        }

        private async Task LoadImage()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypeImage);
            StorageFile localFile = this.loadedFile;
            //if (file.IsLocal)
            //{
            //    localFile = await file.GetLocalFileAsync();
            //}
            //else
            //{
            //    localFile = await downloadToTempFile(new Uri(file.Path));
            //}
            imageViewer.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri(localFile.Path));
        }

        private async Task LoadWeb()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypeWeb);
            //webView.Navigate(new Uri(file.Path));
        }

        private async Task LoadVideo()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypeVideo);
            StorageFile localFile = this.loadedFile;
            //if (file.IsLocal)
            //{
            //    localFile = await file.GetLocalFileAsync();
            //}
            //else
            //{
            //    localFile = await downloadToTempFile(new Uri(file.Path));
            //}
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            txtProcessingDescription.Text = "Processing";
            videoPlayer.Source = new Uri(this.BaseUri, localFile.Path);
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            timer = new DispatcherTimer();
            timer.Tick += timer_Tick;
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Start();
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            RefreshVideoMarkers();


        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            videoPlayer.Play();
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void videoPlayer_Tapped(object sender, TappedRoutedEventArgs e)
        {
            videoPlayer.Pause();
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void videoPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }
        private void timelineSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {

            if (tickUpdateSlider) return;

            int SliderValue = (int)timelineSlider.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, miniseconds. 
            // Create a TimeSpan with miliseconds equal to the slider value.
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue * 1000);
            videoPlayer.Position = ts;
        }

        private void videoPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            //timelineSlider.Maximum = videoPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
            timelineSlider.Maximum = videoPlayer.NaturalDuration.TimeSpan.TotalSeconds;
        }


        private void InitializeZoomedInView()
        {
            try
            {
                // Page Size is set to zero for items in main view so that pages of original size are rendered
                Size pageSize;

                pageSize.Width = Window.Current.Bounds.Width;
                pageSize.Height = Window.Current.Bounds.Height;

                // Main view items are rendered on a VSIS surface as they can be resized (optical zoom)
                this.zoomedInView = new ListView();
                this.zoomedInView.Style = this.zoomedInViewStyle;
                this.zoomedInView.ItemTemplate = this.zoomedInViewItemTemplate;
                this.zoomedInView.ItemsPanel = this.zoomedInViewItemsPanelTemplate;
                this.zoomedInView.Template = this.zoomedInViewControlTemplate;
                this.pdfDataSourceZoomedInView = new PdfDocViewModel(pdfDocument, pageSize, SurfaceType.VirtualSurfaceImageSource);
                this.zoomedInView.ItemsSource = this.pdfDataSourceZoomedInView;

                this.pdfViewer.ZoomedInView = zoomedInView;
            }
            catch
            {
                throw new Exception("Test1");
            }
        }

        /// <summary>
        /// Function to initialize ZoomedOutView of Semantic Zoom control
        /// </summary>
        private void InitializeZoomedOutView()
        {
            try
            {
                // Page Size is set to zero for items in main view so that pages of original size are rendered
                Size pageSize;

                // Page size for thumbnail view is set to 300px as this gives good view of the thumbnails on all resolutions
                pageSize.Width = (double)this.Resources["thumbnailWidth"];
                pageSize.Height = (double)this.Resources["thumbnailHeight"];

                // Thumbnail view items are rendered on a SIS surface as they are of fixed size
                this.pdfDataSourceZoomedOutView = new PdfDocViewModel(pdfDocument, pageSize, SurfaceType.SurfaceImageSource);

                this.zoomedOutView = new GridView();
                this.zoomedOutView.Style = this.zoomedOutViewStyle;
                this.zoomedOutView.ItemTemplate = this.zoomedOutViewItemTemplate;
                this.zoomedOutView.ItemsPanel = this.zoomedOutViewItemsPanelTemplate;
                this.zoomedOutView.ItemContainerStyle = this.zoomedOutViewItemContainerStyle;
                this.zoomedOutView.ItemsSource = this.pdfDataSourceZoomedOutView;
                this.pdfViewer.ZoomedOutView = this.zoomedOutView;
            }
            catch
            {
                throw new Exception("Test2");
            }

        }

        public void OnSuspending(object sender, SuspendingEventArgs args)
        {
            // Hint to the driver that the app is entering an idle state and that its memory
            // can be temporarily used for other apps.
            try
            {
                this.pdfDataSourceZoomedInView.Trim();
                this.pdfDataSourceZoomedOutView.Trim();
            }
            catch
            {
                throw new Exception("Test3");
            }

        }

        private void EventHandlerViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            try
            {
                if (!e.IsIntermediate)
                {
                    var scrollViewer = sender as ScrollViewer;
                    if (scrollViewer != null)
                    {
                        // Reloading pages at new zoomFactor
                        this.pdfDataSourceZoomedInView.UpdatePages(scrollViewer.ZoomFactor);
                    }
                }
            }
            catch
            {
                throw new Exception("Test4");
            }

        }
        private void EventHandlerViewChangeStarted(object sender, SemanticZoomViewChangedEventArgs e)
        {
            try
            {
                PdfPageViewModel sourceItem = e.SourceItem.Item as PdfPageViewModel;
                if (sourceItem != null)
                {
                    int pageIndex = (int)(sourceItem.PageIndex);
                    if (this.pdfDataSourceZoomedInView.Count > pageIndex)
                    {
                        // Transitioning from Zooomed Out View to Zoomed In View
                        if (this.pdfViewer.IsZoomedInViewActive)
                        {
                            // Getting destination item from Zoomed-In-View
                            PdfPageViewModel destinationItem = (PdfPageViewModel)this.pdfDataSourceZoomedInView[pageIndex];

                            if (destinationItem != null)
                            {
                                e.DestinationItem.Item = destinationItem;
                            }
                        }
                        // Transitioning from Zooomed In View to Zoomed Out View
                        else
                        {
                            // Getting destination item from Zoomed-In-View
                            PdfPageViewModel destinationItem = (PdfPageViewModel)this.pdfDataSourceZoomedOutView[pageIndex];

                            if (destinationItem != null)
                            {
                                e.DestinationItem.Item = destinationItem;
                            }
                        }
                    }
                }
            }
            catch
            {
                throw new Exception("Test5");
            }

        }

        private async void btnExit_Click(object sender, RoutedEventArgs e)
        {
            await this.Close();
        }

        private async Task Close()
        {
            //Frame rootFrame = Window.Current.Content as Frame;
            //if (rootFrame.CanGoBack)
            //{
            //    rootFrame.GoBack();
            //}
            this.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void webView_ContentLoading(WebView sender, WebViewContentLoadingEventArgs args)
        {
            gridProcessingWeb.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void webView_LoadCompleted(object sender, NavigationEventArgs e)
        {
            gridProcessingWeb.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void RefreshVideoMarkers()
        {
            gridVideoMarkers.Children.Clear();
            if (_videoMarkers == null) return;
            if (_videoMarkers.Count == 0) return;
            foreach (float marker in _videoMarkers)
            {
                //<Ellipse Width="15" Height="15" Fill="White" Margin="0,5,0,0" />                 
                Ellipse circle = new Ellipse();
                circle.Width = 15;
                circle.Height = 15;
                circle.Fill = new SolidColorBrush(Colors.White);
                circle.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
                circle.Margin = new Thickness(gridVideoMarkers.ActualWidth * marker, 5, 0, 0);
                gridVideoMarkers.Children.Add(circle);
            }
        }

        static bool _shownDelineator = false;
        private void RefreshVideoMarkerCaptions()
        {
            float displaySecondsNormalized = _videoMarkerDisplaySeconds / (float)timelineSlider.Maximum;
            if (_videoMarkerCaptions == null) return;
            if (_videoMarkerCaptions.Count == 0) return;
            float normalized = (float)(timelineSlider.Value / timelineSlider.Maximum);
            int index = -1;
            foreach (float marker in _videoMarkers)
            {
                if (normalized >= marker && normalized <= marker + displaySecondsNormalized)
                {
                    index = _videoMarkers.IndexOf(marker);
                    break;
                }
            }
            if (index >= 0 && _videoMarkerDisplaySeconds < timelineSlider.Maximum)
            {
                textMarkerCaption.Text = _videoMarkerCaptions[index];
                //Margin="82,0,0,20"
                double marginLeft = (gridVideoMarkers.ActualWidth * _videoMarkers[index] - (gridMarkerCaption.ActualWidth/2)) + 7;
                gridMarkerCaption.Margin = new Thickness(marginLeft, 0, 0, 20);
                gridMarkerCaption.Visibility = Windows.UI.Xaml.Visibility.Visible;
                if (normalized >= _videoMarkers[index] && normalized <= _videoMarkers[index] + /*0.01f*/ (displaySecondsNormalized * 0.2f) )
                {
                    float opacity = 1.0f - ((_videoMarkers[index] + /*0.01f*/(displaySecondsNormalized * 0.2f) - normalized) * 100);
                    gridMarkerCaption.Opacity = opacity;
                }
                else if (normalized >= _videoMarkers[index] + /*0.04f*/(displaySecondsNormalized * 0.8f) && normalized <= _videoMarkers[index] + /*0.05f*/displaySecondsNormalized)
                {
                    float opacity = (_videoMarkers[index] + displaySecondsNormalized - normalized) * 100;                    
                    gridMarkerCaption.Opacity = opacity;
                }
                else
                {
                    gridMarkerCaption.Opacity = 1.0f;
                    if (!_shownDelineator)
                    {
                        _shownDelineator = true;
                    }
                }
            }
            else
            {
                gridMarkerCaption.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            
        }

        private void Grid_PointerPressed(object sender, PointerRoutedEventArgs e)
        {

        }

        private async void GridSetup_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            await initializeFile(await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Setup.mp4"));
        }

        private async void GridStep1_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            await initializeFile(await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step01.mp4"));
        }

        private async void GridStep2_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            await initializeFile(await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step02.mp4"));
        }

        private async void GridStep3_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            await initializeFile(await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step03.mp4"));
        }

        private async void GridStep4_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            await initializeFile(await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step04.mp4"));
        }

        private async void GridClose_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            await initializeFile(await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Close.mp4"));
        }

        private void videoViewer_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void videoViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

            gridACTabs.Width = videoPlayer.ActualWidth;

            double r = (double)videoPlayer.ActualWidth / (double)videoPlayer.ActualHeight;
            double videoR = 1920.0 / 1080.0;
            if (r < videoR)
            {
                double videoH = (double)videoPlayer.ActualWidth / videoR;
                gridACTabs.Margin = new Thickness(0, (videoPlayer.ActualHeight - videoH) / 2, 0, 0);
            }
        }

    }
}
