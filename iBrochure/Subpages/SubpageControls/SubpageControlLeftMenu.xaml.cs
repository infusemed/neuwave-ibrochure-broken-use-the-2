﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlLeftMenu : UserControl
    {
        public delegate void HomeEventHandler(object sender, RoutedEventArgs e);
        public event HomeEventHandler HomeClicked;

        public delegate void SystemOverviewEventHandler(object sender, RoutedEventArgs e);
        public event SystemOverviewEventHandler SystemOverviewClicked;

        public delegate void TimeAndPowerEventHandler(object sender, RoutedEventArgs e);
        public event TimeAndPowerEventHandler TimeAndPowerClicked;

        public delegate void CompetitiveComparisonEventHandler(object sender, RoutedEventArgs e);
        public event CompetitiveComparisonEventHandler CompetitiveComparisonClicked;

        public delegate void AblationModalitiesEventHandler(object sender, RoutedEventArgs e);
        public event AblationModalitiesEventHandler AblationModalitiesClicked;

        public delegate void HideMenuEventHandler(object sender, RoutedEventArgs e);
        public event HideMenuEventHandler HideMenuRequested;

        public enum ViewType
        {
            Unknown,
            Home,
            System,
            Comparison,
            AblationModalities
        }

        public ViewType currentSelected
        {
            set { setCurrentViewing(value); }
        }

        public SubpageControlLeftMenu()
        {
            this.InitializeComponent();
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            if (HomeClicked != null)
            {
                HomeClicked(sender, e);
            }
        }

        private void btnSystem_Click(object sender, RoutedEventArgs e)
        {
            if (SystemOverviewClicked != null)
            {
                SystemOverviewClicked(sender, e);
            }
        }

        private void btnTimePower_Click(object sender, RoutedEventArgs e)
        {
            if (TimeAndPowerClicked != null)
            {
                TimeAndPowerClicked(sender, e);
            }
        }

        private void btnComparison_Click(object sender, RoutedEventArgs e)
        {
            if (CompetitiveComparisonClicked != null)
            {
                CompetitiveComparisonClicked(sender, e);
            }
        }

        private void btnAblationModalities_Click(object sender, RoutedEventArgs e)
        {
            if (AblationModalitiesClicked != null)
            {
                AblationModalitiesClicked(this, e);
            }
        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            if (HideMenuRequested != null)
            {
                HideMenuRequested(sender, e);
            }
        }

        private Point initialpoint;
        private bool swipeHandled;
        private void stackMain_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            initialpoint = e.Position;
            swipeHandled = false;
        }

        private void stackMain_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (swipeHandled)
            {
                e.Complete();
                return;
            }
            if (e.IsInertial)
            {
                Point currentpoint = e.Position;
                double diff = initialpoint.X - currentpoint.X;
                if (diff >= 100)//500 is the threshold value, where you want to trigger the swipe right event
                {
                    swipeHandled = true;
                    HideMenuRequested(sender, new RoutedEventArgs());
                }
            }
        }

        private void translateTitles()
        {
            Translator.Current.styleTextBlockWithKey(MenuText1, "MenuText1");
            Translator.Current.styleTextBlockWithKey(MenuText2, "MenuText2");
            Translator.Current.styleTextBlockWithKey(MenuText3, "MenuText3");
            Translator.Current.styleTextBlockWithKey(MenuText4, "MenuText4");
            Translator.Current.styleTextBlockWithKey(MenuText5, "MenuText5");
        }

        private void setCurrentViewing(ViewType viewType)
        {
            translateTitles();

            SolidColorBrush selected = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 237, 37, 37));
            SolidColorBrush clear = new SolidColorBrush(Windows.UI.Colors.Transparent);
            SolidColorBrush white = new SolidColorBrush(Windows.UI.Colors.White);
            SolidColorBrush gray = new SolidColorBrush(Windows.UI.Colors.Gray);

            imgHomeOn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imgSystemOn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imgComparisonOn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imgAblationModalitiesOn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridMarkerHome.Background = clear;
            gridMarkerSystem.Background = clear;
            gridMarkerComparison.Background = clear;
            gridMarkerAblationModalities.Background = clear;
            btnHome.Foreground = gray;
            btnSystem.Foreground = gray;
            btnComparison.Foreground = gray;
            btnAblationModalities.Foreground = gray;

            switch (viewType)
            {
                case ViewType.Home:
                    imgHomeOn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    gridMarkerHome.Background = selected;
                    btnHome.Foreground = white;
                    break;
                case ViewType.System:
                    imgSystemOn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    gridMarkerSystem.Background = selected;
                    btnSystem.Foreground = white;
                    break;
                case ViewType.Comparison:
                    imgComparisonOn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    gridMarkerComparison.Background = selected;
                    btnComparison.Foreground = white;
                    break;
                case ViewType.AblationModalities:
                    imgAblationModalitiesOn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    gridMarkerAblationModalities.Background = selected;
                    btnAblationModalities.Foreground = white;
                    break;
                default:
                    break;
            }
        }

    }
}
