﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlUserInterface : UserControl
    {

        private Page _parentPage;
        public Page ParentPage
        {
            get { return _parentPage; }
            set { _parentPage = value; }
        }

        public StackPanel stackTouchScreen
        {
            get { return stackTouchScreenButton; }
        }

        public Grid gridAblationDelivery
        {
            get { return gridMain; }
        }
        public StackPanel stackAblationDelivery
        {
            get { return stackMain; }
        }


        public SubpageControlUserInterface()
        {
            this.InitializeComponent();
            translatePage();
        }

        private void btnTouchScreen_Click(object sender, RoutedEventArgs e)
        {
            _parentPage.Frame.Navigate(typeof(Subpages.TouchScreenWalkThrough));
        }

        public List<File> getCurrentFiles()
        {
            List<File> files = new List<File>();

            //no files to list, send back empy list

            return files;
        }

        private void translatePage()
        {
            Translator.Current.styleTextBlockWithKey(UserInterfaceText1, "UserInterfaceText1");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText2, "UserInterfaceText2");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText3, "UserInterfaceText3");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText4, "UserInterfaceText4");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText5, "UserInterfaceText5");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText6, "UserInterfaceText6");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText7, "UserInterfaceText7");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText8, "UserInterfaceText8");
            Translator.Current.styleTextBlockWithKey(UserInterfaceText9, "UserInterfaceText9");
        }

    }
}
