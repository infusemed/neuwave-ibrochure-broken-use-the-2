﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlAblationConfirmation : UserControl
    {

        public enum ConfirmationTabs
        {
            None,
            Setup,
            Step1,
            Step2,
            Step3,
            Step4,
            Close
        }

        public ConfirmationTabs currentTab = ConfirmationTabs.None;
        private int ellipsesIndex = 0;

        private BitmapImage redHotspot = new BitmapImage(new Uri("ms-appx:///Assets/ImagesRebrand/System Overview/Confirmation/Hotspot Red.png"));
        private BitmapImage blackHotspot = new BitmapImage(new Uri("ms-appx:///Assets/ImagesRebrand/System Overview/Confirmation/Hotspot Black.png"));

        private bool isViewingSuccessfulImages = false;


        public Grid gridAblationConfirmation
        {
            get { return gridMain; }
        }

        private SystemOverview _parent;
        public SystemOverview ParentPage
        {
            get { return _parent; }
            set { _parent = value; }
        }

        public Grid gridAblationConfirmationTabs
        {
            get { return gridTabs; }
        }
        public Grid gridAblationConfirmationNavigationButtons
        {
            get { return gridNavigationButtons; }
        }

        public Grid gridAblationConfirmationLandingPage
        {
            get { return gridAblationConfirmation0; }
        }
        public StackPanel stackAblationConfirmationLandingPage
        {
            get { return stackAblationConfirmation0; }
        }

        public Grid gridAblationConfirmationSetup
        {
            get { return gridAblationConfirmation1; }
        }
        public StackPanel stackAblationConfirmationSetup
        {
            get { return stackAblationConfirmation1; }
        }

        public Grid gridAblationConfirmationStep1
        {
            get { return gridAblationConfirmation2; }
        }
        public StackPanel stackAblationConfirmationStep1
        {
            get { return stackAblationConfirmation2; }
        }

        public Grid gridAblationConfirmationStep2
        {
            get { return gridAblationConfirmation3; }
        }
        public StackPanel stackAblationConfirmationStep2
        {
            get { return stackAblationConfirmation3; }
        }
        public Grid gridAblationConfirmationStep3
        {
            get { return gridAblationConfirmation4; }
        }
        public StackPanel stackAblationConfirmationStep3
        {
            get { return stackAblationConfirmation4; }
        }
        public Grid gridAblationConfirmationStep4
        {
            get { return gridAblationConfirmation5; }
        }
        public StackPanel stackAblationConfirmationStep4
        {
            get { return stackAblationConfirmation5; }
        }
        public Grid gridAblationConfirmationClose
        {
            get { return gridAblationConfirmation6; }
        }
        public StackPanel stackAblationConfirmationClose
        {
            get { return stackAblationConfirmation6; }
        }

        public SubpageControlAblationConfirmation()
        {
            this.InitializeComponent();
        }

        private int sectionDelay = 500;

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            translatePage();
        }

        private void translatePage()
        {
            Translator.Current.styleTextBlockWithKey(imagePrevButton, "AblationConfirmationNavigationText1");
            Translator.Current.styleTextBlockWithKey(imageNextButton, "AblationConfirmationNavigationText2");
            Translator.Current.styleTextBlockWithKey(imageFinishButton, "AblationConfirmationNavigationText3");

            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText1, "AblationConfirmationHomeText1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText2, "AblationConfirmationHomeText2");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText3, "AblationConfirmationHomeText3");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText4, "AblationConfirmationHomeText4");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText5, "AblationConfirmationHomeText5");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText6, "AblationConfirmationHomeText6");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText7, "AblationConfirmationHomeText7");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText8, "AblationConfirmationHomeText8");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText9, "AblationConfirmationHomeText9");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText10, "AblationConfirmationHomeText10");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationHomeText11, "AblationConfirmationHomeText11");
            Translator.Current.styleTextBlockWithKey(txtStartDescription, "AblationConfirmationHomeText12");

            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text1, "AblationConfirmationTab1Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text1b, "AblationConfirmationTab1Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text2, "AblationConfirmationTab1Text2");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text3, "AblationConfirmationTab1Text3");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text4, "AblationConfirmationTab1Text4");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text5, "AblationConfirmationTab1Text5");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text6, "AblationConfirmationTab1Text6");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text7, "AblationConfirmationTab1Text7");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab1Text8, "AblationConfirmationTab1Text8");

            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text1, "AblationConfirmationTab2Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text1b, "AblationConfirmationTab2Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text2, "AblationConfirmationTab2Text2");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text3, "AblationConfirmationTab2Text3");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text4, "AblationConfirmationTab2Text4");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text5, "AblationConfirmationTab2Text5");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text6, "AblationConfirmationTab2Text6");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab2Text7, "AblationConfirmationTab2Text7");

            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text1, "AblationConfirmationTab3Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text1b, "AblationConfirmationTab3Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text2, "AblationConfirmationTab3Text2");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text3, "AblationConfirmationTab3Text3");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text4, "AblationConfirmationTab3Text4");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text5, "AblationConfirmationTab3Text5");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text6, "AblationConfirmationTab3Text6");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab3Text7, "AblationConfirmationTab3Text7");

            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text1, "AblationConfirmationTab4Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text1b, "AblationConfirmationTab4Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text2, "AblationConfirmationTab4Text2");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text3, "AblationConfirmationTab4Text3");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text4, "AblationConfirmationTab4Text4");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text5, "AblationConfirmationTab4Text5");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text6, "AblationConfirmationTab4Text6");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text7, "AblationConfirmationTab4Text7");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text8, "AblationConfirmationTab4Text8");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab4Text9, "AblationConfirmationTab4Text9");
            
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text1, "AblationConfirmationTab5Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text1b, "AblationConfirmationTab5Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text2, "AblationConfirmationTab5Text2");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text3, "AblationConfirmationTab5Text3");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text4, "AblationConfirmationTab5Text4");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text5, "AblationConfirmationTab5Text5");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text6, "AblationConfirmationTab5Text6");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text7, "AblationConfirmationTab5Text7");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text8, "AblationConfirmationTab5Text8");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text8b, "AblationConfirmationTab5Text8b");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text9, "AblationConfirmationTab5Text9");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text10, "AblationConfirmationTab5Text10");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab5Text11, "AblationConfirmationTab5Text11");

            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab6Text1, "AblationConfirmationTab6Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab6Text1b, "AblationConfirmationTab6Text1");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab6Text2, "AblationConfirmationTab6Text2");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab6Text3, "AblationConfirmationTab6Text3");
            Translator.Current.styleTextBlockWithKey(AblationConfirmationTab6Text4, "AblationConfirmationTab6Text4");
            
        }

        private void setCurrentTab(ConfirmationTabs tab)
        {
            currentTab = tab;
            ParentPage.refreshPlusVisible();
        }

        private async void btnAblationConfirmationSetup_Click(object sender, RoutedEventArgs e)
        {
            setCurrentTab(ConfirmationTabs.Setup);
            ParentPage.refreshPlusVisible();
            gridAblationConfirmationNavigationButtons.Opacity = 0.0f;
            decloakGridChildren(gridAblationConfirmationNavigationButtons, 0, 0);
            navigationButtonsShow.Begin();
            hideAblationConfirmationSubPages();
            hideAblationConfirmationTabs();
            await Task.Delay(sectionDelay);
            imgAblationConfirmationSetup.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationSetup.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //imageAblationConfirmationBackground.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridNavigationButtons.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridScreenViewer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imageNextButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageFinishButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            txtPreviousDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText4");//"Process Overview";
            txtNextDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText5");//"Step 1: Pull In Set-up CT Scan";
            decloakGridChildren(stackAblationConfirmationSetup, 0, 0);

            await showBackground("ms-appx:///Assets/Images/ablation confirmation/steps/main-image.png");

        }

        private async void btnAblationConfirmationStep1_Click(object sender, RoutedEventArgs e)
        {
            setCurrentTab(ConfirmationTabs.Step1);
            btnStep0100Perform_Click(null, null);
            hideAblationConfirmationSubPages();
            hideAblationConfirmationTabs();
            await Task.Delay(sectionDelay);
            imgAblationConfirmationStep1.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridNavigationButtons.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridScreenViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageNextButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageFinishButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            txtPreviousDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText6");//"System Set-up";
            txtNextDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText7");//"Step 2: Define Target Ablation Zone";
            decloakGridChildren(stackAblationConfirmationStep1, 0, 0);
        }

        private async void btnAblationConfirmationStep2_Click(object sender, RoutedEventArgs e)
        {
            setCurrentTab(ConfirmationTabs.Step2);
            btnStep0201UseImaging_Click(null, null);
            hideAblationConfirmationSubPages();
            hideAblationConfirmationTabs();
            await Task.Delay(sectionDelay);
            imgAblationConfirmationStep2.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridNavigationButtons.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridScreenViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageNextButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageFinishButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            txtPreviousDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText5");//"Step 1: Pull In Set-up CT Scan";
            txtNextDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText8");//"Step 3: Place Probes, Register Scans & Confirm";
            decloakGridChildren(stackAblationConfirmationStep2, 0, 0);
        }

        private async void btnAblationConfirmationStep3_Click(object sender, RoutedEventArgs e)
        {
            isViewingSuccessfulImages = false;
            setCurrentTab(ConfirmationTabs.Step3);
            //btnStep0301PullProbe_Click(null, null);
            btnStep0300PlaceProbes_Click(null, null);
            hideAblationConfirmationSubPages();
            hideAblationConfirmationTabs();
            await Task.Delay(sectionDelay);
            imgAblationConfirmationStep3.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridNavigationButtons.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridScreenViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageNextButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageFinishButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            txtPreviousDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText6");//"Step 2: Define Target Ablation Zone";
            txtNextDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText9");//"Step 4: Ablate, Register Scans & Confirm";
            decloakGridChildren(stackAblationConfirmationStep3, 0, 0);
        }

        private async Task showBackground(string image)
        {
            StorageFile imageFile = await EtherFileFinder.storageFileForPath(image);
            BitmapImage img = new BitmapImage();
            img.SetSource(await imageFile.OpenReadAsync());
            imageAblationConfirmationBackground.Source = img;//new BitmapImage(new Uri(image));
            imageAblationConfirmationBackground.Opacity = 0.0f;
            imageAblationConfirmationBackground.Visibility = Windows.UI.Xaml.Visibility.Visible;
            float opacity = 0.01f;
            while (opacity < 1.0f)
            {
                opacity *= 1.5f;
                imageAblationConfirmationBackground.Opacity = opacity;
                await Task.Delay(50);
            }
            imageAblationConfirmationBackground.Opacity = 1.0f;
        }

        private async void btnAblationConfirmationStep4_Click(object sender, RoutedEventArgs e)
        {
            isViewingSuccessfulImages = false;
            setCurrentTab(ConfirmationTabs.Step4);
            //btnStep0401PullPost_Click(null, null);
            btnStep0400Perform_Click(null, null);
            hideAblationConfirmationSubPages();
            hideAblationConfirmationTabs();
            await Task.Delay(sectionDelay);
            imgAblationConfirmationStep4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridNavigationButtons.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridScreenViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageNextButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            imageFinishButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            txtPreviousDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText7");//"Step 3: Place Probes, Register Scans & Confirm";
            txtNextDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText10");//"Close Procedure";
            decloakGridChildren(stackAblationConfirmationStep4, 0, 0);
        }

        //private void btnAblationConfirmationStep5_Click(object sender, RoutedEventArgs e)
        //{
        //    hideAblationConfirmationSubPages();
        //    hideAblationConfirmationTabs();
        //    decloakGridChildren(stackAblationConfirmationStep5, 0, 0);
        //    imgAblationConfirmationStep5.Visibility = Windows.UI.Xaml.Visibility.Visible;
        //    btnAblationConfirmationStep5.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        //}

        private async void btnAblationConfirmationClose_Click(object sender, RoutedEventArgs e)
        {
            setCurrentTab(ConfirmationTabs.Close);
            hideAblationConfirmationSubPages();
            hideAblationConfirmationTabs();
            await Task.Delay(sectionDelay);
            imgAblationConfirmationClose.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationClose.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridNavigationButtons.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridScreenViewer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imageNextButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imageFinishButton.Visibility = Windows.UI.Xaml.Visibility.Visible;
            txtPreviousDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText8");//"Step 4: Ablate, Register Scans & Confirm";
            txtNextDescription.Text = Translator.Current.translatedTextForKey("AblationConfirmationNavigationText10");//"System Overview";
            decloakGridChildren(stackAblationConfirmationClose, 0, 0);

            await showBackground("ms-appx:///Assets/Images/ablation confirmation/steps/start-and-finish-device_1024.png");

        }

        private void hideAblationConfirmationSubPages()
        {
            cloakGridChildren(stackAblationConfirmationSetup);
            cloakGridChildren(stackAblationConfirmationStep1);
            cloakGridChildren(stackAblationConfirmationStep2);
            cloakGridChildren(stackAblationConfirmationStep3);
            cloakGridChildren(stackAblationConfirmationStep4);
            cloakGridChildren(stackAblationConfirmationClose);
            imageAblationConfirmationBackground.Visibility = Windows.UI.Xaml.Visibility.Collapsed; imageAblationConfirmationBackground.Opacity = 0.0f;
        }

        private void hideAblationConfirmationTabs()
        {
            imgAblationConfirmationSetup.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imgAblationConfirmationStep1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imgAblationConfirmationStep2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imgAblationConfirmationStep3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imgAblationConfirmationStep4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
             imgAblationConfirmationClose.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnAblationConfirmationSetup.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep1.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep2.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep3.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationStep4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //btnAblationConfirmationStep5.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnAblationConfirmationClose.Visibility = Windows.UI.Xaml.Visibility.Visible;

        }

        private void cloakGridChildren(Panel grid)
        {
            if (grid.Children.Count > 0)
            {
                //make copy of children and move to tag
                IList<UIElement> transferList = new List<UIElement>();
                foreach (UIElement elem in grid.Children)
                {
                    transferList.Add(elem);
                }
                grid.Tag = transferList;
                grid.Children.Clear();
            }
        }

        private void decloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            grid.Visibility = Windows.UI.Xaml.Visibility.Visible; //ensure visible
            if (grid.Tag != null && grid.Children.Count == 0)
            {
                IList<UIElement> list = grid.Tag as List<UIElement>;
                foreach (UIElement elem in list)
                {
                    grid.Children.Add(elem);
                }
            }
            grid.Tag = null;
        }

        private async void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            disableNextPrev();
            stackStep3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackStep4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            switch (currentTab)
            {
                case ConfirmationTabs.Setup:
                    setCurrentTab(ConfirmationTabs.None);
                    navigationButtonsHide.Begin();
                    hideAblationConfirmationSubPages();
                    cloakGridChildren(gridAblationConfirmationTabs);
                    await Task.Delay(500);
                    landingPageShow.Begin();
                    break;
                case ConfirmationTabs.Step1:
                    btnAblationConfirmationSetup_Click(null, null);
                    break;
                case ConfirmationTabs.Step2:
                    btnAblationConfirmationStep1_Click(null, null);
                    break;
                case ConfirmationTabs.Step3:
                    btnAblationConfirmationStep2_Click(null, null);
                    break;
                case ConfirmationTabs.Step4:
                    btnAblationConfirmationStep3_Click(null, null);
                    break;
                case ConfirmationTabs.Close:
                    btnAblationConfirmationStep4_Click(null, null);
                    break;
            }
            await Task.Delay(sectionDelay);
            enableNextPrev();
        }

        private async void btnNext_Click(object sender, RoutedEventArgs e)
        {
            disableNextPrev();
            stackStep3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackStep4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            switch (currentTab)
            {
                case ConfirmationTabs.Setup:
                    btnAblationConfirmationStep1_Click(null, null);
                    break;
                case ConfirmationTabs.Step1:
                    btnAblationConfirmationStep2_Click(null, null);
                    break;
                case ConfirmationTabs.Step2:
                    btnAblationConfirmationStep3_Click(null, null);
                    break;
                case ConfirmationTabs.Step3:
                    btnAblationConfirmationStep4_Click(null, null);
                    break;
                case ConfirmationTabs.Step4:
                    btnAblationConfirmationClose_Click(null, null);
                    break;
                case ConfirmationTabs.Close:
                    if (_parent != null)
                    {
                        _parent.manualBackPress();
                    }
                    break;
            }
            await Task.Delay(sectionDelay);
            enableNextPrev();
        }

        private void enableNextPrev()
        {
            btnNext.IsEnabled = true;
            btnPrevious.IsEnabled = true;
        }
        private void disableNextPrev()
        {
            btnNext.IsEnabled = false;
            btnPrevious.IsEnabled = false;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            decloakGridChildren(gridAblationConfirmationTabs, 0, 0);
            animateLandingPageHide();
            btnAblationConfirmationSetup_Click(null, null);
        }

        public void animateLandingPageHide()
        {
            landingPageHide.Begin();
        }

        public void animateLandingPageShow()
        {
            landingPageShow.Begin();
        }

        private void resetSelectedScreenButtons()
        {
            switch(currentTab)
            {
                case ConfirmationTabs.Step1:
                    imgStep0100Perform.Source = blackHotspot;
                    imgStep0101PatientID.Source = blackHotspot;
                    imgStep0102RecentScans.Source = blackHotspot;
                    imgStep0103PullInScans.Source = blackHotspot;
                    break;
                case ConfirmationTabs.Step2:
                    imgStep0201UseImaging.Source = blackHotspot;
                    imgStep0202UseTarget.Source = blackHotspot;
                    imgStep0203Select.Source = blackHotspot;
                    imgStep0204Lesion.Source = blackHotspot;
                    break;
                case ConfirmationTabs.Step3:
                    imgStep0300PlaceProbes.Source = blackHotspot;
                    imgStep0301PullProbe.Source = blackHotspot;
                    imgStep0302UseImaging.Source = blackHotspot;
                    imgStep0303Register.Source = blackHotspot;
                    imgStep0304AssessProbe.Source = blackHotspot;
                    break;
                case ConfirmationTabs.Step4:
                    imgStep0400Perform.Source = blackHotspot;
                    imgStep0401PullPost.Source = blackHotspot;
                    imgStep0402UseImaging.Source = blackHotspot;
                    imgStep0403Register.Source = blackHotspot;
                    imgStep0404Register.Source = blackHotspot;
                    imgStep0404bTissueContraction.Source = blackHotspot;
                    imgStep0405Technically.Source = blackHotspot;
                    break;
            }
            stackStep3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackStep4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        //string baseSmallPath = "ms-appx:///Assets/Images/ablation confirmation/screenshots small/";
        string baseLargePath = "ms-appx:///Assets/Images/ablation confirmation/screenshots updated/";
        string largeImageName = "";

        private async Task setScreenImage(string imageString)
        {
            if (imageString.Length > 0)
            {
                try
                {
                    //imgScreen.Source = new BitmapImage(new Uri(baseSmallPath + imageString));
                    imgScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath(baseLargePath + imageString));
                    //imgScreen.Source = new BitmapImage(new Uri(baseLargePath + imageString));
                    await Task.Delay(400);
                    imgScreenBottom.Source = imgScreen.Source;
                }
                catch (Exception ex)
                {
                    imgScreen.Source = null;
                    imgScreenBottom.Source = null;
                }
            }
            else
            {
                imgScreen.Source = null;
                imgScreenBottom.Source = null;
            }
        }

        private async void btnStep0100Perform_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0100Perform.Source = redHotspot;
            await setScreenImage("Step-1_INTRO.jpg");
            largeImageName = "Step-1_INTRO.jpg";
        }

        private async void btnStep0101PatientID_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0101PatientID.Source = redHotspot;
            await setScreenImage("Step 1_A_patient ID.jpg");
            largeImageName = "Step 1_A_patient ID.jpg";
        }

        private async void btnStep0102RecentScans_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0102RecentScans.Source = redHotspot;
            await setScreenImage("Step 1_B_recent scans.jpg");
            largeImageName = "Step 1_B_recent scans.jpg";
        }

        private async void btnStep0103PullInScans_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0103PullInScans.Source = redHotspot;
            await setScreenImage("Step 1_C_pull in scans.jpg");
            largeImageName = "Step 1_C_pull in scans.jpg";
        }

        private async void btnStep0201UseImaging_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0201UseImaging.Source = redHotspot;
            await setScreenImage("Step 2A_Use Imaging Tools to find lesion.jpg");
            largeImageName = "Step 2A_Use Imaging Tools to find lesion.jpg";
        }

        private async void btnStep0202UseTarget_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0202UseTarget.Source = redHotspot;
            await setScreenImage("Step 2B_Use target tools to segment lesion.jpg");
            largeImageName = "Step 2B_Use target tools to segment lesion.jpg";
        }

        private async void btnStep0203Select_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0203Select.Source = redHotspot;
            await setScreenImage("Step 2C_Select your desired margin.jpg");
            largeImageName = "Step 2C_Select your desired margin.jpg";
        }

        private async void btnStep0204Lesion_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0204Lesion.Source = redHotspot;
            await setScreenImage("Step 2D_Lesion measurements provided.jpg");
            largeImageName = "Step 2D_Lesion measurements provided.jpg";
        }

        private async void btnStep0300PlaceProbes_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0300PlaceProbes.Source = redHotspot;
            await setScreenImage("Step 3_A.jpg");
            largeImageName = "Step 3_A.jpg";
        }

        private async void btnStep0301PullProbe_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0301PullProbe.Source = redHotspot;
            await setScreenImage("Step 3_B.jpg");
            largeImageName = "Step 3_B.jpg";
        }

        private async void btnStep0302UseImaging_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0302UseImaging.Source = redHotspot;
            await setScreenImage("Step 3_C.jpg");
            largeImageName = "Step 3_C.jpg";
        }

        private async void btnStep0303Register_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0303Register.Source = redHotspot;
            await setScreenImage("Step 3_D.jpg");
            largeImageName = "Step 3_D.jpg";
        }

        private async void btnStep0304AssessProbe_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0304AssessProbe.Source = redHotspot;
            ellipsesIndex = 0;
            stackStep3.Visibility = Windows.UI.Xaml.Visibility.Visible;
            await refreshEllipses3();
        }

        //private async void btnStep0305View3d_Click(object sender, RoutedEventArgs e)
        //{
        //    resetSelectedScreenButtons();
        //    imgStep0305View3d.Source = redHotspot;
        //    await setScreenImage("Step-3_D1_3D-rendering-provided.png");
        //    largeImageName = "Step 3_D1_3D rendering provided.jpg";
        //    stackStep3.Visibility = Windows.UI.Xaml.Visibility.Visible;
        //}

        //private async void btnStep0306Needle_Click(object sender, RoutedEventArgs e)
        //{
        //    resetSelectedScreenButtons();
        //    imgStep0306Needle.Source = redHotspot;
        //    await setScreenImage("Step-3_D2_Needle-and-Periscope-View-Provided.png");
        //    largeImageName = "Step 3_D2_Needle and Periscope View Provided.jpg";
        //    stackStep3.Visibility = Windows.UI.Xaml.Visibility.Visible;
        //}

        private async void btnStep0400Perform_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0400Perform.Source = redHotspot;
            await setScreenImage("Step 4_PERFORM_ABLATION.jpg");
            largeImageName = "Step 4_PERFORM_ABLATION.jpg";
        }

        private async void btnStep0401PullPost_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0401PullPost.Source = redHotspot;
            await setScreenImage("Step 4_A.jpg");
            largeImageName = "Step 4_A.jpg";
        }

        private async void btnStep0402UseImaging_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0402UseImaging.Source = redHotspot;
            await setScreenImage("Step 4_B.jpg");
            largeImageName = "Step 4_B.jpg";
        }

        private async void btnStep0403UseTarget_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0403Register.Source = redHotspot;
            await setScreenImage("Step 4_C.jpg");
            largeImageName = "Step 4_C.jpg";
        }

        private async void btnStep0404Register_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0404Register.Source = redHotspot;
            await setScreenImage("Step 4_D.jpg");
            largeImageName = "Step 4_D.jpg";
        }

        private async void btnStep0404bTissueContraction_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0404bTissueContraction.Source = redHotspot;
            await setScreenImage("Step 4_E.jpg");
            largeImageName = "Step 4_E.jpg";

        }


        private async void btnStep0405Technically_Click(object sender, RoutedEventArgs e)
        {
            resetSelectedScreenButtons();
            imgStep0405Technically.Source = redHotspot;
            ellipsesIndex = 0;
            stackStep4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            await refreshEllipses4();
        }

        //private async void btnStep0406View3d_Click(object sender, RoutedEventArgs e)
        //{
        //    resetSelectedScreenButtons();
        //    imgStep0406View3d.Source = redHotspot;
        //    await setScreenImage("Step-4_E_Assess-final-ablation-zone-in-relationship-to-targeted-ablation-zone.png");
        //    largeImageName = "Step 4_E_Assess final ablation zone in relationship to targeted ablation zone.jpg";
        //    stackStep3.Visibility = Windows.UI.Xaml.Visibility.Visible;
        //}

        private async void btnShowImageViewer_Click(object sender, RoutedEventArgs e)
        {
            string largeImagePath = baseLargePath + largeImageName;
            Uri uri = await EtherFileFinder.uriForPath(largeImagePath);
            BitmapImage image = new BitmapImage(uri);
            imgImageViewer.Source = image;
            gridImageViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void btnCloseImageViewer_Click(object sender, RoutedEventArgs e)
        {
            gridImageViewer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async void buttonLeftArrow3_Click(object sender, RoutedEventArgs e)
        {
            int orig = ellipsesIndex;
            ellipsesIndex = Math.Max(0, ellipsesIndex - 1);
            if (orig != ellipsesIndex) await refreshEllipses3();
        }

        private async void buttonRightArrow3_Click(object sender, RoutedEventArgs e)
        {
            int orig = ellipsesIndex;
            ellipsesIndex = Math.Min(2, ellipsesIndex + 1);
            if (orig != ellipsesIndex) await refreshEllipses3();
        }

        private async Task refreshEllipses3()
        {
            if (!isViewingSuccessfulImages)
            {
                //set unsuccessful checkbox as CHECKED
                imgUnsuccessfulChecked3.Visibility = Windows.UI.Xaml.Visibility.Visible;
                imgUnsuccessfulUnchecked3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                //set successful checkbox as UNCHECKED
                imgSuccessfulChecked3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                imgSuccessfulUnchecked3.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                //set unsuccessful checkbox as CHECKED
                imgUnsuccessfulChecked3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                imgUnsuccessfulUnchecked3.Visibility = Windows.UI.Xaml.Visibility.Visible;
                //set successful checkbox as UNCHECKED
                imgSuccessfulChecked3.Visibility = Windows.UI.Xaml.Visibility.Visible;
                imgSuccessfulUnchecked3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            SolidColorBrush gray = new SolidColorBrush(Colors.Black);
            SolidColorBrush clear = new SolidColorBrush(Colors.Transparent);
            if (ellipsesIndex == 0) ellipseLeft3.Fill = gray;
            else ellipseLeft3.Fill = clear;

            if (ellipsesIndex == 1) ellipseMiddle3.Fill = gray;
            else ellipseMiddle3.Fill = clear;

            if (ellipsesIndex == 2) ellipseRight3.Fill = gray;
            else ellipseRight3.Fill = clear;

            switch (ellipsesIndex)
            {
                case 0: 
                    textEllipsesDesc3.Text = Translator.Current.translatedTextForKey("AblationConfirmationTab4Text11");//"Axial/Sagittal/Coronal/3D";
                    if (!isViewingSuccessfulImages)
                    {
                        await setScreenImage("Step 3_D1_miss.jpg");
                        largeImageName = "Step 3_D1_miss.jpg";
                    }
                    else
                    {
                        await setScreenImage("Step 3_D1_hit.jpg");
                        largeImageName = "Step 3_D1_hit.jpg";
                    }
                    break;
                case 1: 
                    textEllipsesDesc3.Text = Translator.Current.translatedTextForKey("AblationConfirmationTab4Text12");//"3D Zoom";
                    if (!isViewingSuccessfulImages)
                    {
                        await setScreenImage("Step 3_D2_miss.jpg");
                        largeImageName = "Step 3_D2_miss.jpg";
                    }
                    else
                    {
                        await setScreenImage("Step 3_D2_hit.jpg");
                        largeImageName = "Step 3_D2_hit.jpg";
                    }
                    break;
                case 2:
                    textEllipsesDesc3.Text = Translator.Current.translatedTextForKey("AblationConfirmationTab4Text13");//"Needle/Periscope";
                    if (!isViewingSuccessfulImages)
                    {
                        await setScreenImage("Step 3_D3_miss.jpg");
                        largeImageName = "Step 3_D3_miss.jpg";
                    }
                    else
                    {
                        await setScreenImage("Step 3_D3_hit.jpg");
                        largeImageName = "Step 3_D3_hit.jpg";
                    }
                    break;
            }
        }

        private async void buttonLeftArrow4_Click(object sender, RoutedEventArgs e)
        {
            ellipsesIndex = Math.Max(0, ellipsesIndex - 1);
            await refreshEllipses4();
        }

        private async void buttonRightArrow4_Click(object sender, RoutedEventArgs e)
        {
            ellipsesIndex = Math.Min(1, ellipsesIndex + 1);
            await refreshEllipses4();
        }

        private async Task refreshEllipses4()
        {

            if (!isViewingSuccessfulImages)
            {
                //set unsuccessful checkbox as CHECKED
                imgUnsuccessfulChecked4.Visibility = Windows.UI.Xaml.Visibility.Visible;
                imgUnsuccessfulUnchecked4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                //set successful checkbox as UNCHECKED
                imgSuccessfulChecked4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                imgSuccessfulUnchecked4.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                //set unsuccessful checkbox as CHECKED
                imgUnsuccessfulChecked4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                imgUnsuccessfulUnchecked4.Visibility = Windows.UI.Xaml.Visibility.Visible;
                //set successful checkbox as UNCHECKED
                imgSuccessfulChecked4.Visibility = Windows.UI.Xaml.Visibility.Visible;
                imgSuccessfulUnchecked4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }

            SolidColorBrush gray = new SolidColorBrush(Colors.Black);
            SolidColorBrush clear = new SolidColorBrush(Colors.Transparent);
            if (ellipsesIndex == 0) ellipseLeft4.Fill = gray;
            else ellipseLeft4.Fill = clear;

            if (ellipsesIndex == 1) ellipseRight4.Fill = gray;
            else ellipseRight4.Fill = clear;

            switch (ellipsesIndex)
            {
                case 0:
                    textEllipsesDesc4.Text = Translator.Current.translatedTextForKey("AblationConfirmationTab5Text12");//"Axial/Sagittal/Coronal/3D";
                    if (!isViewingSuccessfulImages)
                    {
                        await setScreenImage("Step 4_F1_miss.jpg");
                        largeImageName = "Step 4_F1_miss.jpg";
                    }
                    else
                    {
                        await setScreenImage("Step 4_F1_hit.jpg");
                        largeImageName = "Step 4_F1_hit.jpg";
                    }
                    break;
                case 1:
                    textEllipsesDesc4.Text = Translator.Current.translatedTextForKey("AblationConfirmationTab5Text13");//"3D zoom";
                    if (!isViewingSuccessfulImages)
                    {
                        await setScreenImage("Step 4_F2_miss.jpg");
                        largeImageName = "Step 4_F2_miss.jpg";
                    }
                    else
                    {
                        await setScreenImage("Step 4_F2_hit.jpg");
                        largeImageName = "Step 4_F2_hit.jpg";
                    }
                    break;
            }


        }



        public void cleanUp()
        {
            gridScreenViewer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            imageAblationConfirmationBackground.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackStep3.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackStep4.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async void btnSuccessfulCheckbox3_Click(object sender, RoutedEventArgs e)
        {
            isViewingSuccessfulImages = true;
            await refreshEllipses3();
        }

        private async void btnUnsuccessfulCheckbox3_Click(object sender, RoutedEventArgs e)
        {
            isViewingSuccessfulImages = false;
            await refreshEllipses3();
        }

        private async void btnSuccessfulCheckbox4_Click(object sender, RoutedEventArgs e)
        {
            isViewingSuccessfulImages = true;
            await refreshEllipses4();
        }

        private async void btnUnsuccessfulCheckbox4_Click(object sender, RoutedEventArgs e)
        {
            isViewingSuccessfulImages = false;
            await refreshEllipses4();
        }

        public async Task<List<File>> getCurrentFiles()
        {
            List<File> files = new List<File>();
            File file;

            int i = 1;
            switch (currentTab)
            {
                case ConfirmationTabs.None: //landing page
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Ablation Confirmation workflow]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_FULL.mp4");
                    file.isACVideo = true;
                    files.Add(file);
                    break;
                case ConfirmationTabs.Setup:
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Ablation Confirmation Set-up]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Setup.mp4");
                    file.isACVideo = true;
                    files.Add(file);
                    break;
                case ConfirmationTabs.Step1:
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Ablation Confirmation Step 1]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step01.mp4");
                    file.isACVideo = true;
                    files.Add(file);
                    break;
                case ConfirmationTabs.Step2:
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Ablation Confirmation Step 2]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step02.mp4");
                    file.isACVideo = true;
                    files.Add(file);
                    break;
                case ConfirmationTabs.Step3:
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Ablation Confirmation Step 3]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step03.mp4");
                    file.isACVideo = true;
                    files.Add(file);
                    break;
                case ConfirmationTabs.Step4:
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Ablation Confirmation Step 4]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Step04.mp4");
                    file.isACVideo = true;
                    files.Add(file);
                    break;
                case ConfirmationTabs.Close:
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Ablation Confirmation Close]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/NW_AblationConfirmationUpdate_v04_Close.mp4");
                    file.isACVideo = true;
                    files.Add(file);
                    break;
            }

            return files;
        }


    }
}
