﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlPowerDistribution : UserControl
    {

        public Grid gridPowerDistribution
        {
            get { return gridMain; }
        }
        public StackPanel stackPowerDistribution
        {
            get { return stackMain; }
        }

        public SubpageControlPowerDistribution()
        {
            this.InitializeComponent();
            translatePage();
        }

        private void translatePage()
        {
            Translator.Current.styleTextBlockWithKey(PowerDistributionText1, "PowerDistributionText1");
            Translator.Current.styleTextBlockWithKey(PowerDistributionText2, "PowerDistributionText2");
            Translator.Current.styleTextBlockWithKey(PowerDistributionText3, "PowerDistributionText3");
            Translator.Current.styleTextBlockWithKey(PowerDistributionText4, "PowerDistributionText4");
            Translator.Current.styleTextBlockWithKey(PowerDistributionText5, "PowerDistributionText5");
            Translator.Current.styleTextBlockWithKey(PowerDistributionText6, "PowerDistributionText6");
            Translator.Current.styleTextBlockWithKey(PowerDistributionText7, "PowerDistributionText7");
            Translator.Current.styleTextBlockWithKey(PowerDistributionText8, "PowerDistributionText8");
        }

        public async Task<List<File>> getCurrentFiles()
        {
            List<File> files = new List<File>();
            File file;

            file = new File();
            file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Microwave vs Radiofrequency]");
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Microwave vs Radiofrequency.mp4");
            files.Add(file);
            return files;
        }

    }
}
