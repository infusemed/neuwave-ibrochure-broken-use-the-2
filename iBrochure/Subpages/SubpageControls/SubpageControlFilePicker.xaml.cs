﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlFilePicker : UserControl
    {

        private Page _parentPage;
        public Page ParentPage
        {
            get { return _parentPage; }
            set { _parentPage = value; }
        }

        public SubpageControlFilePicker()
        {
            this.InitializeComponent();
            Translator.Current.styleTextBlockWithKey(FilePickerText1, "FilePickerText1");
        }

        public void displayFiles(List<File> files)
        {
            stackFiles.Children.Clear();
            foreach (File file in files)
            {
                if (stackFiles.Children.Count > 0)
                {
                    Border seperator = new Border();
                    seperator.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
                    seperator.BorderThickness = new Thickness(5);
                    seperator.BorderBrush = ColorToBrush("#707070");
                    seperator.Height = 2;
                    seperator.Margin = new Thickness(108, 0, 25, 0);
                    stackFiles.Children.Add(seperator);
                }

                //make copy of button template
                Button btnNewFile = new Button();
                btnNewFile.Style = Resources["FileButton"] as Style;
                StackPanel stackButton = new StackPanel();
                stackButton.Orientation = Orientation.Horizontal;
                Image icon = new Image();
                icon.Height = 40;
                icon.Width = 40;
                icon.Margin = new Thickness(10, 20, 0, 20);
                switch (file.fileType())
                {
                    case File.FileType.Video:
                        icon.Source = new BitmapImage(new Uri("ms-appx:///Assets/ImagesRebrand/System Overview/Probe_Portfolio/Multi-Probe Synchrony/Play Button.png"));
                        break;
                    case File.FileType.PDF:
                        icon.Source = new BitmapImage(new Uri("ms-appx:///Assets/ImagesRebrand/System Overview/Probe_Portfolio/Multi-Probe Synchrony/PDF Button.png"));
                        break;
                    default: //File.FileType.Image or Unknown
                        icon.Source = new BitmapImage(new Uri("ms-appx:///Assets/ImagesRebrand/System Overview/Probe_Portfolio/Probe Design/Image Button.png"));
                        break;
                }
                stackButton.Children.Add(icon);
                TextBlock caption = new TextBlock();
                caption.Foreground = new SolidColorBrush(Colors.Black);
                caption.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
                caption.FontSize = 24;
                caption.Margin = new Thickness(20, 0, 0, 0);
                caption.Text = file.displayName;
                stackButton.Children.Add(caption);
                btnNewFile.Content = stackButton;
                btnNewFile.Tag = file;
                btnNewFile.Click += btnFile_Click;
                stackFiles.Children.Add(btnNewFile);

            }
        }
        public static Brush ColorToBrush(string color) // color = "#E7E44D"
        {
            color = color.Replace("#", "");
            if (color.Length == 6)
            {
                return new SolidColorBrush(ColorHelper.FromArgb(255,
                    byte.Parse(color.Substring(0, 2), System.Globalization.NumberStyles.HexNumber),
                    byte.Parse(color.Substring(2, 2), System.Globalization.NumberStyles.HexNumber),
                    byte.Parse(color.Substring(4, 2), System.Globalization.NumberStyles.HexNumber)));
            }
            else
            {
                return null;
            }
        }

        private void btnFile_Click(object sender, RoutedEventArgs e)
        {
            Button btn = ((Button)sender);
            File file = ((File)btn.Tag);
            ((SystemOverview)_parentPage).requestDisplayFile(file);
        }
    }

}
