﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlCooling : UserControl
    {
        public Grid gridCooling
        {
            get { return gridMain; }
        }
        public StackPanel stackCooling
        {
            get { return stackMain; }
        }

        public SubpageControlCooling()
        {
            this.InitializeComponent();
            translatePage();
        }

        public void translatePage()
        {
            Translator.Current.styleTextBlockWithKey(CoolingText1, "CoolingText1");
            Translator.Current.styleTextBlockWithKey(CoolingText2, "CoolingText2");
            Translator.Current.styleTextBlockWithKey(CoolingText3, "CoolingText3");
            Translator.Current.styleTextBlockWithKey(CoolingText4, "CoolingText4");
            Translator.Current.styleTextBlockWithKey(CoolingText5, "CoolingText5");
            Translator.Current.styleTextBlockWithKey(CoolingText6, "CoolingText6");
        }

        public async Task<List<File>> getCurrentFiles()
        {
            List<File> files = new List<File>();
            File file;

            file = new File();
            file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Tissu-Loc™ ex-vivo]");
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Cooling_Tissu-Loc.mp4");
            files.Add(file);
            file = new File();
            file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Tissu-Loc™ lung animation]");
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Breathing_Loop_v05.mp4");
            files.Add(file);
            file = new File();
            file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Comet tail]");
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/cometTail.png");
            file.width = 1085;
            file.height = 885;
            files.Add(file);
            file = new File();
            file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[17 gauge vs 13 gauge]");
            file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/17guageVS13gauge.png");
            file.width = 1157;
            file.height = 885;
            files.Add(file);
            return files;
        }

    }
}
