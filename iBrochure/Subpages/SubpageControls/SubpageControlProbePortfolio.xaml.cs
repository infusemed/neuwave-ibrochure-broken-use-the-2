﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlProbePortfolio : UserControl
    {

        private Page _parentPage;
        public Page ParentPage
        {
            get { return _parentPage; }
            set { _parentPage = value; }
        }

        public Grid gridProbePortfolio
        {
            get { return gridMain; }
        }

        public Grid gridProbePortfolioTabs
        {
            get { return gridTabs; }
        }


        public StackPanel stackProbePortfolioOverview
        {
            get { return stackProbePortfolio1; }
        }

        public Grid gridProbePortfolioPortfolio
        {
            get { return gridProbePortfolio2; }
        }
        public StackPanel stackProbePortfolioPortfolioPageIndicators
        {
            get { return stackProbePortfolio2PageIndicators; }
        }
        public StackPanel stackProbePortfolioPortfolioA
        {
            get { return stackProbePortfolio2a; }
        }
        public StackPanel stackProbePortfolioPortfolioAIllustrations
        {
            get { return stackProbePortfolio2aIllustrations; }
        }
        public StackPanel stackProbePortfolioPortfolioB
        {
            get { return stackProbePortfolio2b; }
        }
        public StackPanel stackProbePortfolioPortfolioBIllustrations
        {
            get { return stackProbePortfolio2bIllustrations; }
        }

        public Grid gridProbePortfolioDesign
        {
            get { return gridProbePortfolio3; }
        }
        public StackPanel stackProbePortfolioDesign
        {
            get { return stackProbePortfolio3; }
        }
        public StackPanel stackProbePortfolioDesignIllustrations
        {
            get { return stackProbePortfolio3Illustrations; }
        }

        public Grid gridProbePortfolioMultiProbeSync
        {
            get { return gridProbePortfolio4; }
        }
        public StackPanel stackProbePortfolioMultiProbeSync
        {
            get { return stackProbePortfolio4; }
        }
        public StackPanel stackProbePortfolioMultiProbeSyncIllustrations
        {
            get { return stackProbePortfolio4Illustrations; }
        }

        private enum ViewType
        {
            Overview,
            PR_Probe,
            Max_Probe,
            Probe_Design,
            Multi_Sync
        }

        private ViewType _currentlyViewing;
        
        public SubpageControlProbePortfolio()
        {
            this.InitializeComponent();
            translatePage();
            btnOverview_Click(null, null);
        }

        private void translatePage()
        {
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text1, "ProbePortfolioTab1Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text1b, "ProbePortfolioTab1Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text2, "ProbePortfolioTab1Text2");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text3, "ProbePortfolioTab1Text3");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text4, "ProbePortfolioTab1Text4");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text5, "ProbePortfolioTab1Text5");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text6, "ProbePortfolioTab1Text6");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text7, "ProbePortfolioTab1Text7");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab1Text8, "ProbePortfolioTab1Text8");


            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text1, "ProbePortfolioTab2Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text1b, "ProbePortfolioTab2Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text2, "ProbePortfolioTab2Text2");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text3, "ProbePortfolioTab2Text3");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text4, "ProbePortfolioTab2Text4");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text5, "ProbePortfolioTab2Text5");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text6, "ProbePortfolioTab2Text6");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text7, "ProbePortfolioTab2Text7");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text8, "ProbePortfolioTab2Text8");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text9, "ProbePortfolioTab2Text9");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text10, "ProbePortfolioTab2Text10");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text11, "ProbePortfolioTab2Text11");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text12, "ProbePortfolioTab2Text12");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text13, "ProbePortfolioTab2Text13");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text14, "ProbePortfolioTab2Text14");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text15, "ProbePortfolioTab2Text15");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text16, "ProbePortfolioTab2Text16");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text17, "ProbePortfolioTab2Text17");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text18, "ProbePortfolioTab2Text18");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text19, "ProbePortfolioTab2Text19");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text20, "ProbePortfolioTab2Text20");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text21, "ProbePortfolioTab2Text21");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text22, "ProbePortfolioTab2Text22");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text23, "ProbePortfolioTab2Text23");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text24, "ProbePortfolioTab2Text24");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text25, "ProbePortfolioTab2Text25");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text26, "ProbePortfolioTab2Text26");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text27, "ProbePortfolioTab2Text27");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text28, "ProbePortfolioTab2Text28");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text29, "ProbePortfolioTab2Text29");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text30, "ProbePortfolioTab2Text30");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text31, "ProbePortfolioTab2Text31");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text32, "ProbePortfolioTab2Text32");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text33, "ProbePortfolioTab2Text33");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text34, "ProbePortfolioTab2Text34");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text35, "ProbePortfolioTab2Text35");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text36, "ProbePortfolioTab2Text36");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text37, "ProbePortfolioTab2Text37");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text38, "ProbePortfolioTab2Text38");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text39, "ProbePortfolioTab2Text39");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text40, "ProbePortfolioTab2Text40");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text41, "ProbePortfolioTab2Text41");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text42, "ProbePortfolioTab2Text42");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text43, "ProbePortfolioTab2Text43");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text44, "ProbePortfolioTab2Text44");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text45, "ProbePortfolioTab2Text45");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text46, "ProbePortfolioTab2Text46");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text47, "ProbePortfolioTab2Text47");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text48, "ProbePortfolioTab2Text48");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text49, "ProbePortfolioTab2Text49");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text50, "ProbePortfolioTab2Text50");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab2Text51, "ProbePortfolioTab2Text51");

            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text1, "ProbePortfolioTab3Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text1b, "ProbePortfolioTab3Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text2, "ProbePortfolioTab3Text2");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text3, "ProbePortfolioTab3Text3");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text4, "ProbePortfolioTab3Text4");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text5, "ProbePortfolioTab3Text5");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text6, "ProbePortfolioTab3Text6");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text7, "ProbePortfolioTab3Text7");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text8, "ProbePortfolioTab3Text8");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text9, "ProbePortfolioTab3Text9");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text10, "ProbePortfolioTab3Text10");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text11, "ProbePortfolioTab3Text11");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text12, "ProbePortfolioTab3Text12");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text13, "ProbePortfolioTab3Text13");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab3Text14, "ProbePortfolioTab3Text14");


            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text1, "ProbePortfolioTab4Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text1b, "ProbePortfolioTab4Text1");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text2, "ProbePortfolioTab4Text2");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text3, "ProbePortfolioTab4Text3");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text4, "ProbePortfolioTab4Text4");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text5, "ProbePortfolioTab4Text5");
            Translator.Current.styleTextBoxWithKey(ProbePortfolioTab4Text6, "ProbePortfolioTab4Text6");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text7, "ProbePortfolioTab4Text7");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text8, "ProbePortfolioTab4Text8");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text9, "ProbePortfolioTab4Text9");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text10, "ProbePortfolioTab4Text10");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text11, "ProbePortfolioTab4Text11");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text12, "ProbePortfolioTab4Text12");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text13, "ProbePortfolioTab4Text13");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text14, "ProbePortfolioTab4Text14");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text15, "ProbePortfolioTab4Text15");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text16, "ProbePortfolioTab4Text16");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text17, "ProbePortfolioTab4Text17");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text18, "ProbePortfolioTab4Text18");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text19, "ProbePortfolioTab4Text19");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text20, "ProbePortfolioTab4Text20");
            Translator.Current.styleTextBlockWithKey(ProbePortfolioTab4Text21, "ProbePortfolioTab4Text21");



        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            stackProbePortfolio2ATransform.X = 0;
            stackProbePortfolio2AIllustrationsTransform.X = 0;
            stackProbePortfolio2BTransform.X = 1542;
            stackProbePortfolio2BIllustrationsTransform.X = 1542;
            //stackProbePortfolio2CTransform.X = 1542;
            btnOverview_Click(null, null);
            gridBackground.Opacity = 0.0f;
        }

        private void btnProbeDesign_Click(object sender, RoutedEventArgs e)
        {
            bool alreadyViewing = btnProbeDesignON.Visibility == Windows.UI.Xaml.Visibility.Visible;

            if (alreadyViewing)
            {
                //nothing
            }
            else
            {
                gridBackground.Background = new SolidColorBrush(Windows.UI.Colors.White);
                fadeBackgroundIN.Begin();
                gridProbePortfolio2.Background = null;
                hideProbePortfolioSubPages();
                hideProbePortfolioTabs();
                decloakGridChildren(stackProbePortfolioDesign, 0, 0);
                decloakGridChildren(stackProbePortfolioDesignIllustrations, 0, 0);
                btnProbeDesignON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                ((SystemOverview)ParentPage).setPlusVisible(true);
                ellipseLeft.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                ellipseRight.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        private void btnProbePortfolio_Click(object sender, RoutedEventArgs e)
        {
            bool alreadyViewing = btnProbePortfolioON.Visibility == Windows.UI.Xaml.Visibility.Visible;
            currentPortfolio2Index = 1;

            if (alreadyViewing)
            {
                if (stackProbePortfolio2ATransform.X == 0)
                {
                    stackProbePortfolio2ATransform.X = 100;
                    stackProbePortfolio2AIllustrationsTransform.X = 100;
                }
                else if (stackProbePortfolio2BTransform.X == 0)
                {
                    pp2BRight.Begin();
                    //stackProbePortfolio2CTransform.X = 1542;
                }
                //else if (stackProbePortfolio2CTransform.X == 0)
                //{
                //    pp2CRight.Begin();
                //    stackProbePortfolio2BTransform.X = 1542;
                //}
                pp2AShow.Begin();
            }
            else
            {
                gridBackground.Background = new SolidColorBrush(Windows.UI.Colors.White);
                fadeBackgroundIN.Begin();
                gridProbePortfolio2.Background = new SolidColorBrush(Windows.UI.Colors.Transparent);
                hideProbePortfolioSubPages();
                hideProbePortfolioTabs();
                stackProbePortfolio2ATransform.X = 0;
                stackProbePortfolio2AIllustrationsTransform.X = 0;
                stackProbePortfolio2BTransform.X = 1542;
                stackProbePortfolio2BIllustrationsTransform.X = 1542;
                //stackProbePortfolio2CTransform.X = 1542;
                decloakGridChildren(stackProbePortfolioPortfolioA, 0, 0);
                decloakGridChildren(stackProbePortfolioPortfolioAIllustrations, 0, 0);
                decloakGridChildren(stackProbePortfolioPortfolioB, 0, 0);
                decloakGridChildren(stackProbePortfolioPortfolioBIllustrations, 0, 0);
                decloakGridChildren(stackProbePortfolioPortfolioPageIndicators, 0, 0);
                //decloakGridChildren(stackProbePortfolioPortfolioC, 0, 0);
                btnProbePortfolioON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                ((SystemOverview)ParentPage).setPlusVisible(true);
            }
            stackProbePortfolio2aIllustrations.Opacity = 1;
            stackProbePortfolio2bIllustrations.Opacity = 0;
            stackProbePortfolio2a.Opacity = 1;
            stackProbePortfolio2b.Opacity = 0;

        }

        private void btnMultiProbeSync_Click(object sender, RoutedEventArgs e)
        {
            bool alreadyViewing = btnMultiProbeSyncON.Visibility == Windows.UI.Xaml.Visibility.Visible;

            if (alreadyViewing)
            {
                //nothing
            }
            else
            {
                gridBackground.Background = new SolidColorBrush(Windows.UI.Colors.White);
                fadeBackgroundIN.Begin();
                gridProbePortfolio2.Background = null;
                hideProbePortfolioSubPages();
                hideProbePortfolioTabs();
                decloakGridChildren(stackProbePortfolioMultiProbeSync, 0, 0);
                decloakGridChildren(stackProbePortfolioMultiProbeSyncIllustrations, 0, 0);
                btnMultiProbeSyncON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                ((SystemOverview)ParentPage).setPlusVisible(true);


            }
            //Rect r = ProbePortfolioTab4Text6.GetRectFromCharacterIndex(ProbePortfolioTab4Text6.Text.Length - 1, false);
            //ProbePortfolioTab4Text21.Margin = new Thickness(r.X + ProbePortfolioTab4Text6.Margin.Left + 15, r.Y+5, 0, 0);
        }

        private void hideProbePortfolioSubPages()
        {
            cloakGridChildren(stackProbePortfolioOverview, 0, 0);
            cloakGridChildren(stackProbePortfolioPortfolioA, 0, 0);
            cloakGridChildren(stackProbePortfolioPortfolioAIllustrations, 0, 0);
            cloakGridChildren(stackProbePortfolioPortfolioB, 0, 0);
            cloakGridChildren(stackProbePortfolioPortfolioBIllustrations, 0, 0);
            cloakGridChildren(stackProbePortfolioPortfolioPageIndicators, 0, 0);
            cloakGridChildren(stackProbePortfolioDesign, 0, 0);
            cloakGridChildren(stackProbePortfolioDesignIllustrations, 0, 0);
            cloakGridChildren(stackProbePortfolioMultiProbeSync, 0, 0);
            cloakGridChildren(stackProbePortfolioMultiProbeSyncIllustrations, 0, 0);
        }

        private void hideProbePortfolioTabs()
        {
            btnOverviewON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnProbeDesignON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnProbePortfolioON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnMultiProbeSyncON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async void cloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            if (grid.Children.Count > 0)
            {
                //get reverse to cloak in reverse order
                IList<UIElement> reverseList = new List<UIElement>();
                for (int i = grid.Children.Count - 1; i >= 0; i--) //UIElement elem in grid.Children
                {
                    reverseList.Add(grid.Children.ElementAt(i));
                }

                //make copy of children and move to tag
                IList<UIElement> transferList = new List<UIElement>();
                foreach (UIElement elem in grid.Children)
                {
                    transferList.Add(elem);
                }
                grid.Tag = transferList;

                if (staggerDelayMilliseconds > 0)
                {
                    //remove children in reverse order
                    foreach (UIElement elem in reverseList)
                    {
                        //await Task.Delay(rnd.Next(50, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                        grid.Children.Remove(elem);
                    }
                }
                else
                {
                    grid.Children.Clear();
                }

            }
        }

        private async void decloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            grid.Visibility = Windows.UI.Xaml.Visibility.Visible; //ensure visible
            if (grid.Tag != null && grid.Children.Count == 0)
            {
                IList<UIElement> list = grid.Tag as List<UIElement>;
                foreach (UIElement elem in list)
                {
                    if (staggerDelayMilliseconds > 0)
                    {
                        //await Task.Delay(rnd.Next(50, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                    }
                    grid.Children.Add(elem);
                }
            }
        }

        private Point initialpoint;
        private bool swipeHandled;
        private int currentPortfolio2Index = 1;
        private void probePortfolio2_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            initialpoint = e.Position;
            swipeHandled = false;
        }

        private void probePortfolio2_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            int threshhold = 100;
            if (swipeHandled)
            {
                e.Complete();
                return;
            }
            if (e.IsInertial)
            {
                Point currentpoint = e.Position;
                double swipeRightDiff = currentpoint.X - initialpoint.X;
                double swipeLeftDiff = initialpoint.X - currentpoint.X;
                if (swipeLeftDiff >= threshhold)
                {
                    //leftSwipe
                    //Increase Index
                    currentPortfolio2Index = Math.Min(currentPortfolio2Index + 1, 2);
                    switch (currentPortfolio2Index)
                    {
                        case 2:
                            {
                                pp2ALeft.Begin();
                                //stackProbePortfolio2CTransform.X = 1542;
                                pp2BShow.Begin();
                                ellipseLeft.Visibility = Windows.UI.Xaml.Visibility.Visible;
                                ellipseRight.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                                break;
                            }
                        //case 3:
                        //    {
                        //        pp2BLeft.Begin();
                        //        stackProbePortfolio2ATransform.X = -1542;
                        //        pp2CShow.Begin();
                        //        break;
                        //    }
                    }


                    swipeHandled = true;
                }
                if (swipeRightDiff >= threshhold)
                {
                    //RightSwipe
                    //Decrease Index
                    currentPortfolio2Index = Math.Max(currentPortfolio2Index - 1, 1);
                    switch (currentPortfolio2Index)
                    {
                        case 1:
                            {
                                pp2BRight.Begin();
                                //stackProbePortfolio2CTransform.X = 1542;
                                pp2AShow.Begin();
                                ellipseLeft.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                                ellipseRight.Visibility = Windows.UI.Xaml.Visibility.Visible;
                                break;
                            }
                        //case 2:
                        //    {
                        //        pp2CRight.Begin();
                        //        stackProbePortfolio2ATransform.X = -1542;
                        //        pp2BShow.Begin();
                        //        break;
                        //    }
                    }
                    swipeHandled = true;
                }
            }
        }

        private void btnProbePR_Click(object sender, RoutedEventArgs e)
        {
            btnProbePortfolio_Click(sender, e);
            currentPortfolio2Index = 1;
            //pp2BRight.Begin();
            //stackProbePortfolio2ATransform.X = 0;
            stackProbePortfolio2BTransform.X = 1542;
            stackProbePortfolio2BIllustrationsTransform.X = 1542;
            //pp2AShow.Begin();
            ellipseLeft.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            ellipseRight.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void btnProbeLKLN_Click(object sender, RoutedEventArgs e)
        {
            btnProbePortfolio_Click(sender, e);
            stackProbePortfolio2aIllustrations.Opacity = 0;
            stackProbePortfolio2bIllustrations.Opacity = 1;
            stackProbePortfolio2a.Opacity = 0;
            stackProbePortfolio2b.Opacity = 1;
            currentPortfolio2Index = 2;
            //pp2ALeft.Begin();
            stackProbePortfolio2ATransform.X = -1542;
            stackProbePortfolio2AIllustrationsTransform.X = -1542;
            stackProbePortfolio2BTransform.X = 0;
            stackProbePortfolio2BIllustrationsTransform.X = 0;
            //pp2BShow.Begin();
            ellipseLeft.Visibility = Windows.UI.Xaml.Visibility.Visible;
            ellipseRight.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        public void cleanUpForDismiss()
        {
            gridBackground.Background = null;
            gridProbePortfolio2.Background = null;
        }

        public void resetView()
        {
            btnOverview_Click(null, null);
        }

        private void btnOverview_Click(object sender, RoutedEventArgs e)
        {
            bool alreadyViewing = btnOverviewON.Visibility == Windows.UI.Xaml.Visibility.Visible;
            
            if (ParentPage != null)
            {
                ((SystemOverview)ParentPage).setPlusVisible(false);
            }

            if (alreadyViewing)
            {
                //nothing
            }
            else
            {
                fadeBackgroundOUT.Begin();
                gridProbePortfolio2.Background = null;
                hideProbePortfolioSubPages();
                hideProbePortfolioTabs();
                decloakGridChildren(stackProbePortfolioOverview, 0, 0);
                btnOverviewON.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        public async Task<List<File>> getCurrentFiles()
        {
            List<File> files = new List<File>();
            File file;

            if (btnProbePortfolioON.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                if (stackProbePortfolio2AIllustrationsTransform.X == 0)
                {
                    file = new File();
                    //file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[HCC - Single PR probe case";
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Liver Lesion - Single PR probe case]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/1 NEUWAVE PR PROBE.pdf");
                    files.Add(file);
                    file = new File();
                    //file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[RCC - 2 PR probe case";
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Kidney Lesion - 2 PR probe case]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/2 PR PROBES A.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[PR Time and Power]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Time and Power PR.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[PR XT Time and Power]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Time and Power PR XT.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[PR vs LK]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/PR vs LK.mp4");
                    files.Add(file);
                }
                else if (stackProbePortfolio2BIllustrationsTransform.X == 0)
                {
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Single LK probe case]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/1 NEUWAVE LK PROBE.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Single LN probe case]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/1 NEUWAVE LN PROBE.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[LK Time and Power]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Time and Power LK.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[LK XT Time and Power]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Time and Power LK XT.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[LN Time and Power]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Time and Power LN.pdf");
                    files.Add(file);
                    file = new File();
                    file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[PR vs LK]");
                    file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/PR vs LK.mp4");
                    files.Add(file);
                }
            }
            else if (btnProbeDesignON.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[17 gauge vs 13 gauge]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/17guageVS13gauge.png");
                file.width = 1157;
                file.height = 885;
                files.Add(file);
            }
            else if (btnMultiProbeSyncON.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Multi-Probe Synchrony vs Repositioning]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/3 probe repositioning_and_synchronyV2.mp4");
                files.Add(file);
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[In-Phase Waves vs Multiple Generators]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/In Phase Waves Asset.pdf");
                files.Add(file);
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Thermal Synergy vs Repositioning]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/Thermal Synergy Asset.pdf");
                files.Add(file);
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Kidney Lesion - 2 PR probe case]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/2 PR PROBES A.pdf");
                files.Add(file);
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Liver Lesion - 2 PR probe case]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/2 PR PROBES B.pdf");
                files.Add(file);
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Kidney Lesion - 3 PR probe case]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/3 PRECISION PROBES.pdf");
                files.Add(file);
                file = new File();
                file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Liver Lesion - 3 LK probe case]");
                file.storageFile = await EtherFileFinder.storageFileForPath("ms-appx:///Assets/Files/3 NEUWAVE LK PROBES.pdf");
                files.Add(file);

            }
            return files;
        }

        private void ProbePortfolioTab4Text6_Loaded(object sender, RoutedEventArgs e)
        {

            Rect r = ProbePortfolioTab4Text6.GetRectFromCharacterIndex(ProbePortfolioTab4Text6.Text.Length - 1, false);
            ProbePortfolioTab4Text21.Margin = new Thickness(r.X + ProbePortfolioTab4Text6.Margin.Left + 20, r.Y+6, 0, 0);
        }

    }
}
