﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace iBrochure.Subpages.SubpageControls
{
    public sealed partial class SubpageControlLandingPage : UserControl
    {

        public delegate void DeliveryEventHandler(object sender, RoutedEventArgs e);
        public event DeliveryEventHandler DeliveryClicked;
        public delegate void PowerEventHandler(object sender, RoutedEventArgs e);
        public event PowerEventHandler PowerClicked;

        public delegate void ProbeEventHandler(object sender, RoutedEventArgs e);
        public event ProbeEventHandler ProbeClicked;

        public delegate void CoolingEventHandler(object sender, RoutedEventArgs e);
        public event CoolingEventHandler CoolingClicked;

        public delegate void ConfirmationEventHandler(object sender, RoutedEventArgs e);
        public event ConfirmationEventHandler ConfirmationClicked;

        public Grid gridLandingPad
        {
            get { return gridMain; }
        }

        public SubpageControlLandingPage()
        {
            this.InitializeComponent();
            Translator.Current.styleTextBlockWithKey(SystemOverviewText1, "SystemOverviewText1");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText2, "SystemOverviewText2");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText3, "SystemOverviewText3");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText4, "SystemOverviewText4");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText5, "SystemOverviewText5");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText6, "SystemOverviewText6");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText7, "SystemOverviewText7");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText8, "SystemOverviewText8");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText9, "SystemOverviewText9");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText10, "SystemOverviewText10");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText11, "SystemOverviewText11");
            Translator.Current.styleTextBlockWithKey(SystemOverviewText12, "SystemOverviewText12");
        }

        private void btnDelivery_Click(object sender, RoutedEventArgs e)
        {
            DeliveryClicked(sender, e);
        }
        private void btnPower_Click(object sender, RoutedEventArgs e)
        {
            PowerClicked(sender, e);
        }
        private void btnProbe_Click(object sender, RoutedEventArgs e)
        {
            ProbeClicked(sender, e);
        }
        private void btnCooling_Click(object sender, RoutedEventArgs e)
        {
            CoolingClicked(sender, e);
        }
        private void btnConfirmation_Click(object sender, RoutedEventArgs e)
        {
            ConfirmationClicked(sender, e);
        }

        public List<File> getCurrentFiles()
        {
            List<File> files = new List<File>();
            //currently none, return empty list
            return files;
        }

    }
}
