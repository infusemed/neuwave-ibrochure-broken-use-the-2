﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Animation;
using System.Threading.Tasks;
using Windows.Storage;
using System.Threading;
using iBrochure.DocumentViewer;
using iBrochure.Classes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace iBrochure.Subpages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SystemOverview : Page
    {
        const string TEMPLATE_NAME = "[name]";
        const string I_TEMPLATE = "/Assets/Images/VideoStills/[name]";
        const string V_TEMPLATE = "/Assets/Videos/[name]";

        const string I_FIRST_GENERAL = "highrezstilltwomonitor.png";

        const string V_C02 = "C02.mp4";
        const string V_REVERSE_C02 = "C02_reverse.mp4";
        const string I_LAST_C02 = "C02.png";

        const string V_CONFIRMATION = "confirmation.mp4";
        const string V_REVERSE_CONFIRMATION = "confirmation_reverse.mp4";
        const string I_LAST_CONFIRMATION = "confirmation.png";

        const string V_DELIVERY = "delivery.mp4";
        const string V_REVERSE_DELIVERY = "delivery_reverse.mp4";
        const string I_LAST_DELIVERY = "delivery.png";

        const string V_POWER = "power.mp4";
        const string V_REVERSE_POWER = "power_reverse.mp4";
        const string I_LAST_POWER = "power.png";

        const string V_PROBE = "probe.mp4";
        const string V_REVERSE_PROBE = "probe_reverse.mp4";
        const string I_LAST_PROBE = "probe.png";

        const int TRANSITION_VIDEO_SPEED = 2; //1 = NORMAL PLAYBACK, 2 = DOUBLE SPEED, 0.000001 = SLOOOOOOOOW MOTION

        public enum GridType
        {
            LANDING_PAD,
            USER_INTERFACE,
            POWER_DISTRIBUTION,
            COOLING,
            PROBE_PORTFOLIO,
            ABLATION_CONFIRMATION
        }

        private GridType currentGrid = GridType.LANDING_PAD;
        bool disableNextAutoplay = false;
        Random rnd = new Random();
        bool disableNavigation;

        Uri currentVideoUri;

        int globalStaggerValue = 50;

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //unhide all controls
            controlLandingPage.Visibility = Windows.UI.Xaml.Visibility.Visible;
            controlUserInterface.Visibility = Windows.UI.Xaml.Visibility.Visible;
            controlPowerDistribution.Visibility = Windows.UI.Xaml.Visibility.Visible;
            controlCooling.Visibility = Windows.UI.Xaml.Visibility.Visible;
            controlAblationConfirmation.Visibility = Windows.UI.Xaml.Visibility.Visible;
            controlProbePortfolio.Visibility = Windows.UI.Xaml.Visibility.Visible;

            controlAblationConfirmation.ParentPage = this;
            controlUserInterface.ParentPage = this;
            controlProbePortfolio.ParentPage = this;
            controlFilePicker.ParentPage = this;

            cloakAllGridChildren(0, 0); 
            cloakGridChildren(gridBarsButtons, 0, 0); //not included in AllGridChildren Method -- controlled differently
            Transition addDelete = new AddDeleteThemeTransition();

            addTransitionsToPanelChildrenTag(controlLandingPage.gridLandingPad, addDelete);
            addTransitionsToPanelChildrenTag(controlUserInterface.stackAblationDelivery, addDelete);
            addTransitionsToPanelChildrenTag(controlUserInterface.stackTouchScreen, addDelete);


            //addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationLandingPage, addDelete);
            addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationSetup, addDelete);
            addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationStep1, addDelete);
            addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationStep2, addDelete);
            addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationStep3, addDelete);
            addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationStep4, addDelete);
            //addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationStep5, addDelete);
            addTransitionsToPanelChildrenTag(controlAblationConfirmation.stackAblationConfirmationClose, addDelete);

            addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioOverview, addDelete);
            addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioDesign, addDelete);
            //addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioDesignIllustrations, addDelete);
            addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioPortfolioA, addDelete);
            //addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioPortfolioAIllustrations, addDelete);
            addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioPortfolioB, addDelete);
            //addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioPortfolioBIllustrations, addDelete);
            addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioMultiProbeSync, addDelete);
            //addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioMultiProbeSyncIllustrations, addDelete);
            //addTransitionsToPanelChildrenTag(controlProbePortfolio.stackProbePortfolioPortfolioPageIndicators, addDelete);

            addTransitionsToPanelChildrenTag(controlPowerDistribution.stackPowerDistribution, addDelete);
            addTransitionsToPanelChildrenTag(controlCooling.stackCooling, addDelete);
            //addTransitionsToPanelChildrenTag(gridBarsButtons, addDelete);
            setBackgroundImage(currentGrid);
            disableNextAutoplay = true;
            playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_PROBE));
            decloakGridChildren(controlLandingPage.gridLandingPad, 0, 0);
            setPlusVisible(controlLandingPage.getCurrentFiles().Count() > 0);
            decloakGridChildren(gridBarsButtons, 0, 0);
            disableNavigation = false;
        }

        public SystemOverview()
        {
            this.InitializeComponent();
        }

        public void setPlusVisible(bool visible)
        {
            if (visible)
            {
                btnPlus.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                btnPlus.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }

        public void manualBackPress()
        {
            btnBack_Click(null, null);
        }

        private int attemptedBackCount;
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            if (disableNavigation)
            {
                attemptedBackCount++;
                if (attemptedBackCount <= 3)
                {
                    return;
                }
            }
            disableNavigation = true;
            switch (currentGrid)
            {
                case GridType.LANDING_PAD:
                    this.Frame.GoBack();
                    break;
                case GridType.ABLATION_CONFIRMATION:
                    playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_REVERSE_CONFIRMATION));
                    controlAblationConfirmation.stackAblationConfirmationLandingPage.Opacity = 0.0f;
                    controlAblationConfirmation.stackAblationConfirmationLandingPage.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    controlAblationConfirmation.cleanUp();
                    break;
                case GridType.USER_INTERFACE:
                    playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_REVERSE_DELIVERY));
                    break;
                case GridType.COOLING:
                    playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_REVERSE_C02));
                    break;
                case GridType.POWER_DISTRIBUTION:
                    playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_REVERSE_POWER));
                    break;
                case GridType.PROBE_PORTFOLIO:
                    controlProbePortfolio.cleanUpForDismiss();
                    playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_REVERSE_PROBE));
                    break;
            }
            currentGrid = GridType.LANDING_PAD;
            ;
        }

        private async void videoPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            attemptedBackCount = 0;
            if (!disableNextAutoplay)
            {
                cloakAllGridChildren(0, globalStaggerValue); 
                cloakGridChildren(gridBarsButtons, 0, 0); 
                videoPlayer.Play();
                videoPlayer.PlaybackRate = TRANSITION_VIDEO_SPEED;
            }
            else
            {
                disableNextAutoplay = false;
            }
        }

        private async void videoPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            setBackgroundImage(currentGrid);
            //decloakGridChildren(gridBarsButtons, 0, 0);
            disableNavigation = false;
            switch (currentGrid)
            {
                case GridType.LANDING_PAD:
                    decloakGridChildren(controlLandingPage.gridLandingPad, 0, globalStaggerValue);
                    setPlusVisible(controlLandingPage.getCurrentFiles().Count() > 0);
                    break;
                case GridType.USER_INTERFACE:
                    decloakGridChildren(controlUserInterface.gridAblationDelivery, 0, 0);
                    decloakGridChildren(controlUserInterface.stackAblationDelivery, 0, globalStaggerValue);
                    decloakGridChildren(controlUserInterface.stackTouchScreen, 0, globalStaggerValue);
                    setPlusVisible(controlUserInterface.getCurrentFiles().Count() > 0);
                    break;
                case GridType.ABLATION_CONFIRMATION:
                    decloakGridChildren(controlAblationConfirmation.gridAblationConfirmation, 0, 0);
                    //decloakGridChildren(controlAblationConfirmation.gridAblationConfirmationTabs, 0, 0);
                    //decloakGridChildren(controlAblationConfirmation.stackAblationConfirmationSetup, 0, globalStaggerValue);
                    //decloakGridChildren(controlAblationConfirmation.stackAblationConfirmationLandingPage, 0, globalStaggerValue);
                    //decloakGridChildren(controlAblationConfirmation.gridAblationConfirmationNavigationButtons, 0, globalStaggerValue);
                    setPlusVisible((await controlAblationConfirmation.getCurrentFiles()).Count() > 0);
                    //await Task.Delay(250);
                    controlAblationConfirmation.stackAblationConfirmationLandingPage.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    controlAblationConfirmation.animateLandingPageShow();
                    decloakGridChildren(gridBarsButtons, 0, 0);
                    controlAblationConfirmation.currentTab = SubpageControls.SubpageControlAblationConfirmation.ConfirmationTabs.None;
                    break;
                case GridType.POWER_DISTRIBUTION:
                    decloakGridChildren(controlPowerDistribution.gridPowerDistribution, 0, 0);
                    decloakGridChildren(controlPowerDistribution.stackPowerDistribution, 0, globalStaggerValue);
                    setPlusVisible((await controlPowerDistribution.getCurrentFiles()).Count() > 0);
                    break;
                case GridType.COOLING:
                    decloakGridChildren(controlCooling.gridCooling, 0, 0);
                    decloakGridChildren(controlCooling.stackCooling, 0, globalStaggerValue);
                    setPlusVisible((await controlCooling.getCurrentFiles()).Count() > 0);
                    break;
                case GridType.PROBE_PORTFOLIO:
                    decloakGridChildren(controlProbePortfolio.gridProbePortfolio, 0, 0);
                    decloakGridChildren(controlProbePortfolio.gridProbePortfolioTabs, 0, 0);
                    //decloakGridChildren(controlProbePortfolio.stackProbePortfolioDesign, 0, 0);
                    decloakGridChildren(controlProbePortfolio.stackProbePortfolioOverview, 0, 0);
                    setPlusVisible((await controlProbePortfolio.getCurrentFiles()).Count() > 0);
                    controlProbePortfolio.resetView();
                    break;
            }
        }

        private async void playVideo(string videoUriPath)
        {
            videoPlayer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            //currentVideoUri = new Uri(this.BaseUri, videoUriPath);
            //currentVideoUri = new Uri("", UriKind.Absolute);
            StorageFile video = await EtherFileFinder.storageFileForPath(videoUriPath);
            currentVideoUri = new Uri(this.BaseUri, video.Path);
            await setVideoSource();

            //await Task.Delay(1000);
            //if (!setSource.IsCompleted)
            //{
            //    //go back to main screen
            //    audioPlayer.Play();
            //    this.Frame.GoBack();
            //}
        }

        private async Task setVideoSource()
        {
            videoPlayer.Source = currentVideoUri;
        }


        private void setBackgroundImage(GridType grid)
        {
            string nextImage = "";
            switch (currentGrid)
            {
                case GridType.LANDING_PAD:
                    nextImage = I_TEMPLATE.Replace(TEMPLATE_NAME, I_FIRST_GENERAL);
                    break;
                case GridType.USER_INTERFACE:
                    nextImage = I_TEMPLATE.Replace(TEMPLATE_NAME, I_LAST_DELIVERY);
                    break;
                case GridType.ABLATION_CONFIRMATION:
                    nextImage = I_TEMPLATE.Replace(TEMPLATE_NAME, I_LAST_CONFIRMATION);
                    break;
                case GridType.POWER_DISTRIBUTION:
                    nextImage = I_TEMPLATE.Replace(TEMPLATE_NAME, I_LAST_POWER);
                    break;
                case GridType.COOLING:
                    nextImage = I_TEMPLATE.Replace(TEMPLATE_NAME, I_LAST_C02);
                    break;
                case GridType.PROBE_PORTFOLIO:
                    nextImage = I_TEMPLATE.Replace(TEMPLATE_NAME, I_LAST_PROBE);
                    break;
            }
            imageBackground.Source = new BitmapImage(new Uri(this.BaseUri, nextImage));
        }

        private void btnCooling_Click(object sender, RoutedEventArgs e)
        {
            if (disableNavigation) return;
            disableNavigation = true;
            currentGrid = GridType.COOLING;
            playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_C02));
        }

        private void btnConfirmation_Click(object sender, RoutedEventArgs e)
        {
            if (disableNavigation) return;
            disableNavigation = true;
            currentGrid = GridType.ABLATION_CONFIRMATION;
            playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_CONFIRMATION));
        }

        private void btnDelivery_Click(object sender, RoutedEventArgs e)
        {
            if (disableNavigation) return;
            disableNavigation = true;
            currentGrid = GridType.USER_INTERFACE;
            playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_DELIVERY));
        }

        private void btnPower_Click(object sender, RoutedEventArgs e)
        {
            if (disableNavigation) return;
            disableNavigation = true;
            currentGrid = GridType.POWER_DISTRIBUTION;
            playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_POWER));
        }

        private void btnProbe_Click(object sender, RoutedEventArgs e)
        {
            if (disableNavigation) return;
            disableNavigation = true;
            currentGrid = GridType.PROBE_PORTFOLIO;
            playVideo(V_TEMPLATE.Replace(TEMPLATE_NAME, V_PROBE));
        }

        private void cloakAllGridChildren(int delayMilliseconds, int staggerDelayMilliseconds)
        {
            //cloak all appropriate grids here
            cloakGridChildren(controlLandingPage.gridLandingPad, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlUserInterface.stackAblationDelivery, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlUserInterface.stackTouchScreen, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlPowerDistribution.stackPowerDistribution, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlCooling.stackCooling, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.gridAblationConfirmationTabs, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.gridAblationConfirmationNavigationButtons, delayMilliseconds, staggerDelayMilliseconds);
            //cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationLandingPage, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationSetup, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationStep1, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationStep2, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationStep3, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationStep4, delayMilliseconds, staggerDelayMilliseconds);
            //cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationStep5, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlAblationConfirmation.stackAblationConfirmationClose, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.gridProbePortfolioTabs, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioDesign, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioDesignIllustrations, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioOverview, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioPortfolioA, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioPortfolioAIllustrations, 0, 0);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioPortfolioB, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioPortfolioBIllustrations, 0, 0);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioPortfolioPageIndicators, 0, 0);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioMultiProbeSync, delayMilliseconds, staggerDelayMilliseconds);
            cloakGridChildren(controlProbePortfolio.stackProbePortfolioMultiProbeSyncIllustrations, delayMilliseconds, 0);
        }

        private async void cloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            if (grid.Children.Count > 0)
            {
                //get reverse to cloak in reverse order
                IList<UIElement> reverseList = new List<UIElement>();
                for (int i = grid.Children.Count - 1; i >= 0; i--) //UIElement elem in grid.Children
                {
                    reverseList.Add(grid.Children.ElementAt(i));
                }

                //make copy of children and move to tag
                IList<UIElement> transferList = new List<UIElement>();
                foreach (UIElement elem in grid.Children)
                {
                    transferList.Add(elem);
                }
                grid.Tag = transferList;

                if (staggerDelayMilliseconds > 0)
                {
                    //remove children in reverse order
                    foreach (UIElement elem in reverseList)
                    {
                        //await Task.Delay(rnd.Next(globalStaggerValue, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                        grid.Children.Remove(elem);
                    }
                }
                else
                {
                    grid.Children.Clear();
                }
            
            }
        }

        private async void decloakGridChildren(Panel grid, int delayMilliseconds, int staggerDelayMilliseconds)
        {
            if (delayMilliseconds > 0)
            {
                await Task.Delay(delayMilliseconds);
            }
            grid.Visibility = Windows.UI.Xaml.Visibility.Visible; //ensure visible
            if (grid.Tag != null && grid.Children.Count == 0)
            {
                IList<UIElement> list = grid.Tag as List<UIElement>;
                foreach (UIElement elem in list)
                {
                    if (staggerDelayMilliseconds > 0)
                    {
                        //await Task.Delay(rnd.Next(globalStaggerValue, 100));
                        await Task.Delay(staggerDelayMilliseconds);
                    }
                    grid.Children.Add(elem);
                }
                decloakGridChildren(gridBarsButtons, 0, 0);
            }
        }

        private void removeTransitionsFromPanelChildrenTag(Panel grid)
        {
            IList<UIElement> list = grid.Tag as List<UIElement>;
            foreach (UIElement elem in list)
            {
                if (elem.Transitions == null)
                {
                    elem.Transitions = new TransitionCollection();
                }
                elem.Transitions.Clear();
            }
        }

        private void addTransitionsToPanelChildrenTag(Panel grid, Transition transition)
        {
            IList<UIElement> list = grid.Tag as List<UIElement>;
            foreach (UIElement elem in list)
            {
                if (elem.Transitions == null)
                {
                    elem.Transitions = new TransitionCollection();
                }
                elem.Transitions.Clear();
                elem.Transitions.Add(transition);
            }
        }


        private async void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            //if (disableNavigation) return;
            ////populate files
            //List<File> files = new List<File>();
            //File file = new File();
            //file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Test File PDF 1";
            //file.fileUri = new Uri("ms-appx:///Assets/Test/testPDF1.pdf");
            //files.Add(file);
            //file = new File();
            //file.displayName = Translator.Current.translatedTextForKey("FileDisplayName[Test File PNG 1";
            //file.fileUri = new Uri("ms-appx:///Assets/Test/BG.png");
            //files.Add(file);
            //controlFilePicker.displayFiles(files);
            //animateFilesShow();

            List<File> files;
            switch (currentGrid)
            {
                case GridType.LANDING_PAD:
                    files = controlLandingPage.getCurrentFiles();  //no files
                    break;
                case GridType.USER_INTERFACE:
                    files = controlUserInterface.getCurrentFiles();  //no files
                    break;
                case GridType.ABLATION_CONFIRMATION:
                    files = await controlAblationConfirmation.getCurrentFiles(); //not configured
                    break;
                case GridType.POWER_DISTRIBUTION:
                    files = await controlPowerDistribution.getCurrentFiles();
                    break;
                case GridType.COOLING:
                    files = await controlCooling.getCurrentFiles();
                    break;
                case GridType.PROBE_PORTFOLIO:
                    files = await controlProbePortfolio.getCurrentFiles();
                    break;
                default:
                    files = new List<File>();
                    break;
            }


            if (files.Count() > 0)
            {
                //we found some files, display them in picker
                controlFilePicker.displayFiles(files);
                animateFilesShow();
            }
            else
            {
                //should have been hidden... hide now
                setPlusVisible(false);
            }

        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            disableNavigation = false;
            animateMenuShow();
        }



        private void controlLeftNav_HideMenuRequested(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void controlLeftNav_HomeClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void controlLeftNav_TimeAndPowerClicked(object sender, RoutedEventArgs e)
        {

        }

        private void controlLeftNav_SystemOverviewClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToSystemOverview();
        }

        private void controlLeftNav_CompetitiveComparisonClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToComparison();
        }

        private void btnCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void animateMenuHide()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            menuHide.Begin();
        }

        private void animateMenuShow()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Visible;
            menuShow.Begin();
        }

        private void animateFilesHide()
        {
            btnCloseFiles.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            filesHide.Begin();
        }

        private void animateFilesShow()
        {
            btnCloseFiles.Visibility = Windows.UI.Xaml.Visibility.Visible;
            filesShow.Begin();
        }


        private Point initialpoint;
        private bool swipeHandled;
        private void gridMain_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            initialpoint = e.Position;
            swipeHandled = false;
        }

        private void gridMain_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            int threshhold = 100;
            if (swipeHandled || initialpoint.X > 447)
            {
                e.Complete();
                return;
            }
            if (e.IsInertial)
            {
                Point currentpoint = e.Position;
                double diff = currentpoint.X - initialpoint.X;
                if (diff >= threshhold)
                {
                    swipeHandled = true;
                    animateMenuShow();
                }
            }
        }

        //private async void btnTest_Click(object sender, RoutedEventArgs e)
        //{
        //    Frame rootFrame = Window.Current.Content as Frame;
        //    rootFrame.Navigate(typeof(Viewer));

        //    //Uri testUri = new Uri(this.BaseUri, "//Assets//Test//testPDF1.pdf");
        //    Uri testUri = new Uri("ms-appx:///Assets/Test/testPDF1.pdf");
        //    await((Viewer)rootFrame.Content).initializeFile(testUri);
        //}

        private void btnCloseFiles_Click(object sender, RoutedEventArgs e)
        {
            animateFilesHide();
        }

        public async void requestDisplayFile(File file)
        {
            if (file.fileType() == File.FileType.PDF || file.fileType() == File.FileType.Video)
            {
                //Frame rootFrame = Window.Current.Content as Frame;
                //rootFrame.Navigate(typeof(Viewer));
                //await ((Viewer)rootFrame.Content).initializeFile(file.fileUri);
                controlDocumentViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
                controlDocumentViewer.ShowGridACTabs(file.isACVideo);
                if (file.storageFile != null)
                {
                    await controlDocumentViewer.initializeFile(file.storageFile);
                }
                else
                {
                    await controlDocumentViewer.initializeFile(file.fileUri);
                }
            }
            else if (file.fileType() == File.FileType.Image)
            {
                gridImage.Width = file.width;
                gridImage.Height = file.height;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.UriSource = file.fileUri;
                imageView.Source = bitmapImage;
                gridImageViewer.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

            //animateFilesHide();
        }

        private void btnCloseImageViewer_Click(object sender, RoutedEventArgs e)
        {
            gridImageViewer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        public void gotoUserInterface()
        {

            //manually switch to user interface screen and bypass transition

            videoPlayer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            currentGrid = GridType.USER_INTERFACE;
            setBackgroundImage(currentGrid);

            removeTransitionsFromPanelChildrenTag(controlLandingPage.gridLandingPad);
            removeTransitionsFromPanelChildrenTag(controlUserInterface.stackAblationDelivery);
            removeTransitionsFromPanelChildrenTag(controlUserInterface.stackTouchScreen);
            cloakAllGridChildren(0, 0);
            decloakGridChildren(controlUserInterface.gridAblationDelivery, 0, 0);
            decloakGridChildren(controlUserInterface.stackAblationDelivery, 0, 0);
            decloakGridChildren(controlUserInterface.stackTouchScreen, 0, 0);
            setPlusVisible(controlUserInterface.getCurrentFiles().Count() > 0);

            //add transitions back on
            Transition addDelete = new AddDeleteThemeTransition();
            addTransitionsToPanelChildrenTag(controlLandingPage.gridLandingPad, addDelete);
            addTransitionsToPanelChildrenTag(controlUserInterface.stackAblationDelivery, addDelete);
            addTransitionsToPanelChildrenTag(controlUserInterface.stackTouchScreen, addDelete);


        }

        public async void refreshPlusVisible()
        {
            switch (currentGrid)
            {
                case GridType.LANDING_PAD:
                    setPlusVisible(controlLandingPage.getCurrentFiles().Count() > 0);
                    break;
                case GridType.USER_INTERFACE:
                    setPlusVisible(controlUserInterface.getCurrentFiles().Count() > 0);
                    break;
                case GridType.ABLATION_CONFIRMATION:
                    setPlusVisible((await controlAblationConfirmation.getCurrentFiles()).Count() > 0);
                    break;
                case GridType.POWER_DISTRIBUTION:
                    setPlusVisible((await controlPowerDistribution.getCurrentFiles()).Count() > 0);
                    break;
                case GridType.COOLING:
                    setPlusVisible((await controlCooling.getCurrentFiles()).Count() > 0);
                    break;
                case GridType.PROBE_PORTFOLIO:
                    setPlusVisible((await controlProbePortfolio.getCurrentFiles()).Count() > 0);
                    break;
            }

        }

        private void controlLeftNav_AblationModalitiesClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToAblationModalities();
        }


    }
}
