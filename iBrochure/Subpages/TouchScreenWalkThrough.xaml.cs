﻿using iBrochure.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace iBrochure.Subpages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TouchScreenWalkThrough : Page
    {
        public TouchScreenWalkThrough()
        {
            this.InitializeComponent();
        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            animateMenuShow();
        }

        private async void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(200); }); //small wait
            ((SystemOverview)this.Frame.Content).gotoUserInterface();
        }

        private void translatePage()
        {
            Translator.Current.styleTextBlockWithKey(WalkthroughTab1Text1, "WalkthroughTab1Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab1Text1b, "WalkthroughTab1Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab1Text2, "WalkthroughTab1Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab1Text3, "WalkthroughTab1Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab1Text4, "WalkthroughTab1Text4");
            Translator.Current.styleTextBlockWithKey(txtDesc0101, "WalkthroughTab1Text5");
            Translator.Current.styleTextBlockWithKey(txtDesc0102, "WalkthroughTab1Text6");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab1Text7, "WalkthroughTab1Text7");
            Translator.Current.styleTextBlockWithKey(txtDesc0103, "WalkthroughTab1Text8");
            Translator.Current.styleTextBlockWithKey(txtDesc0104, "WalkthroughTab1Text9");
            Translator.Current.styleTextBlockWithKey(txtDesc0105, "WalkthroughTab1Text10");


            Translator.Current.styleTextBlockWithKey(WalkthroughTab2Text1, "WalkthroughTab2Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab2Text1b, "WalkthroughTab2Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab2Text2, "WalkthroughTab2Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab2Text3, "WalkthroughTab2Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab2Text4, "WalkthroughTab2Text4");
            Translator.Current.styleTextBlockWithKey(txtDesc0201, "WalkthroughTab2Text5");

            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text1, "WalkthroughTab3Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text1b, "WalkthroughTab3Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text2, "WalkthroughTab3Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text2b, "WalkthroughTab3Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text2c, "WalkthroughTab3Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text3, "WalkthroughTab3Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text3b, "WalkthroughTab3Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text3c, "WalkthroughTab3Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text4, "WalkthroughTab3Text4");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text5, "WalkthroughTab3Text5");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text6, "WalkthroughTab3Text6");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text7, "WalkthroughTab3Text7");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text8, "WalkthroughTab3Text8");
            Translator.Current.styleTextBlockWithKey(txtDesc0401, "WalkthroughTab3Text9");
            Translator.Current.styleTextBlockWithKey(txtDesc0402, "WalkthroughTab3Text10");
            Translator.Current.styleTextBlockWithKey(txtDesc0403, "WalkthroughTab3Text11");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text12, "WalkthroughTab3Text12");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text13, "WalkthroughTab3Text13");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text14, "WalkthroughTab3Text14");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text15, "WalkthroughTab3Text15");
            Translator.Current.styleTextBlockWithKey(txtDesc0501, "WalkthroughTab3Text16");
            Translator.Current.styleTextBlockWithKey(txtDesc0502, "WalkthroughTab3Text17");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab3Text18, "WalkthroughTab3Text18");
            Translator.Current.styleTextBlockWithKey(txtDesc0503, "WalkthroughTab3Text19");
            Translator.Current.styleTextBlockWithKey(txtDesc0504, "WalkthroughTab3Text20");
            Translator.Current.styleTextBlockWithKey(txtDesc0505, "WalkthroughTab3Text21");

            Translator.Current.styleTextBlockWithKey(WalkthroughTab4Text1, "WalkthroughTab4Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab4Text1b, "WalkthroughTab4Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab4Text2, "WalkthroughTab4Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab4Text3, "WalkthroughTab4Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab4Text4, "WalkthroughTab4Text4");
            Translator.Current.styleTextBlockWithKey(txtDesc0601, "WalkthroughTab4Text5");

            Translator.Current.styleTextBlockWithKey(WalkthroughTab5Text1, "WalkthroughTab5Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab5Text1b, "WalkthroughTab5Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab5Text2, "WalkthroughTab5Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab5Text3, "WalkthroughTab5Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab5Text4, "WalkthroughTab5Text4");
            Translator.Current.styleTextBlockWithKey(txtDesc0701, "WalkthroughTab5Text5");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab5Text6, "WalkthroughTab5Text6");
            Translator.Current.styleTextBlockWithKey(txtDesc0702, "WalkthroughTab5Text7");
            Translator.Current.styleTextBlockWithKey(txtDesc0703, "WalkthroughTab5Text8");
            Translator.Current.styleTextBlockWithKey(txtDesc0704, "WalkthroughTab5Text9");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab5Text10, "WalkthroughTab5Text10");
            Translator.Current.styleTextBlockWithKey(txtDesc0705, "WalkthroughTab5Text11");
            Translator.Current.styleTextBlockWithKey(txtDesc0706, "WalkthroughTab5Text12");


            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text1, "WalkthroughTab6Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text1b, "WalkthroughTab6Text1");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text2, "WalkthroughTab6Text2");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text3, "WalkthroughTab6Text3");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text4, "WalkthroughTab6Text4");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text5, "WalkthroughTab6Text5");
            Translator.Current.styleTextBlockWithKey(txtDesc0801, "WalkthroughTab6Text6");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text7, "WalkthroughTab6Text7");
            Translator.Current.styleTextBlockWithKey(txtDesc0802, "WalkthroughTab6Text8");
            Translator.Current.styleTextBlockWithKey(txtDesc0803, "WalkthroughTab6Text9");
            Translator.Current.styleTextBlockWithKey(txtDesc0804, "WalkthroughTab6Text10");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text11, "WalkthroughTab6Text11");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text12, "WalkthroughTab6Text12");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text13, "WalkthroughTab6Text13");
            Translator.Current.styleTextBlockWithKey(txtDesc0805, "WalkthroughTab6Text14");
            Translator.Current.styleTextBlockWithKey(txtDesc0806, "WalkthroughTab6Text15");
            Translator.Current.styleTextBlockWithKey(txtDesc0807, "WalkthroughTab6Text16");
            Translator.Current.styleTextBlockWithKey(txtDesc0808, "WalkthroughTab6Text17");
            Translator.Current.styleTextBlockWithKey(WalkthroughTab6Text18, "WalkthroughTab6Text18");
            
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void controlLeftNav_HomeClicked(object sender, RoutedEventArgs e)
        {
            //go back twice to get to Home
            this.Frame.GoBack();
            this.Frame.GoBack();
        }

        private void controlLeftNav_TimeAndPowerClicked(object sender, RoutedEventArgs e)
        {

        }

        private void controlLeftNav_SystemOverviewClicked(object sender, RoutedEventArgs e)
        {
            //Go back once to get to system overview
            this.Frame.GoBack();
        }

        private void controlLeftNav_HideMenuRequested(object sender, RoutedEventArgs e)
        {
            animateMenuHide();
        }

        private void controlLeftNav_CompetitiveComparisonClicked(object sender, RoutedEventArgs e)
        {
            //go back twice to get to Home and have home navigate to Comparison
            this.Frame.GoBack();
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToComparison();
        }

        private void btnCloseFiles_Click(object sender, RoutedEventArgs e)
        {
            animateFilesHide();
        }

        private void animateMenuHide()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            menuHide.Begin();
        }

        private void animateMenuShow()
        {
            btnCloseMenu.Visibility = Windows.UI.Xaml.Visibility.Visible;
            menuShow.Begin();
        }

        private void animateFilesHide()
        {
            btnCloseFiles.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            filesHide.Begin();
        }

        private void animateFilesShow()
        {
            btnCloseFiles.Visibility = Windows.UI.Xaml.Visibility.Visible;
            filesShow.Begin();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            translatePage();
            await showStep(1, 1);
        }

        private async void btnProcedureSetup_Click(object sender, RoutedEventArgs e)
        {
            await showStep(1, 1);
        }

        private async void btnTissuLoc_Click(object sender, RoutedEventArgs e)
        {
            await showStep(2, 1);
        }

        private async void btnTimeAndPower_Click(object sender, RoutedEventArgs e)
        {
            await showStep(3, 1);  //has steps 3, 4 and 5 within this tab
        }

        private async void btnCauterize_Click(object sender, RoutedEventArgs e)
        {
            await showStep(6, 1);
        }

        private async void btnReports_Click(object sender, RoutedEventArgs e)
        {
            await showStep(7, 1);
        }

        private async void btnSurgical_Click(object sender, RoutedEventArgs e)
        {
            await showStep(8, 1);
        }

        private void resetAllOnTabs()
        {
            btnProcedureSetupON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnTissuLocON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnTimeAndPowerON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnCauterizeON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnReportsON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnSurgicalON.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //////////
            btnProcedureSetup.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnTissuLoc.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnTimeAndPower.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnCauterize.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnReports.Visibility = Windows.UI.Xaml.Visibility.Visible;
            btnSurgical.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private int _currentSection = 1;
        private int _currentStep = 1;
        private int _autoStepDelay = 1000;
        private bool _firstOfTwoPressed = false;
        private bool _secondOfTwoPressed = false;
        private async Task showStep(int section, int step)
        {
            _currentSection = section;
            _currentStep = step;
            stackProcedureSetup.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackTissuLoc.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackTimeAndPowerSelector.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackTimeAndPowerSingle.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackTimeAndPowerMulti.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackCauterize.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackReports.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stackSurgical.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridTimer.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnHotspot2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            btnCloseWalkthough.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridTime1.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridTime2.Visibility = Windows.UI.Xaml.Visibility.Visible;
            gridHotspotMarker.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            gridHotspotMarker2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            //Margin="791,432,0,0" Width="62" Height="50"
            gridPower1.Margin = new Thickness(791, 432, 0, 0);

            //Margin="1045,432,0,0" Width="62" Height="50"
            gridPower2.Margin = new Thickness(1045, 432, 0, 0);


            switch (section)
            {
                #region Procedure Setup
                case 1: //////////////////////// SECTION 01
                    stackProcedureSetup.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    //Set Screen Image & hotspot
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnProcedureSetup.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnProcedureSetupON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        scrollViewerTabs.ChangeView(0, 0, 1, true);
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Procedure Set-Up/0_Landing-Page.png"));
                        //Width="80" Height="70" Margin="970,390,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 80;
                        btnHotspot.Height = 70;
                        btnHotspot.Margin = new Thickness(970,390,0,0);
                    }
                    else if (step == 2)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Procedure Set-Up/1_Select-Tissue-Type.png"));
                        //Width="53" Height="30" Margin="1319,747,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 53;
                        btnHotspot.Height = 30;
                        btnHotspot.Margin = new Thickness(1319, 747, 0, 0);
                    }
                    else if (step == 3)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Procedure Set-Up/2_PR-probe.png"));
                        //Width="91" Height="37" Margin="699,561,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 91;
                        btnHotspot.Height = 37;
                        btnHotspot.Margin = new Thickness(699, 561, 0, 0);

                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "---";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;


                    }
                    else if (step == 4)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Procedure Set-Up/3_Test.png"));
                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "---";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else if (step == 5)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Procedure Set-Up/4_Test-Done.png"));
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        //Width="44" Height="56" Margin="735,719,0,0"
                        btnHotspot.Width = 44;
                        btnHotspot.Height = 56;
                        btnHotspot.Margin = new Thickness(735, 719, 0, 0);
                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "---";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //////////////////////// STEP 01-01
                    if (step <= 1)
                    {
                        imgIcon0101.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0101.Opacity = 1;
                    }
                    if (step >= 1)
                    {
                        stackStep0101.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0101.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 01-02
                    if (step <= 2)
                    {
                        imgIcon0102.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0102.Opacity = 1;
                    }
                    if (step >= 2)
                    {
                        stackStep0102.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0102.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 01-03
                    if (step <= 3)
                    {
                        imgIcon0103.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0103.Opacity = 1;
                    }
                    if (step >= 3)
                    {
                        stackStep0103.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0103.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 01-04
                    if (step <= 4)
                    {
                        imgIcon0104.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0104.Opacity = 1;
                    }
                    if (step >= 4)
                    {
                        stackStep0104.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0104.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 01-05
                    if (step <= 5)
                    {
                        imgIcon0105.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0105.Opacity = 1;
                    }
                    if (step >= 5)
                    {
                        stackStep0105.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0105.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    if (step >= 6)
                    {
                        btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //Check for auto steps
                    if (step == 4)
                    {
                        await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay); });
                        imageScreenBehind.Source = imageScreen.Source;
                        await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay / 3); });
                        await showStep(_currentSection, _currentStep + 1);
                    }
                    break;
                #endregion
                #region Tissu-Loc
                case 2: //////////////////////// SECTION 02
                    stackTissuLoc.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    //Set Screen Image & hotspot
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnTissuLoc.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnTissuLocON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        scrollViewerTabs.ChangeView(0, 0, 1, true);
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Tissue-Loc/5_Clicked-NO-BUBBLES.png"));
                        //Width="91" Height="37" Margin="813,561,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 91;
                        btnHotspot.Height = 37;
                        btnHotspot.Margin = new Thickness(813, 561, 0, 0);

                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "---";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;

                    }
                    if (step == 2)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Tissue-Loc/6_Tissu-Loc_negative20_degrees.png"));

                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "---";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;

                    }
                    //////////////////////// STEP 02-01
                    if (step <= 1)
                    {
                        imgIcon0201.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0201.Opacity = 1;
                    }
                    if (step >= 1)
                    {
                        stackStep0201.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0201.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    if (step >= 2)
                    {
                        btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //Check for auto steps
                    //if (step == 2)
                    //{
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(3000); });
                    //    imageScreenBehind.Source = imageScreen.Source;
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay / 3); });
                    //    await showStep(_currentSection, _currentStep + 1);
                    //}
                    break;
                #endregion
                #region Time and Power - Select Single or Multi
                case 3: //////////////////////// SECTION 03
                    stackTimeAndPowerSelector.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnTimeAndPower.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnTimeAndPowerON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        scrollViewerTabs.ChangeView(0, 0, 1, true);
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Single Probe/single probe landing.png"));
                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "---";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        //user select single or multi to continue to next step

                    }
                    if (step >= 2)
                    {
                        btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    break;
                #endregion
                #region Time and Power - Single
                case 4: //////////////////////// SECTION 04
                    stackTimeAndPowerSingle.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnTimeAndPower.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnTimeAndPowerON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Single Probe/single probe landing.png"));
                        //Width="55" Height="44" Margin="862,326,0,0" 
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(862, 326, 0, 0);
                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "20";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step >= 2 && step <= 6)
                    {
                        //Width="55" Height="44" Margin="862,326,0,0" 
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(862, 326, 0, 0);
                        textTime1Minutes.Text = (step - 1).ToString();
                        textTime1Seconds.Text = "00";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step >= 6 && step <= 15)
                    {
                        //Width="55" Height="44" Margin="862,432,0,0" 
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(862, 432, 0, 0);
                        textPower1.Text = ((step - 2) * 5).ToString();
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 15)
                    {
                        //Width="393" Height="37" Margin="863,661,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 393;
                        btnHotspot.Height = 37;
                        btnHotspot.Margin = new Thickness(863, 661, 0, 0);
                        //Width="89" Height="37" Margin="701,608,0,0"
                        btnHotspot2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot2.Width = 89;
                        btnHotspot2.Height = 37;
                        btnHotspot2.Margin = new Thickness(701, 608, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 16)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Single Probe/single_probe_ablation_countdown.png"));
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 17)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Single Probe/single_probe_abaltion_end.png"));
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //////////////////////// STEP 04-01
                    if (step <= 5)
                    {
                        imgIcon0401.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0401.Opacity = 1;
                    }
                    if (step >= 1)
                    {
                        stackStep0401.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0401.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 04-06
                    if (step <= 14)
                    {
                        imgIcon0402.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0402.Opacity = 1;
                    }
                    if (step >= 6)
                    {
                        stackStep0402.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0402.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 04-03
                    if (step <= 15)
                    {
                    imgIcon0403.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0403.Opacity = 1;
                    }
                    if (step >= 15)
                    {
                        stackStep0403.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0403.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    if (step >= 17)
                    {
                        _currentSection = 5; //so the next button goes to 6 instead of 5...
                        btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //Check for auto steps
                    if (step == 16)
                    {
                        gridTimer.Opacity = 0.0;
                        gridTimer.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        txtTimerTimeRemaining.Text = "5:00";
                        double newOpacity = 0.0;
                        while (newOpacity < 1.0)
                        {
                            newOpacity += 0.05;
                            if (newOpacity > 1.0) newOpacity = 1.0;
                            gridTimer.Opacity = newOpacity;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(25); }); //small wait
                        }

                        imageScreenBehind.Source = imageScreen.Source;

                        double minutesLeft = 5.0;
                        while (minutesLeft > 0.0)
                        {
                            rotateHand2.Angle += 1.2;
                            rotateHand1.Angle += 12;
                            minutesLeft -= 0.21;
                            if (minutesLeft < 0) minutesLeft = 0;
                            string minutes = ((int)minutesLeft).ToString();
                            string seconds = ((int)((minutesLeft - ((int)minutesLeft)) * 60)).ToString();
                            if (seconds.Length == 1)
                            {
                                seconds = "0" + seconds;
                            }
                            textTime1Minutes.Text = minutes;
                            textTime1Seconds.Text = seconds;
                            txtTimerTimeRemaining.Text = minutes + ":" + seconds;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(200); }); //small wait
                        }

                        while (newOpacity > 0.0)
                        {
                            newOpacity -= 0.05;
                            if (newOpacity < 0.0) newOpacity = 0.0;
                            gridTimer.Opacity = newOpacity;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(25); }); //small wait
                        }
                        await showStep(_currentSection, _currentStep + 1);
                    }

                    break;
                #endregion
                #region Time and Power - Multi
                case 5:
                    stackTimeAndPowerMulti.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnTimeAndPower.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnTimeAndPowerON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Multi-Probe/Multi-probe-ablation-landing-page.png"));
                        //Width="55" Height="44" Margin="862,326,0,0" 
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(862, 326, 0, 0);
                        textTime1Seconds.Text = textTimeBlankTemplate.Text;
                        textTime1Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "20";
                        textTime2Seconds.Text = textTimeBlankTemplate.Text;
                        textTime2Minutes.Text = textTimeBlankTemplate.Text;
                        textPower2.Text = "20";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step >= 2 && step <= 6)
                    {
                        //Width="55" Height="44" Margin="862,326,0,0" 
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(862, 326, 0, 0);
                        textTime1Minutes.Text = (step - 1).ToString();
                        textTime1Seconds.Text = "00";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        textTime2Seconds.Text = textTimeBlankTemplate.Text;
                        textTime2Minutes.Text = textTimeBlankTemplate.Text;
                        textPower2.Text = "20";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step >= 6 && step <= 15)
                    {
                        //Width="55" Height="44" Margin="862,432,0,0" 
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(862, 432, 0, 0);
                        textPower1.Text = ((step - 2) * 5).ToString();
                        textTime2Seconds.Text = textTimeBlankTemplate.Text;
                        textTime2Minutes.Text = textTimeBlankTemplate.Text;
                        textPower2.Text = "20";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 15)
                    {
                        //Width="55" Height="44" Margin="1116,326,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(1116, 326, 0, 0);
                        textTime1Minutes.Text = "5"; //(step - 1).ToString();
                        textTime1Seconds.Text = "00";
                        textTime2Seconds.Text = textTimeBlankTemplate.Text;
                        textTime2Minutes.Text = textTimeBlankTemplate.Text;
                        textPower1.Text = "65";
                        textPower2.Text = "20";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step >= 16 && step <= 20)
                    {
                        //Width="55" Height="44" Margin="1116,326,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(1116, 326, 0, 0);
                        textTime1Minutes.Text = "5";
                        textTime1Seconds.Text = "00";
                        textTime2Minutes.Text = (step - 15).ToString();
                        textTime2Seconds.Text = "00";
                        textPower2.Text = "20";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step >= 20 && step <= 29)
                    {
                        //Width="55" Height="44" Margin="1116,432,0,0" 
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 44;
                        btnHotspot.Margin = new Thickness(1116, 432, 0, 0);
                        textPower1.Text = "65";
                        textTime2Seconds.Text = "00";
                        textTime2Minutes.Text = "5";
                        textPower2.Text = ((step - 16) * 5).ToString();
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 29)
                    {
                        //Width="393" Height="37" Margin="863,661,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 393;
                        btnHotspot.Height = 37;
                        btnHotspot.Margin = new Thickness(863, 661, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 30)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Multi-Probe/multi_probe_ablation_countdown.png"));
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 31)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Multi-Probe/multi_probe_abalation_end.png"));
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //////////////////////// STEP 05-01
                    if (step <= 5)
                    {
                        imgIcon0501.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0501.Opacity = 1;
                    }
                    if (step >= 1)
                    {
                        stackStep0501.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0501.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 05-06 to 05-14
                    if (step <= 14)
                    {
                        imgIcon0502.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0502.Opacity = 1;
                    }
                    if (step >= 6)
                    {
                        stackStep0502.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0502.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 05-15 to 05-19
                    if (step <= 19)
                    {
                        imgIcon0503.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0503.Opacity = 1;
                    }
                    if (step >= 15)
                    {
                        stackStep0503.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0503.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }

                    stackStep0504.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    stackStep0505.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                    //////////////////////// STEP 05-20 to 05-28
                    if (step <= 28)
                    {
                        imgIcon0504.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0504.Opacity = 1;
                    }
                    if (step >= 20)
                    {
                        stackStep0504.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0504.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    if (step <= 29)
                    {
                        imgIcon0505.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0505.Opacity = 1;
                    }
                    if (step >= 29)
                    {
                        stackStep0505.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0505.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    if (step >= 31)
                    {
                        btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //Check for auto steps
                    if (step == 30)
                    {
                        gridTimer.Opacity = 0.0;
                        gridTimer.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        txtTimerTimeRemaining.Text = "5:00";
                        double newOpacity = 0.0;
                        while (newOpacity < 1.0)
                        {
                            newOpacity += 0.05;
                            if (newOpacity > 1.0) newOpacity = 1.0;
                            gridTimer.Opacity = newOpacity;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(25); }); //small wait
                        }

                        imageScreenBehind.Source = imageScreen.Source;

                        double minutesLeft = 5.0;
                        while (minutesLeft > 0.0)
                        {
                            rotateHand2.Angle += 1.2;
                            rotateHand1.Angle += 12;
                            minutesLeft -= 0.21;
                            if (minutesLeft < 0) minutesLeft = 0;
                            string minutes = ((int)minutesLeft).ToString();
                            string seconds = ((int)((minutesLeft - ((int)minutesLeft)) * 60)).ToString();
                            if (seconds.Length == 1)
                            {
                                seconds = "0" + seconds;
                            }
                            textTime1Minutes.Text = minutes;
                            textTime1Seconds.Text = seconds;
                            textTime2Minutes.Text = minutes;
                            textTime2Seconds.Text = seconds;
                            txtTimerTimeRemaining.Text = minutes + ":" + seconds;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(200); }); //small wait
                        }

                        while (newOpacity > 0.0)
                        {
                            newOpacity -= 0.05;
                            if (newOpacity < 0.0) newOpacity = 0.0;
                            gridTimer.Opacity = newOpacity;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(25); }); //small wait
                        }
                        await showStep(_currentSection, _currentStep + 1);
                    }
                    //if (step == 31)
                    //{
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(3000); });
                    //    imageScreenBehind.Source = imageScreen.Source;
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay / 3); });
                    //    await showStep(_currentSection, _currentStep + 1);
                    //}

                    break;
                #endregion
                #region Cauterize
                case 6: //////////////////////// SECTION 02
                    stackCauterize.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    //Set Screen Image & hotspot
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnCauterize.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnCauterizeON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        scrollViewerTabs.ChangeView(0, 0, 1, true);
                        //imageScreen.Source = new BitmapImage(new Uri("ms-appx:///Assets/Images/Walkthrough/Time and Power/Single Probe/7_Power-and-Time-Chosen.png"));
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Time and Power/Single Probe/single_probe_abaltion_end.png"));
                        //Width="89" Height="36" Margin="813,609,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 89;
                        btnHotspot.Height = 36;
                        btnHotspot.Margin = new Thickness(813, 609, 0, 0);

                        textTime1Minutes.Text = "0";
                        textTime1Seconds.Text = "00";
                        //textTime2Minutes.Text = "0";
                        //textTime2Seconds.Text = "00";
                        textPower1.Text = "65";
                        //textPower2.Text = "65";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        //gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;

                    }
                    if (step == 2)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Cauterize/cauterize-end-85.png"));

                        textTime1Minutes.Text = "0";
                        textTime1Seconds.Text = "00";
                        //textTime2Minutes.Text = "0";
                        //textTime2Seconds.Text = "00";
                        textPower1.Text = "65";
                        //textPower2.Text = "65";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        //gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;

                    }
                    //////////////////////// STEP 02-01
                    if (step <= 1)
                    {
                        imgIcon0601.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0601.Opacity = 1;
                    }
                    if (step >= 1)
                    {
                        stackStep0601.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0601.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    if (step >= 2)
                    {
                        btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //Check for auto steps
                    //if (step == 2)
                    //{
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(3000); });
                    //    imageScreenBehind.Source = imageScreen.Source;
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay / 3); });
                    //    await showStep(_currentSection, _currentStep + 1);
                    //}
                    break;
                #endregion
                #region Reports and Tools
                case 7:
                    stackReports.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    //Set Screen Image & hotspot
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnReports.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnReportsON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        scrollViewerTabs.ChangeView(0, 365, 1, true);
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Reports and Tools/Report_landing_page.png"));
                        //Width="72" Height="23" Margin="994,260,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 72;
                        btnHotspot.Height = 23;
                        btnHotspot.Margin = new Thickness(994, 260, 0, 0);

                        textTime1Minutes.Text = "0";
                        textTime1Seconds.Text = "00";
                        textPower1.Text = "65";
                        textTime2Minutes.Text = "0";
                        textTime2Seconds.Text = "00";
                        textPower2.Text = "65";

                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;

                    }
                    if (step == 2)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Reports and Tools/17_Report.png"));
                        //Width="73" Height="21" Margin="1337,246,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 73;
                        btnHotspot.Height = 21;
                        btnHotspot.Margin = new Thickness(1337, 246, 0, 0);
                    }
                    if (step == 3)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Reports and Tools/18_Tools-Landing-Screen.png"));
                        //Width="125" Height="22" Margin="801,272,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 125;
                        btnHotspot.Height = 22;
                        btnHotspot.Margin = new Thickness(801, 272, 0, 0);
                    }
                    if (step == 4)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Reports and Tools/19_Tools-Procedure-History-Tab.png"));
                        //Width="84" Height="35" Margin="1294,247,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 84;
                        btnHotspot.Height = 35;
                        btnHotspot.Margin = new Thickness(1294, 247, 0, 0);
                    }
                    if (step == 5)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Reports and Tools/20_probes-done-ablating.png"));
                        //Width="30" Height="31" Margin="1302,788,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 30;
                        btnHotspot.Height = 31;
                        btnHotspot.Margin = new Thickness(1302, 788, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 6)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Reports and Tools/21_Tank-Detail.png"));
                        //Width="52" Height="28" Margin="1191,659,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 52;
                        btnHotspot.Height = 28;
                        btnHotspot.Margin = new Thickness(1191, 659, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        //gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 7)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Reports and Tools/20_probes-done-ablating.png"));
                        //Width="52" Height="28" Margin="1191,659,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 52;
                        btnHotspot.Height = 28;
                        btnHotspot.Margin = new Thickness(1191, 659, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }

                    //////////////////////// STEP 07-01
                    if (step <= 1)
                    {
                        imgIcon0701.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0701.Opacity = 1;
                    }
                    if (step >= 1)
                    {
                        stackStep0701.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0701.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 07-02
                    if (step <= 2)
                    {
                        imgIcon0702.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0702.Opacity = 1;
                    }
                    if (step >= 2)
                    {
                        stackStep0702.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0702.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 07-03
                    if (step <= 3)
                    {
                        imgIcon0703.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0703.Opacity = 1;
                    }
                    if (step >= 3)
                    {
                        stackStep0703.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0703.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 07-04
                    if (step <= 4)
                    {
                        imgIcon0704.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0704.Opacity = 1;
                    }
                    if (step >= 4)
                    {
                        stackStep0704.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0704.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 07-05
                    if (step <= 5)
                    {
                        imgIcon0705.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0705.Opacity = 1;
                    }
                    if (step >= 5)
                    {
                        stackStep0705.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0705.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 07-05
                    if (step <= 6)
                    {
                        imgIcon0706.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0706.Opacity = 1;
                    }
                    if (step >= 6)
                    {
                        stackStep0706.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0706.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    if (step >= 7)
                    {
                        btnNextSection.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    //Check for auto steps
                    //if (step == 7)
                    //{
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(1500); });
                    //    imageScreenBehind.Source = imageScreen.Source;
                    //    await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay / 3); });
                    //    await showStep(_currentSection, _currentStep + 1);
                    //}
                    break;
                #endregion
                #region Surgical Mode
                case 8:
                    
                    stackSurgical.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    if (step == 1)
                    {
                        resetAllOnTabs();
                        btnSurgical.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        btnSurgicalON.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        scrollViewerTabs.ChangeView(0, 365, 1, true);
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/16_2-probes-done-ablating-copy.png"));
                        //Width="78" Height="21" Margin="900,261,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 78;
                        btnHotspot.Height = 21;
                        btnHotspot.Margin = new Thickness(900, 261, 0, 0);
                        textTime1Minutes.Text = "0";
                        textTime1Seconds.Text = "00";
                        textPower1.Text = "65";
                        textTime2Minutes.Text = "0";
                        textTime2Seconds.Text = "00";
                        textPower2.Text = "65";
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    if (step == 2)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/1.Surgical-Mode-Landing-Page.png"));
                        _firstOfTwoPressed = false;
                        _secondOfTwoPressed = false;
                    }
                    if (step == 2 || step == 3)
                    {
                        if (_firstOfTwoPressed == false)
                        {
                            //Width="90" Height="28" Margin="992,320,0,0"
                            btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            btnHotspot.Width = 90;
                            btnHotspot.Height = 28;
                            btnHotspot.Margin = new Thickness(992, 320, 0, 0);
                        }
                        else
                        {
                            //Width="90" Height="28" Margin="992,320,0,0"
                            gridHotspotMarker.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            gridHotspotMarker.Width = 90;
                            gridHotspotMarker.Height = 28;
                            gridHotspotMarker.Margin = new Thickness(992, 320, 0, 0);
                        }
                        if (_secondOfTwoPressed == false)
                        {
                            //Width="90" Height="28" Margin="992,472,0,0"
                            btnHotspot2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            btnHotspot2.Width = 90;
                            btnHotspot2.Height = 28;
                            btnHotspot2.Margin = new Thickness(992, 472, 0, 0);
                        }
                        else
                        {
                            //Width="90" Height="28" Margin="992,472,0,0"
                            gridHotspotMarker2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            gridHotspotMarker2.Width = 90;
                            gridHotspotMarker2.Height = 28;
                            gridHotspotMarker2.Margin = new Thickness(992, 472, 0, 0);
                        }

                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "---";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "---";
                        //Margin="725,500,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);

                    }
                    if (step == 4)
                    {
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/2.Surgical-Mode-Testing-Probes.png"));
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "---";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "---";
                        //Margin="725,500,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);

                        //auto step
                    }

                    if (step == 5)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/3.Surgical-Mode-Done-Testing_Select-Icon.png"));
                        _firstOfTwoPressed = false;
                        _secondOfTwoPressed = false;
                    }
                    if (step == 5 || step == 6)
                    {
                        if (_firstOfTwoPressed == false)
                        {
                            //Width="42" Height="56" Margin="1200,365,0,0"
                            btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            btnHotspot.Width = 42;
                            btnHotspot.Height = 56;
                            btnHotspot.Margin = new Thickness(1200, 365, 0, 0);
                        }
                        else
                        {
                            //Width="42" Height="56" Margin="1200,365,0,0"
                            gridHotspotMarker.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            gridHotspotMarker.Width = 42;
                            gridHotspotMarker.Height = 56;
                            gridHotspotMarker.Margin = new Thickness(1200, 365, 0, 0);
                        }
                        if (_secondOfTwoPressed == false)
                        {
                            //Width="42" Height="56" Margin="1200,516,0,0"
                            btnHotspot2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            btnHotspot2.Width = 42;
                            btnHotspot2.Height = 56;
                            btnHotspot2.Margin = new Thickness(1200, 516, 0, 0);
                        }
                        else
                        {
                            //Width="42" Height="56" Margin="1200,516,0,0"
                            gridHotspotMarker2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                            gridHotspotMarker2.Width = 42;
                            gridHotspotMarker2.Height = 56;
                            gridHotspotMarker2.Margin = new Thickness(1200, 516, 0, 0);
                        }

                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "---";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "---";
                        //Margin="725,500,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);

                    }
                    if (step == 7)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/4.Surgical-Mode-No-Bubbles-Icon-Selected.png"));
                        //Width="55" Height="45" Margin="796,329,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 45;
                        btnHotspot.Margin = new Thickness(796, 329, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "20";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "20";
                        //Margin="725,500,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);


                    }
                    if (step >= 8 && step <= 19)
                    {
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = ((step - 3) * 5).ToString();
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "20";
                        //Margin="725,500,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);

                    }

                    if (step == 20)
                    {
                        //Width="55" Height="45" Margin="796,480,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 55;
                        btnHotspot.Height = 45;
                        btnHotspot.Margin = new Thickness(796, 480, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "85";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "20";
                        //Margin="725,500,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);

                    }

                    if (step >= 21 && step <= 32)
                    {
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;

                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "85";
                        //Margin="725,500,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = ((step - 16) * 5).ToString();
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);
                    }

                    if (step == 33)
                    {
                        //Width="395" Height="37" Margin="844,745,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 395;
                        btnHotspot.Height = 37;
                        btnHotspot.Margin = new Thickness(844, 745, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "85";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "85";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);
                    }
                    if (step == 34)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/6.Surgical-Mode-On-1 2-Selected.png"));
                        //Width="85" Height="35" Margin="1279,745,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 85;
                        btnHotspot.Height = 35;
                        btnHotspot.Margin = new Thickness(1279, 745, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "85";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "85";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);

                    }
                    if (step == 35)
                    {

                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/7.Surgical-Mode-Done-Ablating_Change-temps-to-120.png"));
                        //Width="85" Height="35" Margin="1279,745,0,0"
                        btnHotspot.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        btnHotspot.Width = 85;
                        btnHotspot.Height = 35;
                        btnHotspot.Margin = new Thickness(1279, 745, 0, 0);
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower1.Text = "85";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower1.Margin = new Thickness(725, 350, 0, 0);

                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTime2.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                        textPower2.Text = "85";
                        //Margin="725,350,0,0" Width="62" Height="50"
                        gridPower2.Margin = new Thickness(725, 500, 0, 0);

                    }

                    if (step == 36)
                    {
                        imageScreen.Source = new BitmapImage(await EtherFileFinder.uriForPath("ms-appx:///Assets/Images/Walkthrough/Surgical Mode/16_2-probes-done-ablating-copy.png"));
                        gridTimeAndPower1.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        gridTimeAndPower2.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        textTime1Minutes.Text = "0";
                        textTime1Seconds.Text = "00";
                        textPower1.Text = "65";
                        textTime2Minutes.Text = "0";
                        textTime2Seconds.Text = "00";
                        textPower2.Text = "65";
                    }
                    //////////////////////// STEP 08-01
                    if (step <= 1)
                    {
                        imgIcon0801.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0801.Opacity = 1;
                    }
                    if (step >= 1)
                    {
                        stackStep0801.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0801.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 08-02
                    if (step <= 3)
                    {
                        imgIcon0802.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0802.Opacity = 1;
                    }
                    if (step >= 2)
                    {
                        stackStep0802.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0802.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }


                    //////////////////////// STEP 08-03
                    if (step <= 4)
                    {
                        imgIcon0803.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0803.Opacity = 1;
                    }
                    if (step >= 4)
                    {
                        stackStep0803.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0803.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 08-04
                    if (step <= 6)
                    {
                        imgIcon0804.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0804.Opacity = 1;
                    }
                    if (step >= 5)
                    {
                        stackStep0804.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0804.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 08-05
                    if (step <= 19)
                    {
                        imgIcon0805.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0805.Opacity = 1;
                    }
                    if (step >= 7)
                    {
                        stackStep0805.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0805.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 08-07
                    if (step <= 32)
                    {
                        imgIcon0806.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0806.Opacity = 1;
                    }
                    if (step >= 20)
                    {
                        stackStep0806.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0806.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 08-08
                    if (step <= 33)
                    {
                        imgIcon0807.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0807.Opacity = 1;
                    }
                    if (step >= 33)
                    {
                        stackStep0807.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0807.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    //////////////////////// STEP 08-10
                    if (step <= 34)
                    {
                        imgIcon0808.Opacity = 0;
                    }
                    else
                    {
                        imgIcon0808.Opacity = 1;
                    }
                    if (step >= 34)
                    {
                        stackStep0808.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                    else
                    {
                        stackStep0808.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }

                    if (step < 5)
                    {
                        stackInnerSurgical.Height = 500;
                    }
                    else
                    {
                        stackInnerSurgical.Height = 1100;
                    }
                    if (step > 6) //auto scroll down
                    {
                        double current = scrollSurgicalMode.VerticalOffset;
                        if (current == 0)
                        {
                            current = 5;
                        }

                        while (current < scrollSurgicalMode.ScrollableHeight)
                        {
                            current *= 1.2;
                            scrollSurgicalMode.ChangeView(0, current, 1.0f);
                            await Task.Delay(10);
                        }




                    }
                
                    //close out of walkthrough
                    if (step == 36)
                    {
                        //btnBack_Click(null, null);
                        btnCloseWalkthough.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }

                    //Check for auto steps
                    if (step == 4)
                    {
                        await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay); });
                        imageScreenBehind.Source = imageScreen.Source;
                        await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay / 3); });
                        await showStep(_currentSection, _currentStep + 1);
                    }
                    //Check for auto steps
                    if (step == 34)
                    {
                        gridTimer.Opacity = 0.0;
                        gridTimer.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        txtTimerTimeRemaining.Text = "1:00";
                        double newOpacity = 0.0;
                        while (newOpacity < 1.0)
                        {
                            newOpacity += 0.05;
                            if (newOpacity > 1.0) newOpacity = 1.0;
                            gridTimer.Opacity = newOpacity;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(25); }); //small wait
                        }

                        imageScreenBehind.Source = imageScreen.Source;

                        double minutesLeft = 1.0;
                        while (minutesLeft > 0.0)
                        {
                            rotateHand2.Angle += 1.2;
                            rotateHand1.Angle += 12;
                            minutesLeft -= 0.21;
                            if (minutesLeft < 0) minutesLeft = 0;
                            string minutes = ((int)minutesLeft).ToString();
                            string seconds = ((int)((minutesLeft - ((int)minutesLeft)) * 60)).ToString();
                            if (seconds.Length == 1)
                            {
                                seconds = "0" + seconds;
                            }
                            textTime1Minutes.Text = minutes;
                            textTime1Seconds.Text = seconds;
                            txtTimerTimeRemaining.Text = minutes + ":" + seconds;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(200); }); //small wait
                        }

                        while (newOpacity > 0.0)
                        {
                            newOpacity -= 0.05;
                            if (newOpacity < 0.0) newOpacity = 0.0;
                            gridTimer.Opacity = newOpacity;
                            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(25); }); //small wait
                        }
                        await showStep(_currentSection, _currentStep + 1);
                    }


                    break;
                #endregion
            }
            await Task.Run(() => { new System.Threading.ManualResetEvent(false).WaitOne(_autoStepDelay/2); });
            imageScreenBehind.Source = imageScreen.Source;

        }

        
        private async void btnHotspot_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnHotspot) _firstOfTwoPressed = true;
            if (sender == btnHotspot2) _secondOfTwoPressed = true;
            await showStep(_currentSection, _currentStep + 1);
        }

        private async void btnTimePowerSingle_Click(object sender, RoutedEventArgs e)
        {
            await showStep(4, 1);
        }

        private async void btnTimePowerMulti_Click(object sender, RoutedEventArgs e)
        {
            await showStep(5, 1);
        }

        private async void btnNextSection_Click(object sender, RoutedEventArgs e)
        {
            await showStep(_currentSection + 1, 1);
        }

        private void btnCloseWalkthough_Click(object sender, RoutedEventArgs e)
        {
            btnBack_Click(null, null);
        }

        private void controlLeftNav_AblationModalitiesClicked(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
            ((MainPage)this.Frame.Content).navigateToAblationModalities();
        }

    }
}
