﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.Data.Pdf;
using Windows.ApplicationModel;
using Windows.Storage;
using Windows.Storage.Pickers;
using PdfViewModel;
//using Infuse.EtherWin;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Popups;
//using Infuse.EtherWin.Sessions;
using System.Net;
using Windows.Storage.Streams;
using System.Net.Http;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace iBrochure.DocumentViewer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Viewer : Page
    {

        private enum ViewerType
        {
            ViewerTypeNone,
            ViewerTypePDF,
            ViewerTypeImage,
            ViewerTypeVideo,
            ViewerTypeWeb
        }
        
        private Uri fileUri;

        private bool endFileViewDone = false;

        private DispatcherTimer timer;
        private bool tickUpdateSlider;

        private PdfDocViewModel pdfDataSourceZoomedInView;
        private PdfDocViewModel pdfDataSourceZoomedOutView;
        private PdfDocument pdfDocument;
        private StorageFile loadedFile;
        private GridView zoomedOutView;
        private ListView zoomedInView;

        public Uri fileURI
        {
            get { return fileUri; }
        }

        public Viewer()
        {
            this.InitializeComponent();
        }

        void timer_Tick(object sender, object e)
        {
            tickUpdateSlider = true;
            //timelineSlider.Value = videoPlayer.Position.TotalMilliseconds;
            timelineSlider.Value = videoPlayer.Position.TotalSeconds;
            tickUpdateSlider = false;
        } 


        public async Task initializeFile(Uri file)
        {
            try
            {
                this.fileUri = file;
                await this.LoadFile();
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to Initialize File.", ex);
            }
        }

        private void showViewer(ViewerType type)
        {
            pdfViewer.Visibility = type == ViewerType.ViewerTypePDF ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
            imageViewer.Visibility = type == ViewerType.ViewerTypeImage ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
            videoViewer.Visibility = type == ViewerType.ViewerTypeVideo ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
            webBrowser.Visibility = type == ViewerType.ViewerTypeWeb ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async Task LoadFile()
        {
            // Getting installed location of this app
            StorageFolder installedLocation = Package.Current.InstalledLocation;

            bool cannotOpen = false;

            try
            {
                //this.loadedFile = await file.GetLocalFileAsync();
                this.loadedFile = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(this.fileUri);
            }
            catch (Exception ex)
            {
                //nothing
                int i = 0;
                i++;
            }

            try
            {
                if (this.loadedFile != null)
                {
                    string value = Path.GetExtension(fileUri.AbsolutePath).ToUpper();
                    switch (value)
                    {
                        //PDFs
                        case ".PDF":
                            await this.LoadPDF();
                            break;
                        //IMAGES
                        case ".JPG":
                        case ".JPEG":
                        case ".PNG":
                        case ".BMP":
                        case ".GIF":
                        case ".TIFF":
                        case ".TIF":
                            await this.LoadImage();
                            break;
                        //VIDEOS
                        case ".3G2":
                        case ".3GP2":
                        case ".3GP":
                        case ".3GPP":
                        case ".M4A":
                        case ".M4V":
                        case ".MP4V":
                        case ".MP4":
                        case ".MOV":
                        case ".M2TS":
                        case ".ASF":
                        case ".WM":
                        case ".WMV":
                        case ".WMA":
                        case ".AAC":
                        case ".ADT":
                        case ".ADTS":
                        case ".MP3":
                        case ".WAV":
                        case ".AVI":
                        case ".AC3":
                        case ".EC3":
                            await this.LoadVideo();
                            break;
                        default:
                            //if (file.IsLocal)
                            //{
                                await LaunchExternal("Would you like to open this file?");
                                await this.Close();
                            //}
                            //else
                            //{
                            //    await LoadWeb();
                            //}
                            break;
                    }
                }
                else
                {
                    btnExit.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    showViewer(ViewerType.ViewerTypeNone);
                    MessageDialog dialog = new MessageDialog("File does not exist. Please ensure it has finished downloading from the Online Document Library.", "Unable to Locate File");
                    await dialog.ShowAsync();
                    await this.Close();
                }
            }
            catch (Exception ex)
            {
                cannotOpen = true;
            }
            if (cannotOpen)
            {
                await LaunchExternal("Unable to display this file.  Would you like to try and launch it externally?");
                await this.Close();
            }
        }

        private async Task LaunchExternal(string prompt)
        {
            try
            {
                btnExit.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                showViewer(ViewerType.ViewerTypeNone);
                MessageDialog dialog = new MessageDialog(prompt, "External File");
                dialog.Commands.Add(new UICommand { Label = "Yes", Id = 0 });
                dialog.Commands.Add(new UICommand { Label = "No", Id = 1 });
                IUICommand response = await dialog.ShowAsync();
                if ((int)response.Id == 0)
                {
                    //file.HasBeenOpened = true;
                    //await Utilities.startFileView(file);
                    //await Utilities.endFileView(file);
                    endFileViewDone = true;
                    //LAUNCH EXTERNALLY
                    //StorageFile localFile = await file.GetLocalFileAsync();
                    StorageFile localFile = this.loadedFile;
                    if (localFile != null)
                    {
                        // Launch the retrieved file
                        bool success = await Windows.System.Launcher.LaunchFileAsync(localFile);

                        if (success)
                        {
                            // File launched
                        }
                        else
                        {
                            // File launch failed
                        }
                    }
                    else
                    {
                        // Could not find file
                    }
                }

            }
            catch (Exception ex)
            {
                //do nothing, carry on!
                throw ex;
            }
        }

        private async Task LoadPDF()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypePDF);
            StorageFile localFile = this.loadedFile;
            //if (file.IsLocal)
            //{
            //    localFile = await file.GetLocalFileAsync();
            //}
            //else
            //{
            //    localFile = await downloadToTempFile(new Uri(file.Path));
            //}
            this.pdfDocument = await loadPDFFromFile(localFile);
            if (this.pdfDocument != null)
            {
                InitializeZoomedInView();
                InitializeZoomedOutView();
            }

        }

        private async Task<PdfDocument> loadPDFFromFile(StorageFile localFile)
        {
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            txtProcessingDescription.Text = "Processing";
            PdfDocument result = await PdfDocument.LoadFromFileAsync(localFile);
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            return result;
        }

        private async Task<StorageFile> downloadToTempFile(Uri uriPath)
        {
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            txtProcessingDescription.Text = "Downloading";
            string tempFilename = Guid.NewGuid().ToString();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, uriPath);
            HttpResponseMessage response = await (new HttpClient()).SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
            StorageFile result = await (ApplicationData.Current.TemporaryFolder).CreateFileAsync(tempFilename, CreationCollisionOption.ReplaceExisting);
            using (var fs = await result.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite))
            {
                var outStream = fs.GetOutputStreamAt(0);
                var dataWriter = new Windows.Storage.Streams.DataWriter(outStream);
                dataWriter.WriteBytes(await response.Content.ReadAsByteArrayAsync());
                await dataWriter.StoreAsync();
                dataWriter.DetachStream();
                await outStream.FlushAsync();
                outStream.Dispose();
                fs.Dispose();
            }
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            return result;
        }

        private async Task LoadImage()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypeImage);
            StorageFile localFile = this.loadedFile;
            //if (file.IsLocal)
            //{
            //    localFile = await file.GetLocalFileAsync();
            //}
            //else
            //{
            //    localFile = await downloadToTempFile(new Uri(file.Path));
            //}
            imageViewer.Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri(localFile.Path));
        }


        private async Task LoadWeb()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypeWeb);
            //webView.Navigate(new Uri(file.Path));
        }

        private async Task LoadVideo()
        {
            //await Utilities.startFileView(file);
            showViewer(ViewerType.ViewerTypeVideo);
            StorageFile localFile = this.loadedFile;
            //if (file.IsLocal)
            //{
            //    localFile = await file.GetLocalFileAsync();
            //}
            //else
            //{
            //    localFile = await downloadToTempFile(new Uri(file.Path));
            //}
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            txtProcessingDescription.Text = "Processing";
            videoPlayer.Source = new Uri(this.BaseUri, localFile.Path);
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            timer = new DispatcherTimer();
            timer.Tick += timer_Tick;
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Start();
            gridProcessing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            videoPlayer.Play();
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void videoPlayer_Tapped(object sender, TappedRoutedEventArgs e)
        {
            videoPlayer.Pause();
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void videoPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            btnPlay.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void timelineSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {

            if (tickUpdateSlider) return;

            int SliderValue = (int)timelineSlider.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, miniseconds. 
            // Create a TimeSpan with miliseconds equal to the slider value.
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue * 1000);
            videoPlayer.Position = ts;
        }

        private void videoPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            //timelineSlider.Maximum = videoPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
            timelineSlider.Maximum = videoPlayer.NaturalDuration.TimeSpan.TotalSeconds;
        }


        private void InitializeZoomedInView()
        {
            try
            {
                // Page Size is set to zero for items in main view so that pages of original size are rendered
                Size pageSize;

                pageSize.Width = Window.Current.Bounds.Width;
                pageSize.Height = Window.Current.Bounds.Height;

                // Main view items are rendered on a VSIS surface as they can be resized (optical zoom)
                this.zoomedInView = new ListView();
                this.zoomedInView.Style = this.zoomedInViewStyle;
                this.zoomedInView.ItemTemplate = this.zoomedInViewItemTemplate;
                this.zoomedInView.ItemsPanel = this.zoomedInViewItemsPanelTemplate;
                this.zoomedInView.Template = this.zoomedInViewControlTemplate;
                this.pdfDataSourceZoomedInView = new PdfDocViewModel(pdfDocument, pageSize, SurfaceType.VirtualSurfaceImageSource);
                this.zoomedInView.ItemsSource = this.pdfDataSourceZoomedInView;

                this.pdfViewer.ZoomedInView = zoomedInView;
            }
            catch
            {
                throw new Exception("Test1");
            }
        }

        /// <summary>
        /// Function to initialize ZoomedOutView of Semantic Zoom control
        /// </summary>
        private void InitializeZoomedOutView()
        {
            try
            {
                // Page Size is set to zero for items in main view so that pages of original size are rendered
                Size pageSize;

                // Page size for thumbnail view is set to 300px as this gives good view of the thumbnails on all resolutions
                pageSize.Width = (double)this.Resources["thumbnailWidth"];
                pageSize.Height = (double)this.Resources["thumbnailHeight"];

                // Thumbnail view items are rendered on a SIS surface as they are of fixed size
                this.pdfDataSourceZoomedOutView = new PdfDocViewModel(pdfDocument, pageSize, SurfaceType.SurfaceImageSource);

                this.zoomedOutView = new GridView();
                this.zoomedOutView.Style = this.zoomedOutViewStyle;
                this.zoomedOutView.ItemTemplate = this.zoomedOutViewItemTemplate;
                this.zoomedOutView.ItemsPanel = this.zoomedOutViewItemsPanelTemplate;
                this.zoomedOutView.ItemContainerStyle = this.zoomedOutViewItemContainerStyle;
                this.zoomedOutView.ItemsSource = this.pdfDataSourceZoomedOutView;
                this.pdfViewer.ZoomedOutView = this.zoomedOutView;
            }
            catch
            {
                throw new Exception("Test2");
            }

        }

        public void OnSuspending(object sender, SuspendingEventArgs args)
        {
            // Hint to the driver that the app is entering an idle state and that its memory
            // can be temporarily used for other apps.
            try
            {
                this.pdfDataSourceZoomedInView.Trim();
                this.pdfDataSourceZoomedOutView.Trim();
            }
            catch
            {
                throw new Exception("Test3");
            }

        }

        private void EventHandlerViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            try
            {
                if (!e.IsIntermediate)
                {
                    var scrollViewer = sender as ScrollViewer;
                    if (scrollViewer != null)
                    {
                        // Reloading pages at new zoomFactor
                        this.pdfDataSourceZoomedInView.UpdatePages(scrollViewer.ZoomFactor);
                    }
                }
            }
            catch
            {
                throw new Exception("Test4");
            }

        }
        private void EventHandlerViewChangeStarted(object sender, SemanticZoomViewChangedEventArgs e)
        {
            try
            {
                PdfPageViewModel sourceItem = e.SourceItem.Item as PdfPageViewModel;
                if (sourceItem != null)
                {
                    int pageIndex = (int)(sourceItem.PageIndex);
                    if (this.pdfDataSourceZoomedInView.Count > pageIndex)
                    {
                        // Transitioning from Zooomed Out View to Zoomed In View
                        if (this.pdfViewer.IsZoomedInViewActive)
                        {
                            // Getting destination item from Zoomed-In-View
                            PdfPageViewModel destinationItem = (PdfPageViewModel)this.pdfDataSourceZoomedInView[pageIndex];

                            if (destinationItem != null)
                            {
                                e.DestinationItem.Item = destinationItem;
                            }
                        }
                        // Transitioning from Zooomed In View to Zoomed Out View
                        else
                        {
                            // Getting destination item from Zoomed-In-View
                            PdfPageViewModel destinationItem = (PdfPageViewModel)this.pdfDataSourceZoomedOutView[pageIndex];

                            if (destinationItem != null)
                            {
                                e.DestinationItem.Item = destinationItem;
                            }
                        }
                    }
                }
            }
            catch
            {
                throw new Exception("Test5");
            }

        }

        private async void btnExit_Click(object sender, RoutedEventArgs e)
        {
            await this.Close();
        }

        private async Task Close()
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack)
            {
                rootFrame.GoBack();
            }
            try
            {
                if (!endFileViewDone)
                {
                    //await Utilities.endFileView(file);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to endFileView", ex);
            }
        }

        private void webView_ContentLoading(WebView sender, WebViewContentLoadingEventArgs args)
        {
            gridProcessingWeb.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void webView_LoadCompleted(object sender, NavigationEventArgs e)
        {
            gridProcessingWeb.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }


    }
}
