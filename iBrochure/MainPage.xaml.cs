﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Animation;
using iBrochure.Classes;
using System.Threading.Tasks;
using Infuse.EtherWinUI;
using Infuse.EtherWin;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace iBrochure
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private static bool etherLoaded = false;
        private static DispatcherTimer progressTimer = null;
        public MainPage()
        {
            this.InitializeComponent();

            
            //var bounds = Window.Current.Bounds;
            //double height = bounds.Height;
            //double width = bounds.Width;

            //string dimensions = height.ToString() + " : " + width.ToString();
            //txtTest.Text = dimensions;

        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            translateTextBlocks();
        }

        private void translateTextBlocks()
        {
            MainPageText1.Text = Translator.Current.translatedTextForKey("MainPageText1");
            MainPageText2.Text = Translator.Current.translatedTextForKey("MainPageText2");
            Translator.Current.styleTextBlockWithKey(MainPageText4, "MainPageText4");
            Translator.Current.styleTextBlockWithKey(MainPageText5, "MainPageText5");
            Translator.Current.styleTextBlockWithKey(MainPageText6, "MainPageText6");
            Translator.Current.styleTextBlockWithKey(MainPageText7, "MainPageText7");
            MainPageText8.Text = Translator.Current.translatedTextForKey("MainPageText8");
            MainPageText9.Text = Translator.Current.translatedTextForKey("MainPageText9");
            MainPageText10.Text = Translator.Current.translatedTextForKey("MainPageText10");
            MainPageText11.Text = Translator.Current.translatedTextForKey("MainPageText11");
            MainPageText12.Text = Translator.Current.translatedTextForKey("MainPageText12");
            MainPageText13.Text = Translator.Current.translatedTextForKey("MainPageText13");
        }

        private void btnOverview_Click(object sender, RoutedEventArgs e)
        {
            navigateToSystemOverview();
        }

        private void btnSimulator_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnExternalLaunch_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnComparison_Click(object sender, RoutedEventArgs e)
        {
            navigateToComparison();
        }

        public void navigateToSystemOverview()
        {
            this.Frame.Navigate(typeof(Subpages.SystemOverview));
        }

        public void navigateToComparison()
        {
            this.Frame.Navigate(typeof(Subpages.Comparison));
        }

        public void navigateToAblationModalities()
        {
            this.Frame.Navigate(typeof(Subpages.AblationModalities));
        }

        private void btnMicrowave_Click(object sender, RoutedEventArgs e)
        {
            navigateToAblationModalities();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (!MainPage.etherLoaded)
            {
                MainPage.etherLoaded = true;
                await Task.Delay(500);
                await Utilities.loadCachedConfigAsync();
                await Task.Delay(500);
                delayedUpdateProgress();
                await Utilities.downloadFilesAsync();
            }
            else
            {
                stackPanelNavigation.Visibility = Visibility.Visible;
                stackPanelDownloadProgress.Visibility = Visibility.Collapsed;
            }
        }

        private void delayedUpdateProgress()
        {

            progressTimer = new DispatcherTimer();
            progressTimer.Tick += async (s2, e2) =>
            {
                await UpdateProgress();
            };
            progressTimer.Interval = TimeSpan.FromMilliseconds(500);
            progressTimer.Start();
        }

        private async Task UpdateProgress()
        {
            if (progressTimer != null)
            {
                progressTimer.Stop();
                progressTimer = null;
            }
            if (! await isFinishedDownloading())
            {
                if (Utilities.Downloader.IsDownloading)
                {
                    stackPanelNavigation.Visibility = Visibility.Collapsed;
                    stackPanelDownloadProgress.Visibility = Visibility.Visible;
                }
                else
                {
                    stackPanelDownloadProgress.Visibility = Visibility.Collapsed;
                }
                DownloadProgress.Value = Utilities.Downloader.PercentCompleted;
                delayedUpdateProgress();
            }
            else
            {
                stackPanelNavigation.Visibility = Visibility.Visible;
                stackPanelDownloadProgress.Visibility = Visibility.Collapsed;
            }
        }


        private async Task<bool> isFinishedDownloading()
        {
            if (Utilities.DownloadFailed || Utilities.DownloadSucceeded)
            {
                EtherConfig config = Utilities.CurrentEtherConfig;
                if (config == null)
                {
                    return false;
                }
                IReadOnlyList<EtherFile> files = await config.AllFilesAsync();
                if (files.Count == 0)
                {
                    return false;
                }
                foreach (EtherFile file in files)
                {
                    if (file.isDownloaded() == false)
                    {
                        return false;
                    }
                }
                Translator.Current.loadDocument( await EtherFileFinder.storageFileForPath("Translation.xml"));
                translateTextBlocks();
                return true;
            }
            return false;
        }



    }
}
