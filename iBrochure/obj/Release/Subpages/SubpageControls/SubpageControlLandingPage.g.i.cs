﻿

#pragma checksum "C:\Users\Carlin\Documents\Visual Studio 2013\Projects\Neuwave\iBrochure\iBrochure\Subpages\SubpageControls\SubpageControlLandingPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E1CE9A2070442468123D71F02B2EA04E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iBrochure.Subpages.SubpageControls
{
    partial class SubpageControlLandingPage : global::Windows.UI.Xaml.Controls.UserControl
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid gridMain; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnDelivery; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnPower; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnCooling; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnProbe; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button btnConfirmation; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private bool _contentLoaded;

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent()
        {
            if (_contentLoaded)
                return;

            _contentLoaded = true;
            global::Windows.UI.Xaml.Application.LoadComponent(this, new global::System.Uri("ms-appx:///Subpages/SubpageControls/SubpageControlLandingPage.xaml"), global::Windows.UI.Xaml.Controls.Primitives.ComponentResourceLocation.Application);
 
            gridMain = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("gridMain");
            btnDelivery = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnDelivery");
            btnPower = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnPower");
            btnCooling = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnCooling");
            btnProbe = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnProbe");
            btnConfirmation = (global::Windows.UI.Xaml.Controls.Button)this.FindName("btnConfirmation");
        }
    }
}



