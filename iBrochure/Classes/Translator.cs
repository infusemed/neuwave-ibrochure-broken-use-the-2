﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml;

namespace iBrochure.Classes
{
    class Translator
    {
        Dictionary<String, String> translationDictionary = new Dictionary<string, string>();

        private static Translator _translator;
        private Translator() { } //hide default constructor
        public static Translator Current
        {
            get
            {
                if (_translator == null)
                {
                    _translator = new Translator();
                }
                return _translator;
            }
        }

        public void loadDocument(String path)
        {
            try{
            
                XDocument xml = XDocument.Load(path);
                translationDictionary =  xml.Descendants("data").ToDictionary(d => (string)d.Attribute("key"), d => (string)d);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }
        public void loadDocument(StorageFile file)
        {
            try
            {

                XDocument xml = XDocument.Load(file.Path);
                translationDictionary = xml.Descendants("data").ToDictionary(d => (string)d.Attribute("key"), d => (string)d);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }


        public String translatedTextForKey(String key)
        {
            if (translationDictionary.ContainsKey(key))
            {
                return translationDictionary[key];
            }
            return "";
        }

        public void styleTextBlockWithKey(TextBlock tb, String key)
        {
            String text = this.translatedTextForKey(key);

            double halfSize = tb.FontSize;// *0.75;

            if (text.IndexOf('|') == -1)
            {
                tb.Text = text;
            }
            else
            {
                String[] parts = text.Split('|');
                tb.Inlines.Clear();
                for (int i = 0; i < parts.Length; i++ )
                {
                    String part = parts[i];
                    if (part.StartsWith("boldsubscript"))
                    {
                        var r = new Run();
                        r.Text = part.Remove(0, 13);
                        r.FontSize = halfSize;
                        r.FontFamily = new FontFamily("Calibri Light");
                        r.SetValue(Typography.VariantsProperty, FontVariants.Subscript);
                        //r.Style = (Style)Application.Current.Resources["SubScriptStyle"];
                      
                        var b = new Bold();
                        b.Inlines.Add(r);

                        tb.Inlines.Add(b);
                    }
                    else if (part.StartsWith("bold"))
                    {
                        var r = new Run();
                        r.Text = part.Remove(0, 4);
                        var b = new Bold();
                        b.FontFamily = new FontFamily("{StaticResource BookFontType}");
                        b.Inlines.Add(r);
                        tb.Inlines.Add(b);
                    }
                    else if (part.StartsWith("subscript"))
                    {
                        var r = new Run();
                        r.Text = part.Remove(0, 9);
                        r.FontSize = halfSize;
                        r.FontFamily = new FontFamily("Calibri Thin");
                        r.SetValue(Typography.VariantsProperty, FontVariants.Subscript);
                        tb.Inlines.Add(r);
                    }
                    else if (part.StartsWith("superscript"))
                    {
                        var r = new Run();
                        r.Text = part.Remove(0, 11);
                        r.FontSize = halfSize;
                        r.FontFamily = new FontFamily("Calibri Thin");
                        r.SetValue(Typography.VariantsProperty, FontVariants.Superscript);
                        tb.Inlines.Add(r);
                    }
                    else if (part.StartsWith("linebreak"))
                    {
                        var r = new Run();
                        r.Text = part.Remove(0, 9);
                        var lb = new LineBreak();
                        tb.Inlines.Add(lb);
                        tb.Inlines.Add(r);
                    }
                    else
                    {
                        var r = new Run();
                        r.Text = part;
                        tb.Inlines.Add(r);
                    }
                }
            }


        }

        public void styleTextBoxWithKey(TextBox tb, String key)
        {
            String text = this.translatedTextForKey(key);
            tb.Text = text;
        }
    }
}
