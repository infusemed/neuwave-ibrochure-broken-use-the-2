﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage;

namespace iBrochure.Classes
{
    public class File
    {
        public enum FileType
        {
            Unknown,
            PDF,
            Image,
            Video
        }

        private double _width;
        public double width
        {
            get { return _width; }
            set { _width = value; }
        }

        private bool _isACVideo = false;
        public bool isACVideo
        {
            get { return _isACVideo; }
            set {_isACVideo = value;}
        }

        private double _height;
        public double height
        {
            get { return _height; }
            set { _height = value; }
        }

        private string _displayName;
        public string  displayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private Uri _fileUri;
        public Uri fileUri
        {
            get { return _fileUri; }
            set { _fileUri = value; }
        }

        private StorageFile _storageFile = null;
        public StorageFile storageFile
        {
            get { return _storageFile; }
            set { 
                _storageFile = value;
                _fileUri = new Uri(_storageFile.Path);
            }
        }
        public FileType fileType()
        {
            FileType result;
            switch (Path.GetExtension(fileUri.AbsolutePath).ToUpper())
            {
                //PDFs
                case ".PDF":
                    result = FileType.PDF;
                    break;
                //IMAGES
                case ".JPG": case ".JPEG": case ".PNG": case ".BMP": case ".GIF": case ".TIFF": case ".TIF":
                    result = FileType.Image;
                    break;
                //VIDEOS
                case ".3G2": case ".3GP2": case ".3GP": case ".3GPP": case ".M4A": case ".M4V": case ".WMV": case ".WMA": case ".AAC": case ".ADT": case ".ADTS":
                case ".MP4V": case ".MP4": case ".MOV": case ".M2TS": case ".ASF": case ".WM": case ".MP3": case ".WAV": case ".AVI": case ".AC3": case ".EC3":
                    result = FileType.Video;
                    break;
                default:
                    result = FileType.Unknown;
                    break;
            }
            return result;
        }

    }
}
