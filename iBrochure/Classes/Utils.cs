using System.Text.RegularExpressions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Windows.ApplicationModel.Resources;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace iBrochure.Classes
{
    public static class Utils
    {

        //private static bool USE_ETHER = false;

        private const string ObfuscatedTag = "Obfuscated";

        private static Dictionary<string, string> InitLanguageCodeToResourceMap()
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            map.Add("en-US", "LangEnglishUS");
            map.Add("en-GB", "LangEnglishGB");
            map.Add("de-DE", "LangGerman");
            map.Add("it-IT", "LangItalian");
            map.Add("es-ES", "LangSpanish");
            map.Add("fr-FR", "LangFrench");
            map.Add("ja", "LangJapanese"); // ja-JP doesn't work
            return map;
        }

        public static Visibility ResValueToVisibility(string resValue)
        {
            if (string.Equals("True", resValue ?? "", StringComparison.OrdinalIgnoreCase))
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public static async Task XmlSerializeAsync(object obj, StorageFile file)
        {
            using (var stream = await file.OpenStreamForWriteAsync())
            {
                await Task.Factory.StartNew(() =>
                {
                    XmlSerializer serializer = new XmlSerializer(obj.GetType());
                    serializer.Serialize(stream, obj);
                });
            }
        }

        public static async Task<T> XmlDeserializeAsync<T>(StorageFile file)
            where T : class
        {
            return (T)await XmlDeserializeAsync(typeof(T), file);
        }

        public static async Task<Object> XmlDeserializeAsync(Type type, StorageFile file)
        {
            using (var stream = await file.OpenStreamForReadAsync())
            {
                if (stream.Length == 0)
                {
                    //the file was empty
                    return null;
                }
                return await Task<Object>.Factory.StartNew(() =>
                {
                    XmlSerializer serializer = new XmlSerializer(type);
                    Object result = serializer.Deserialize(stream);
                    var fromFile = result as IFromFile;
                    if (fromFile != null)
                    {
                        fromFile.FileCreationTime = file.DateCreated;
                    }
                    return result;
                });
            }
        }

        public static async Task XmlObfuscateAndSerializeAsync(object obj, StorageFile file)
        {
            byte[] bytes = await Task<byte[]>.Factory.StartNew(() =>
            {
                using (var memStream = new MemoryStream())
                {
                    XmlSerializer serializer = new XmlSerializer(obj.GetType());
                    serializer.Serialize(memStream, obj);
                    return memStream.ToArray();
                }
            });

            using (var stream = await file.OpenStreamForWriteAsync())
            {
                //using (var writer = new StreamWriter(stream))
                using (var writer = XmlWriter.Create(stream, new XmlWriterSettings() { Async = true }))
                {
                    await writer.WriteStartDocumentAsync();
                    await writer.WriteStartElementAsync("", ObfuscatedTag, "");
                    await writer.WriteStringAsync(Convert.ToBase64String(bytes));
                    await writer.WriteEndDocumentAsync();
                }
            }
        }

        public static async Task<T> XmlDeserializeAndDeobfuscateAsync<T>(StorageFile file)
        {
            return (T)await XmlDeserializeAndDeobfuscateAsync(typeof(T), file);
        }

        public static async Task<Object> XmlDeserializeAndDeobfuscateAsync(Type type, StorageFile file)
        {
            string obfuscatedValue = null;
            using (var stream = await file.OpenStreamForReadAsync())
            {
                obfuscatedValue = await Task<string>.Factory.StartNew(() =>
                {
                    XElement xml = XElement.Load(stream);
                    return (xml == null || !xml.Name.LocalName.Equals(ObfuscatedTag, StringComparison.Ordinal)) ?
                        null : xml.Value;
                });
            }

            if (string.IsNullOrEmpty(obfuscatedValue))
            {
                return null;
            }

            return await Task<Object>.Factory.StartNew(() =>
            {
                using (var memStream = new MemoryStream(Convert.FromBase64String(obfuscatedValue)))
                {
                    memStream.Position = 0;
                    XmlSerializer serializer = new XmlSerializer(type);
                    Object result = serializer.Deserialize(memStream);
                    var fromFile = result as IFromFile;
                    if (fromFile != null)
                    {
                        fromFile.FileCreationTime = file.DateCreated;
                    }
                    return result;
                }
            });
        }

        public static async Task DeleteRecursiveAsync(StorageFolder folder)
        {
            var files = await folder.GetFilesAsync();
            foreach (var file in files)
            {
                await file.DeleteAsync(StorageDeleteOption.PermanentDelete);
            }

            var dirs = await folder.GetFoldersAsync();
            foreach (var dir in dirs)
            {
                await DeleteRecursiveAsync(dir);
            }

            await folder.DeleteAsync(StorageDeleteOption.PermanentDelete);
        }

        public static async Task DeleteFileConfirmedAsync(StorageFolder folder, StorageFile file)
        {
            string fileName = file.Name;
            try
            {
                for (; ; )
                {
                    await file.DeleteAsync(StorageDeleteOption.PermanentDelete);
                    file = await folder.GetFileAsync(fileName);
                }
            }
            catch (FileNotFoundException)
            {
            }
        }

        public static async Task<bool> CatchAllAsync(Func<Task> action)
        {
            // Attribution for this idea:
            // http://stackoverflow.com/a/13759803/1313473

            Exception exception = null;
            try
            {
                await action();
                return true;
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            if (exception != null)
            {
                await DebugLogAsync("Unhandled exception: " + exception.ToString());
            }
            return false;
        }

        public static async Task InitDebugLoggingAsync()
        {
            await _debugLogSemaphore.WaitAsync();
            try
            {
                if (_debugLogFile == null)
                {
                    var folder = Utils.GetDocFolderAsync();
                    _debugLogFile = await folder.CreateFileAsync("DebugLog.xml", CreationCollisionOption.OpenIfExists);
                }
            }
            finally
            {
                _debugLogSemaphore.Release();
            }
        }

        private static SemaphoreSlim _debugLogSemaphore = new SemaphoreSlim(1, 1);
        private static StorageFile _debugLogFile = null;
        public static async Task DebugLogAsync(string message)
        {
            await _debugLogSemaphore.WaitAsync();
            try
            {
                if (_debugLogFile == null)
                {
                    var folder = Utils.GetDocFolderAsync();
                    _debugLogFile = await folder.CreateFileAsync("DebugLog.xml", CreationCollisionOption.OpenIfExists);
                }

                await FileIO.AppendTextAsync(_debugLogFile,
                        "<Log Time=\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) + "\">\r\n" +
                        message + "\r\n</Log>\r\n");
            }
            finally
            {
                _debugLogSemaphore.Release();
            }
        }

        public static T Clone<T>(T obj)
            where T : class, new()
        {
            if (obj == null)
            {
                return null;
            }

            T clone = (T)Activator.CreateInstance(typeof(T));
            CopyProperties(obj, clone);
            return clone;
        }

        private static void CopyProperties(object source, object dest)
        {
            if (source == null || dest == null)
            {
                return;
            }

            var properties = source.GetType().GetRuntimeProperties();
            foreach (var prop in properties)
            {
                if (!prop.CanWrite) continue;

                var attr = prop.GetCustomAttribute(typeof(DoNotCloneAttribute));
                if (attr != null) continue;

                var indexes = prop.GetIndexParameters();
                if (indexes.Length == 0)
                {
                    CopyProperty(source, dest, prop);
                }
                // Else we ignore the property - in our case we don't need it. The only places we
                // use indexed properties, there are other non-indexed properties available that get/set all
                // the data that is available via the indexed property.
            }
        }

        private static void CopyProperty(object source, object dest, PropertyInfo prop)
        {
            var propVal = prop.GetValue(source);
            if (propVal is ValueType || propVal is string)
            {
                prop.SetValue(dest, propVal);
            }
            else if (propVal == null)
            {
                prop.SetValue(dest, null);
            }
            else
            {
                object newProp = null;
                try
                {
                    newProp = Activator.CreateInstance(prop.PropertyType);
                }
                catch //(MissingMemberException ex)
                {
                    if (prop.PropertyType == typeof(Brush))
                    {
                        prop.SetValue(dest, propVal);
                        return;
                    }
                }
                prop.SetValue(dest, newProp);
                if (propVal is IList)
                {
                    CopyList((IList)propVal, (IList)newProp);
                }
                else
                {
                    CopyProperties(propVal, newProp);
                }
            }
        }

        private static void CopyList(IList source, IList dest)
        {
            foreach (var item in source)
            {
                if (item is ValueType || item is string)
                {
                    dest.Add(item);
                }
                else
                {
                    var newItem = Activator.CreateInstance(item.GetType());
                    if (item is IList)
                    {
                        CopyList((IList)item, (IList)newItem);
                    }
                    else
                    {
                        CopyProperties(item, newItem);
                    }
                    dest.Add(newItem);
                }
            }
        }

        private const string DocFolder = "Neuwave\\NeuwaveEther";
        private static StorageFolder _docFolder;

        //public static async Task<StorageFolder> GetDocFolderAsync()
        public static StorageFolder GetDocFolderAsync()
        {
            if (_docFolder == null)
            {
                //Pointing root folder to Documents Folder
                //_docFolder = await KnownFolders.DocumentsLibrary.CreateFolderAsync(DocFolder, CreationCollisionOption.OpenIfExists);

                //Pointing root folder to AppData folder
                _docFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

            }

            return _docFolder;
        }



        //private static int index = 0;
        //public static void ReportGoogleAnalytics(string report)
        //{
        //    AnalyticsHelper.TrackPageView(report);
        //    AnalyticsHelper.Track("Page View", report, "TestLabel", ++index);
        //}

    }



    public class DoNotCloneAttribute : Attribute
    {
    }

    public interface IFromFile
    {
        DateTimeOffset FileCreationTime { get; set; }
    }

}
