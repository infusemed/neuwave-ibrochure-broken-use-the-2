﻿using Infuse.EtherWin;
using Infuse.EtherWinUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace iBrochure.Classes
{
    class EtherFileFinder
    {

        public static async Task<Uri> uriForPath(String path)
        {
            EtherFile file = fileFromPath(path);
            if (file != null)
            {
                StorageFile sf = await file.GetLocalFileAsync();
                return new Uri(sf.Path);
            }

            return null;
        }

        public static async Task<StorageFile> storageFileForPath(String path)
        {
            EtherFile file = fileFromPath(path);
            if (file != null)
            {
                StorageFile sf = await file.GetLocalFileAsync();
                return sf;
            }

            return null;
        }
        public static async Task<String> pathForPath(String path)
        {
            EtherFile file = fileFromPath(path);
            if (file != null)
            {
                StorageFile sf = await file.GetLocalFileAsync();
                return sf.Path;
            }
            return "";
        }

        private static EtherFile fileFromPath(String path)
        {
            String cleanPath = path.Replace("ms-appx:///Assets/", "");
            cleanPath = cleanPath.Replace("/Assets/", "");
            cleanPath = cleanPath.Replace("//", "/");
            String[] parts = cleanPath.Split('/');

            EtherConfig config = Utilities.CurrentEtherConfig;
            EtherFolder hiddenFolder = null;
            foreach (EtherFolder folder in config.Folders)
            {
                if (folder.Name == "HIDDEN")
                {
                    hiddenFolder = folder;
                    break;
                }
            }
            return fileFromPartsRecursive(hiddenFolder, parts);
        }
        private static EtherFile fileFromPartsRecursive(EtherFolder baseFolder, String[] parts)
        {
            if (parts.Length == 1)
            {
                foreach (EtherFile file in baseFolder.Files)
                {
                    if (file.Name == parts[0])
                    {
                        return file;
                    }
                }
            }
            else
            {
                String[] trimmedParts= new String[parts.Length-1];
                for (int i = 0; i < trimmedParts.Length; i++)
                {
                    trimmedParts[i] = parts[i + 1];
                }
                foreach (EtherFolder folder in baseFolder.Folders)
                {
                    if (folder.Name == parts[0])
                    {
                        return fileFromPartsRecursive(folder, trimmedParts);
                    }
                }

            }
            return null;
        }
    }
}
